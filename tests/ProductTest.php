<?php

use App\Enums\HttpStatus;
use App\Enums\PostgreSQL;
use App\Models\Category;
use App\Models\Product;
use App\Models\File;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;
use Illuminate\Support\Facades\File as StorageFile;
use Illuminate\Support\Str;

class ProductTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    protected static function path()
    {
        return storage_path('app/tests/product');
    }

    public function model()
    {
        $unserialize = unserialize(StorageFile::get(self::path()));
        $this->assertNotNull($unserialize);

        $id = $unserialize['id'];

        $model = Product::find($id);
        $this->assertNotNull($model);

        return $model;
    }

    public function testEmptyInsert()
    {
        $this->json('POST', '/product', [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['name']);
        $this->assertNotNull($data['price']);
    }

    public function testMaxLengthInsert()
    {
        $this->json('POST', '/product', [
            'name' => Str::random(46),
            'description' => Str::random(4001)
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['name']);
        $this->assertNotNull($data['description']);
    }

    public function testMinMaxInsert()
    {
        $this->json('POST', '/product', [
            'quantity' => PostgreSQL::INTEGER_MIN - 1,
            'price' => -1
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['quantity']);
        $this->assertNotNull($data['price']);

        $this->json('POST', '/product', [
            'quantity' => PostgreSQL::INTEGER_MAX + 1,
            'price' => PostgreSQL::INTEGER_MAX + 1
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['quantity']);
        $this->assertNotNull($data['price']);
    }

    public function testValidateTypeInsert()
    {
        $this->json('POST', '/product', [
            'status' => 'boolean',
            'quantity' => 'number',
            'price' => 'number',
            'metadata' => 'array'
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['status']);
        $this->assertNotNull($data['quantity']);
        $this->assertNotNull($data['price']);
        $this->assertNotNull($data['metadata']);
    }

    public function testInsert()
    {
        $category = Category::where('subscription_id', $this->getSubscriptionId())->orderBy('id')->first();
        $this->assertNotNull($category);

        $this->json('POST', '/product', [
            'category_id' => $category->id,
            'status' => true,
            'name' => 'Product ' . Str::random(24),
            'description' => Str::random(4000),
            'quantity' => rand(-9999.99, 9999.99),
            'price' => rand(.01, 9999.99),
            'metadata' => [
                'subscategory' => false,
                'version' => 1.0
            ]
        ], $this->getHeaderAuthorization());

        $this->seeStatusCode(HttpStatus::CREATED);

        $data = $this->data();

        $this->assertNotNull($data['id']);

        $this->assertNotFalse(StorageFile::put(self::path(), serialize($data)));
    }

    public function testUpdate()
    {
        $category = Category::where('subscription_id', $this->getSubscriptionId())->orderBy('id', 'desc')->first();
        $this->assertNotNull($category);

        $current = $this->model();
        $status = !$current->status;
        $name = 'Product ' . Str::random(24);
        $description = Str::random(4000);
        $quantity = $current->quantity + rand(999, 9999);
        $price = $current->price + rand(0, 100);
        $metadata = ['version' => rand(1, 100), 'other' => Str::random(24)];
        $this->json('PUT', '/product/' . $current->id, [
            'category_id' => $category->id,
            'status' => $status,
            'name' => $name,
            'description' => $description,
            'quantity' => $quantity,
            'price' => $price,
            'metadata' => $metadata
        ], $this->getHeaderAuthorization());

        $this->seeStatusCode(HttpStatus::NO_CONTENT);

        $product = $this->model();
        $this->assertTrue($current->category_id !== $product->category_id);
        $this->assertTrue($current->status !== $product->status);
        $this->assertTrue($current->name !== $product->name);
        $this->assertTrue($current->description !== $product->description);
        $this->assertTrue($current->quantity !== $product->quantity);
        $this->assertTrue($current->price !== $product->price);
        $this->assertTrue($current->metadata !== $product->metadata);

        $this->assertTrue($product->status == $status);
        $this->assertTrue($product->name == $name);
        $this->assertTrue($product->description == $description);
        $this->assertTrue($product->quantity == $quantity);
        $this->assertTrue($product->price == $price);
        $this->assertTrue($product->metadata == $metadata);
    }

    public function testValidateProductIdInvalidUpdate()
    {
        $product = $this->model();

        $this->json('PUT', '/product/' . $product->id, [
            'product_id' => $product->id,
            'name' => 'Product ' . Str::random(24),
            'price' => rand(1, 1000)
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['product_id']);
    }

    public function testProductIdOtherUserInsert()
    {
        $this->secondaryUser()->testInsert();
        $otherUserProduct = $this->model();

        $this->json('POST', '/product', [
            'product_id' => $otherUserProduct->id,
            'name' => 'Product ' . Str::random(24),
        ], $this->primaryUser()->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['product_id']);
    }

    public function testProductUploadImage()
    {
        $this->testInsert();
        $product = $this->model();

        $this->assertNotNull($product);

        $this->json('POST', '/file/product/' . $product->id . '/image', [
            'file' => '/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wgARCAHCAcIDASIAAhEBAxEB/8QAGwABAAMBAQEBAAAAAAAAAAAAAAECAwQFBgf/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIDBAX/2gAMAwEAAhADEAAAAfkd8vrM3t9PO283tlQtxZ/KnVvx1PSr5/p16Wvn+Oe95nz2V5+1n5JPb9L5Oa/Q9vgfot8vaUv18yYEgAAlEqAAAAAAAIAABCSgfJ/W8Xd4/p3nO1k8l/nDi48ZXbbIb7cfHrOuU3wjPbDl1pauy16cJzemvROs+37PxH1Xv+b3jXNIAAJiQFAAAAAEAACJJAAWlqPL9HSrE5fjPU8WNNctc7upjqLVtMTNZ8/e2WlTLbLSpiay793l+hy1X0uKfZ5Pr78nX7fnyAABMFkAAAAACJRAAoBEwAYTS3l+hbh6fAPBzWzrSaTnVYpbpztOc+fteaTLpEszn0zvteIians4unLtgxPoPU8X2fr/ACbIm5ABQAJQiQAAAAEAAKRMAHJbO3m99Pkfp/jJa3pfHRW2aTFoSq0c+gk1ma85helulmlqyunk6T0Yh577Pt+H7f1/l3G+QAEoLKJgBMSAqBEwAAAAIFAcM1ef2+d8j9L8zjp0Jjn1Z2jpymDl0lE5qWkXy25szK9L9Kqi2Orl7cuys58Z73t+R7H1fmSN8wUTEJAACYkELIISICAAAQkQLfPVnh6/B8H2vGzvatq46Us01zxt26eP08Onpa415F/Vonk83q8PXHPM12iEaT6HF6PJZXomfofS5Ov6nzCYQkAAAAsgAAEAAAAAAHl2zvy9XheD9L8zjfRnfOat28fbHoxO/wA725adLpjG9tNMc+/TU8Hzvsaaz8Fn9n4Ws8nblp57n6/lfTevzejtW3s8UAlEgAAKAmBIABAJISITAAAB5Fsr8/Tz/Ifb/I8+mMVmavvzanqdHL2eb0dnf5fVvPoUr22cN/Sxjn1aDi9LGz4rl9Di4uz6Th9b6PgsN8gAJQJACgAJiQQTAAAAAAAfP7eT6HL09fhezjnfx19efG7a4aWd/q/O+hy6+708Xoammtb2a9fBJ6HJdLVToj5OfpddY4ejqw9PjoNcwAAJRICgAATAACSABAUAQPzr1PCv5fofVdHz3p9eOPgfX+Xz6eC1yW98kvuex8f69n02/ldp02z2zq1rWza9E3VpGg5+nHePMjsx7+PJMb5k2ii1aBJRKgAABAUmJgCABSJgA/Loh5fo36OWbn3fR+T9DWPS831Ly/PV93FfGj1cow9bzLy/U93kennXf149U1N4uQtFlcrUgreqR031jn10max5fQz1jyHRh6PHAuQJAAAESCAACKAA/LFp830K2te5rq11zv18uvTj26ce6b2ro1XaN+Hp29Ly+vG/V14dl7L8lzpypMtrU6DDK/MdV+TSzptzXOicLE4dFtZ8vD2sunDynXzdeFRcgSgShEgAACokAX8znSePppa1rmLraxN631zvrl1Wab338/upppGN02w2ja9NzTrvvNZ06hl01k4eXv44rbNW9+e5val7NVLFrZVOmmdrPPz9Dz/R4kG+YCYmAAAUAKIH55N2OlZsSLJsm1Z1nT1/J+h59pnbHh6Vs+g5Fq51t63keqdWnNpW84TL0Tz2NpwuXzvY5se3zSbcyzqvyaWdlM7CeYdXHanfyB04gBAVMTESgShUoLKEfBrCqwrMiJK0+o+U+k5dvQ4/S5+Pq4ujDoS/Z0bTVdJsUjUYU65jijuqck9FFpNaF+PbE5011nS+dzbbn2OHPWnbzaWOvnBAUICgETBIBJCS/DtIuM2kFI0mXJ6ndnr856fp4Y37e/nerx9Xm19uYxvqWkzUtOQ2nCTecJN5xk1xtQ4MO3yY3rDUtfOybbctzXHbm7+W6HThMAmACzAJiQQSFIJKB8jGsb4ZtBX6fyfpfN7r6Z34+uyLRyc22GszbMvRtwj1ejwpPdjxtZfSpzdBR0jlac5o58rOjxNKpe/N2kXqrffk1l6/N7uXr562pbt5ZRIAACzAAAAAfNxq358Y2Vb3PC6+Hs9u/i9XD1+lfl2zvjrfi3jqc141mlpZACgTpkO6eK9nJhGWsJ5qJ093l90102rM1pplc6KTbWOS1L+jwyACYkESoABASgkHh10b4Z10yXLr8/mx193p5vpfP7uXo0x59Obj25jCac9nfbypT1r+Tuvo25NTdldbTFia6cFz5FeKvTl3z5tNZ9bt+f8Aazr1Zyvz663xsdGvLpZhel/V8+UTKBIExKgQSRMAmAkeRFm+GdNoOTl9Oh6vp/P+j5Ppd9cp59656jmp2ynnU9STxsPoKr8zj9TznzPRv8vvn9Xv839JLt0+h2Z38/z/AFbWfzzzv1L47r5/A7+OenD3Ojxuzn6O23PaXp05ejfK9q26cEplQCYkmJhQBCSgSiVA8uZb4REisWot3Ptx9VteacdfQ38iZv27eLvnXq24N876rYXl1rW5xeX9NqnN0XlYtEE1jMv893+X14/O068/T87jp11nTDqjoue/v4e1NrVtNTKJZQJmILTWVEAIBKCyDzkt8YialcdsTg5+7mmtOvyMOXp+jfP9WenruTojS2crttze9ncd8X59ZtEkxFSa047N/PzjfOctOXpx5I6J7+Hldcpza7WI2rpOkzEzUxKWJSIkISTCVIExEgAHDFo3xiLRFK6QY4dkV5+Pp1TyMvYxmvHj08p0w6sMsdPf+l+B+q5ej2Lefpz7d88MHfjwYazvzw1zmmPhbx1b8vpd/FpM2krM2IslZtEzUzEzQglAsgASgTAJgJgEjiTG+USREWgqlVYuM40GFOmDiz9CicXp83Ry9RWMdrzlc21xmXXzuTm6cY7L9nXyRuvLEyVKYSLMwWyJlTBUxJBJEhMBEgIJAQOUnXKEwK2ghKoSiIsWq0FaXovPx9fnL36+FXl6PoHgWX1+Gnb08+Pdfo3xjSbZ1F4ssSmWJiRIokiUygCQFEEggABMAHMNcgIi0ESAkhIiLQtaaQc3P30PNz9SLnzdu21mG+l5a3mZUpVJLJBIExKyFCJRIRIRJMCgIkAAAc0xOuaJEJhABJEhCSxFhWLikaEpNhFkhJSUpIRJQEhMCyiYTEgKAAAAAAC841xAQCQiQBZgJAAAkEgCyISKAkAUBIBAEhQAAECyCAn/xAArEAACAgEDBAIBBQEAAwAAAAAAAQIRAwQQEhMgITFAUEEFFCIwMiMVM3D/2gAIAQEAAQUCMWN5JafAscdrJSJ5VFZM0s0seJRJZlEhGWVwhGJaRPPGJPWpD1rP3siOuZj1yIZ1Iu/pErej0/GO7kZMigs2Z5pxaipZnIj4ePISzqKy6wlklLt8mPPKDwankKV/R6PDylFUtmyc6Wp1DySXguz0IeRQU8spsoZeyZHydMVwemz8knf0Wnx8IbyZrdRW/rZy4puyt2LaheCLskjHLhPDk5L6FbtmfJwhknzmLa6H5F2MXZB7NGlmRdr6Ns12a5C3bvufaiLGY3U8T8fRNmWfGE3ynvJ9yH3Qe35wPx9EzXZKiLf2+1D7se+nfhevoZM1cuWXeXpd0R92PfT+o+vocrqE3c1u/wDXchn57MYttP6j6+h1UqxC3Xctpd2PfT+o+vodc/8AmL0P0u5bS7se35068L19Drn43lvQoM6Z0yqGPtXuAyHmWFePotd2MirfSFjFA4HAeMliJYmVXZD3EZgVyxrx9Frl4R+NsK/lxK7KOA8JLTk9O0Si47QWzNLAgvo9YrhuveLxLkR8nTs6RxoRRRwHhTMmlTMulcHFDIq5YI0l9EjURuD8SWy9p+URITIyTOKY8RxrZCRxM2JVkjxmzTwt44/RIRNXHUR4zWy2x+UhESMiMjimPEULbJ61X+4x5PDjpL6KL31ePs/GGZFkRIQhSojkscUx+BMl61UG3gwtEI/R48gmJmWPKOaHGWyE6ePIY5WRF2RmT8kSrHhsWE6Y419FGdPHlIyPZnxWSjxe8ZGHIRkRl2IZGJXZNfRxnRDMQyniSzYSUGuyM3ExaghkTExMQirEu1jiOBW1HFlP5tkcrRi1Bakp4rJYRwZRR6IZpRMesMebkKZF2RXa+zicBROJwJw+cjHmaI5LPDHjQ8R0h4TokcHnDj4qMbIRr+lb0VtJEoFfMoSI+CMxS34nAjARjExMve9qJeCxMsssvZxHjJQ+RRRRQhbITFvHaE6FMUjkci72QjKyyyyyyyyyykx4kyWEar4lFFFdyEhIUd1tZG2KJxJWQ2ye9rLLL7LLLJRTJx4v4Vf0wiKIls9o7QjZFJd0o2OD7Eeiy9mWWWT8r5SMcSt36ExSIerLLLLLL24o6aOkeizkciyxlljfj5UP9Y14oey9SW0UR9b2WWWWWWSdRZZeyES9WWcvlw/1i/zRLaJ03Nw0sULFBHCJ00dM6bOL7bLJv+L7Ue02WL5mlncTJA/MSCpf0cUdM6bKaLG9n2oyL+aXzdLOnB2qsnjoj7W1lllll90kmTVFj7UTXylFtw0eSRH9OH+mo/YzxvFyTi4C4M6WNnTRwODKfZZZZZZY2T8qz32chTHbj8eiMOUsGnjjSXZl2s5yOtMWoYs8WKcWeGcUcDi+yyyycqVkWeThI4RFxFIu16fx9Iv+pfZkdz71OSFmkhagWeIpxZxixwobOSHmSJ5HIrxFll7oRkX8/h0UUUYXxnHKhSLLLORf9iySQs7JOOQk/NjZ1CLIvxuhGX41FEf9Cm0RzEZp7ThtbOZyRf8AXKVybJS2RjfYhE/MF8dyojNSRDFKZHTJFKJLLRKcWdRIU0y9rLLL7sj44+ozk3tYpK4Pz2/j4rGSLcXok9QLxtIkhxHAeE4TRzyxP3FEc8GKaLLL3Rrp8MHUR1jqs5SZHxKLIvwWWWX8ehxHE0NftrL2aOJwOB0zpjxD06Y9NQ1ngfvZxcddEWsxn7rGQ1HIy6Napf8Ah9OS/RombQZsG+KXiEiyyyy/j0UOJhbglnQpJ9tFHE4HTHiJYjU6fljMOlyZTB+nRiY8EYiXZ+o6ZY8gnRHIRylosTF8pbWLLJCziyJ/0NGWH8dP+mJOGKMSu2z9Rlyx7M5SFKZBsxsT+V1FF32qTQszFnQsiZyRaOSF5FErucqJ5LNV5xcRxOBwFAjAhGhfJZMWSeMjqokZxl3WcmR5SMeIS755KJTvbN/66OJwR0zpiiJfKZKI4jgcGhZcsCOrI54SL3j5eLGorubJ5Ru95T5FFFFFfMoocBwHAcBwHjFziLUzRHVxNPljKcX4ssssslOiWS95SUVkzvI8Xr6OjicBwHA4HAeMwR45IZKFlOodQ6w8w5XvkzRxqeSWVwxkV9PRxOBwHAhGpbcmcmWLb0ZtQonnI4YyK+qooo/N9qHNRWXUORGFkICX1rJHWprMmc0ckdRIlqEiU5ZCMCMBL65kiURplzRzmfyZHGRgKIl9g0OA8Z0zpixigKJX2NFHE4nEor7Wiiv/AIv/AP/EACYRAAICAQMEAgMBAQAAAAAAAAABAhEDECAwEiExQAQiMkFRE2D/2gAIAQMBAT8B3WOR9j7E+/pva3RGDl5OlIWk8SaJxp16qJdiMb7vVaI+RjtX6i0f2db5KySp+ktJdkY1+97Mv5ektMgvGxasyefRWjlQ+8jpOgcStk3SH6K0mQ/IbLLL2Z5dq9JCGR0ZZell9jJK36TVCeiEy9s02icGvSmrPAnqxPbJWiWJrSn6DR4E9OrfR0IpEoJocWuWxschyP8ARibZHicUyWD+Dg154+osbGRjbIxrR8bVmSHS+Ky9cC580rfFezC/1zTlXHZZYsc2Rxyixcubzx2WfGiq6uS9mddr5KPjyqIpaeC91jLE9ciuPLilTIq+5WlI6SttjkQlerfNin9Vvk2iLb06EZcdK0RlTFM6jJk/S5sT+pZ1sUy9a2ZZfVikKaJzRfNGXSxZEy9Y7GxyMrqOlFC9DqaFmFlRjmWWORZKaiSk5O/Wow+Nk8qQ25ewpUxTHMnm/h58+5X/ABn/xAAmEQACAgEDBAICAwAAAAAAAAAAAQIREAMSMCAhMUAEQRMyIlFg/9oACAECAQE/AV10djsR9NLqlNR8G9sbL/o09Z3TIu16yJyrssIeJr7PjanreFY8feZrsaTpi9J4RqP6z94RLwQ8kPHpSxAfnDI5n4NPyR8emon6oeoj8jNzIvOozRVsXovETU/VkYGw2lYsk7Z8eP36bESKEiisUKH8iEaXpJ2PDJIrpjSdikn6UXR5GsMQ10p0KWL9BYrFddm5lsjIT50hI2nZEuJSFqCafHRWW6JSvCzXAmRdrm1Hz6a7c2pzRXM5oc0+bT5dWXeuR9Gly6qtjjmuCh5j55ZqyTrNssvoWKJLK5px79aSY0kI3MhO+w1ZtKIx5p+cbTaVm+iC7lG0UedqxxKy+ihIiu/qUbDYTiUUKOIxsqvXn0RgePYqzabRQ/1P/8QALBAAAQMCBAUDBAMAAAAAAAAAAQARITFQAhAgMBIiMkBRYXBxAxNggEGBkf/aAAgBAQAGPwJNqlNhopTBTTOuqtlZOdLlemTBTnyqTa+I6XTCm/Fp4ReHT9k1m4ReCibw35iUTeD76TeaJxfH/MKe7s/uGNUKZXSqewr91C8KTlCYquXSO3p3LDUM6qqnKu9ReFJddNprlIvlVK8FNtg9847A7Z7uBHlSTlRUbKu5iNlcInEOXDsRiK8qcJVdj5OkGwt67dFykhQXTY8GXUupcoJWH7hIA/gKuP8A1cn1D/ac4Xw+RY4Uqu4fIygQpVNPHhpis07RXF9SSoGsfOmqrYq3T0XNCg7M3CFV/lc2FQexYWmCVIdTCg7cpsNLx6XdhVObu2FT+1//xAAoEAEBAQACAgICAgIDAAMAAAABABEQITFBIFFQYTBAcYGRobFgwdH/2gAIAQEAAT8hgIIj13FvJbqnrZQtg/ttI8gnieJDxTelwAe53lg+oj8IYhAQdx1bxlOUsYPP5OdOzAWgWd0iPwMul591CTWIfg+uOoMrYsSBlM26SxaeRhkrdre7uT1Bt0ll2jbHzYjo6nvVid92T8EBmcbbhwt2+2G8sdIdY/2SPWIT4jeUExrs1qSEniIe/wAF0LY4xaKziPu8yTslaw9vwMO45yY7LusXX4KNtt64uyuDbblog+Bw9w8rG2IbZVuPwOxdESNn/cR5johvRFtvPlx9x8o6N1o8fgNid0n3wJvBseSXLbbbed8xbbbKPHHQ/BG9cGqfXB1wvCGE/NfMTxso8cLp+BCXq1F/ljgxdp6+R64sJeHh4ceD8CEvFN1j1N4Lz2fiO7wSvcT8B4tj0/AhNjiPPB7nPCfPxMyjh5F6judH8CE3ixHjj0jgTIh2oYnPuPh2h6l1DGyEePwLesR4tvIuujkYPhMDK8p5GwYTu9sh+C9XmcDh7tq8BEEVLE+rf1eGkunOdEtboGwPwR4tyI8cdouJiAmi16QiFw3oIWYg6s5dWcWJDD8ErTjoS5sDPSSWPnkL9FuBDgyyl1bRLW7wWAfglLu0C1WUy7lnjGN0kI3zZbLxdeBd4crCWRHD8FrDeS2FLw/Bh0wMOIcHgdGI0kvODvemztEsD8Hoebc4tyVIeF1xCDZrDDkmOmyN4dUD8RHq8Zvwav2WtuLOuT8A8YML75kd4IXhnAQILLrkx/ArK790T7mGm4SvA8IbF1re0v38gRtYQRHHe14FFkN4kvX9veBHAA9LFRxiFbJncV8xe0B0i5AEFnCm2zeP+EMCQ2P95J4usYzJCIJ/W/Ra+pUWBOrJEcrLPIsLHLQu6Uf2wjgJ8JPdrDsFhsRbdIGcgTbZ1wbhiORIcHTA8Slmf1s4EIQQiXEoY7hzAg+4H3B5uzDkFIQpxnA8CEPEq7P6wQgQREXn8JszmQuA8IsPUfu9XfhsQpSGvgsbUiHALL+j+oyyyCIiLbnQhwuN/wCoWG26YwtvOi+rE8nOmA8nuP8AjAtv05Sk/p+WWWckXZC6yM8EIyHB1MQ4DkMMlPUv6n623z5DPuNyx4nFPn+lllny8N156cN7hswbLtn1hth+B8vgN2fXIMp3v+uV0Yf08ssss+Cyu4npDGPPDqheWqhuhB+OD+jMI+rs4LeXR3YeSJXkLG2sOv6eWfM6bDgvZZkEBkPG8bzg+pf1I9M+jufW4dNt0eCI45LPff8AV8ssssss56KGsjJ1pdRLobY+QG2228ecJ3+uPnvBETi6/wBP6uWWWWWCCt5WfYWHpY0VpAQZexP0m8j/AIIE62/R4EvJxtv8DANC/wDLDjghgX0C2pqC3+pllnBC8zGeu4D1zlkQcaIDwoCG8LznV4QX6CXn6sl6kTjfggdbyX7eDs4Qbyky8v8AhZekvrC3g3kPqH+nlllkLp9ERN50P1189y8Ar20sK/V7MnxJ/DuA8yfuI683mZ7eAeWvvgZTsNfcf1wTSZ5gY5aFXfNh/j1Pd4GHhHUsJeSeA3stOJDyjr/RD/Tyzk6C29tfZeBbsv8A8yerL3H2iMP8JeLUfvg0cLZ93o4GJTv9F3/UZZw2i0A23/dheaz+urF0f9wrq9z3fqUdt4YQIYX3Dj5QX6dLf6nyG2wTs73YjgYYYZdR+o/p5ZyC75mMDof2/UAAdBbdy7eLf1E+r/7svYCHol6TK8MFjkIQZr6rP3Y9SvgvYb/GODQ2xCEOsf1Umb8bAPkW8i7aTWNY/ra+rzUTt/hm8Gn9l2QGX50/1M9I+pa8/wBJwIoGUPXX+G29gv22WWufZZ9fAEOW/wAhxtvGWWcGAyDfuZ443hpvwxZs2LF+i/RCzPYNkj2H2bETr+4HqYcoJFjzzH7skekP3B4WG8Jw298A5P4k+eWcZZe5MI8N7i+peyhH3wQ2w8a2hBql+zBkLgEUOdnMQD1EktSPhl+9r7TwbQxP8fr5ZyzLPYeJ12W28beGgfN7l7KPsj7rL3J8YYmc7LEO21YQw/zNWVbllkh58Tbzvw93fPvj38M4Z3e8J39GX4L/AOYXTbbbbeDEF7k2Gz+aAdQc7LLAZHwN/wBpuW/VYgfXKhHOc5x3baW/Dq9fNhyRvqWakf1fWEH2H+Lxbv0wHw22wzIi5DDFttsJegkXfGh2x/ULt8AQII+Gz8Nt53+NJj8YgP1wK94uifBnTBhRwKcwRP486pYXjB/7gkLLOQj57e/hvGXjnfllllkk6kSOVY/pBZqSghYP3Z+5B7tfEnnzsV7uz9fTixIOMs5P4D5b/PllkxiJPKvq2/ZfshPAl9oXyTzXeDGCyzjPwmcjACYSeTh3hlP+yRb2bHjCz+qc7/NlnxPCF3eL3Vp7v2X2oDpkPo48OPI/E5ZyPXALxfcv2x71vs48uAPxuSW1twf4R+vERwED8fkx+FFIH5HLLPgMss/LZZ/8d//aAAwDAQACAAMAAAAQxxp4tk6ff1y2uC89IAAAAAAAE8wHPx/jU1t+Erj8e2i19pAAAAAAE8sR/wDhOEmiC4WYtawFwAfbAAAQQQE//AFPvn1S7LqA6jIX6SQUMPDwwwwwz/wVPeAXA6NGJ5OyXjvffTCt6wn7/wDOMP30Xpi1BzCfojBWv3zrIJesYLK8MMPPwiinWbKHHOnWIXbIMMN4AAATz74woIh86CwPiCLvPmP4oMN7ywEEXnHXlEEBObgBjCXOc3j7748HyDSkV3330kEEII8sYl0m0Nm5LL30lSwj133n3/0k0UCMBbQCnNXoH4Q/jVjCBDf2sP8A4V98CYH8/wBcpioQ/Pyp/PPAAAHw/wD8T31CApiMU0s2lQxr3mFFpTzww/OMMG0wJILuCzRyJgRIYwJV3W2NzCoPP6rAyReIyzPMDIlYq5r6akHmMTz7Vc80w41uOSe58SkV99JCBFKaGhHRKCBUEnCTV6D/ACbNBRN9FVZIzvnOYt4UolY99eBrp7XuQhyrl5pdyVOuuaBdYU99hxLp9LgcmvDeOCQlm90VhCXvTaAZtkAZ1ekbjMLir4i/102TT1pa+TgB5EExpF7jGuLr0IONkQeyhhc4iO/UoZFo88BsjbIFGWt7FPScmRZ9wmenE0pdIAYMlxTefqpSjDJLAG3DY1++9ZYYcgMQXrYbjF7bgDEf0sa6FRiGOPzNdthxEnTgAgcv6JISVLCUvXlTiqayRiAFOrHTADakM8nK/wCEqaq4Ocp+wNYaptcTy9rygwndl5Wt3Rh04VMy223XUidQ6mNz+4/mkkzr0cs0jtXLiwnIlbUXfH/34/8A5wAIKCHx+L6Fzzx/+AJ11xwP/8QAHxEBAQEAAwADAQEBAAAAAAAAAQARECExIEBBMFFh/9oACAEDAQE/EFtttglkB3K+Xace3V3Jn0lvGwTBOw8WTohYftvifZ9LbYhIFt4Hk/y6h+T9HY9hHVqR0YWyx7bD3ZEtg+iPAOpR0a+Bxvcr0+iPHJ/kcBwz8D6lr+ioctGTyIccFRHBbrPX6Kjy9kejB+c7HJvLCH6K74dwutumy5tmJNNvP0nVtfmXRuvJLxttliS7d+kPaBfAkMP2T9juDnpLwrGG8JE9/ssD7JBv+l/hsPyDPihlmCvNTPf9ccsg9R+0/wBRQ7ty3jPl7k3t8t/NjzCZkZh7hEW/wAY3k+fzMWYdWY/jtvGxYH5/NtvGSxIs+b8H6S67/SNeX5kEUlpZ/BZ40uv8W287tveX222223lQu0d3VtsP8fzzg2EL7Dsqu4LafDeCutp3zokfyyC2ts/+IB5P/ba/4tF3y+bIgI5tsGMPwPlvHZ2RJNu8ZZwyt6xIJkv+X+NXfQ2Ycgj578Scf5CPID2F9gvlvCF1+HVH+QHsaUYRojnY5z4nGexAfOQscgezMKD+zYYEY4z4b8NjhjfyLMeyoEgWITyU+xlq1kEERxv9s4exalrEbhOawWcH0wk2I0gJHqYrYQWfVGbs8nX1gFkH2sgs+i/0Po//xAAhEQEBAQACAgMBAAMAAAAAAAABABEQITFAIDBBUWBhcf/aAAgBAgEBPxDtBz4g/vB/dojU89PI52INus8oT22ErNUKJ7HpZEy3Zs/6TdHiWTi16fT8Esw20utnB8QWnDVpvovBe463ZN48x8cDRV19FOGx/Z6nhYQ1mCWQbTg9FJO20I7W/FLeLbzFeYL8rFuh6R83i3jH8nfMYiFBIwd+pCTq/q6LDS6OcnrBdpHFlHpAIfkN2u3YxykM9K8R6XjWgv0IYS/ORY42Qtti37gkluzc/su1ls52ERlLSjA/bkHAFiQJG9R3Zwvx2Qj/AG8J9ZCBFg2RT4lNnAMu7ec4Q8fX2WQRYGcPB88ssgkNP1ZZZwOt4Z5yzkjkWDPqyy8Qf2DyerfoyOMvD68smMOTxxllllnALDLxxksUn69h0kJIBOpVnxwgu3mGcvC/XvGK1ZKt/wA4P97FpwXZgh2dlkO/uYaWfDLxG37Fodg/20as2Tw9us/aM1IPmSyyUciPHw66zFNlP3YJSUc5J5EwkyONln0EMiYo2rUn7AElAcON9Pbbz2yw4R7YAdS+u4TiExHbLnRM+ucbL/hn/8QAKRABAQEAAgICAgEEAwEBAQAAAQARITEQQVFhIHGBMFCRoUCxwdHh8P/aAAgBAQABPxDeZGybyxTnhy2Qw8eBZ8DGnMPmfhrmnuGZL7WygP0gLPgjYDfmI7CBdH+ZRN/1Pe8b+/8AMM2zYYR1B37g9Hn8N/4u/hv5iRqty5PPNkAeA4bZMGMyYS9AH57jXXEjoh82En7Ij4JgEP3OqX7nynPrbbq7hvUa6afqAc2GnLEkf7Fsw8B1sABmRFvimJwCQsNj9x8VnBcnhO/ClbpnqQKHqSriyPl8BS8GjBA4tBHmGeoDPB2QCn9iCWHIY0zTVeYH4g5e55WFyJnH3LHG3Va9W9jYLl4QhpCxn4iGPYJcHlzIJrSHTf7CQw8HZGbK5xziTXt4ksRyt1shXfq1ZCf6JdYb1aeB1mcbhN8DPROTddWD/YSGm4Tvd4Jg4B3kPgxwE5bM/PF7vVu+BlHuOPAZnOxbBIz7u3f2HpPmyjNxwynd3hDwGlz5Y4Xw9eGw3KDuXKpePC47cRbpc192nNLR/YGPDw8/8wvZLnIdVNRWCIQhOTDzHB4vC6blXsfAPR53+jv/ABN5uEwFuWNIGtwXuzA7bALa2NzbDddunnDieLV1zu+IWj/mvlc2E5Z6JnvajvNxOIS4vRdKyyyyDmwq4lsVxS3S9ZUY11f2Bc3Fds7J0Py2W7tPIw1fK4YeCyy1gR2W6/Byyx4J3v4MYP7Ap8Wh8raLgJZlj5jglq8bD459nhdngS5lu0uEnitA+Wwk4P7ApcXFh5L/AKLOW3ojhdt0A3pZJ0yPzdIsC0xCWWWGBYiymZ+7pfqGD+wDcibvw29btjnHa3Jl2QD1BnVi6kvqFOpkcJMgyWBJSyy6WbeiU8deMZZ4z/m7HM1+qXUuV7nkxoJ6gzghj1ceMudtA+oHqBYRbuJZQKfc8oXVxDwhgg5bB/sKz2br6njPXLzcCxWApzDLcgOJ5QQ7nyYn1fDiOk74f4n6MerCaWETN98x8L1ZP9hHS4ssnnq1/wBNxz3PhcMkJlzFmwQ83LJxljyLVyfhph5LRh1GUYPgFDsMHG/sWpYQPnCXM4WybvYCwRg8B5yDjLL5scnTZJzEJ4kAmFhf1ch8xkON5i4kZ/2IQc3KMJjYyjcn1LpsPNuhg5USc2uXR4xknw8R3h27FaxU/ULWebIXv6s7jP7GZQy5tTICZ6kQOLiyXm6zKaMA1hDWQHPgDxodG5s0jhW1ujbxLerSDgTv1/YnLniBCHhGxjAJE4jl3zIMumylz1EA8rcEbg5hHgXX4nuyyPP5TqFXEyGf2IKCuWODMUYsXDsZUMzOJEeS9E/JEQsoHJGnC2BJt3DbAmnFweHj3YlwnjIPSY6lOrXw3RFxdXaRE/Hbf+IzZ3RmTlsnA3LEi1CRcOJTpnsEnxC2ijGNSQcMH7iqaMTNbg5Ynkc2I3DJ2AyYh9XaE9WDqI6uUhIij/yd8kiKRkRXPmG6jF+rqAl+iOP6i72jZ3NstzxEu8jHYgQg8EFo+BvhmEtOrh1Yl6STaEzyf8oPJaTXJIYEAQwX1fEj4YOsiZIBbKCKCyQ5tmEHgH4nhifb4QThelthwtFCUsT+nv8AQyyzxPA+rzOHiwlp5NzAlmAPU/tlwEFb4EYGsqwLNDw+6+38ACffAe4DkIvgLTbhmP8AigfiCHy88SJK2B1GF7uskiPbFDNZYaXqJhgp00c3UbGpDJTyn2zfkYE5z/JewJ4ftgJzPuC6V/wmwIMQ8AQ8iZfmyDiE9QFkTwwJEprCABZQZOwG9AydE7RIcanvBDCwUCO7vhJ64+HgB9Nw54fUU+19s4fc8Kf0z8ykIQMg8h+0b01k6kyXNvrwR4t0H3Eze/FhF+2IQMtDkuzF1mIzVWCh/mRt3+Yz1HzePqfpwiHzfr3kv2vtgXzLU/nv47/RGQQRFzH7vQWMWORwXByPERDlt9RIMeKKHEN9o17j7RNmd5ku7Ww+21vTOPcFxm7i93OH9XfO2/mDPBax62Ag+Ldz+M48SR+0+iyH3A6LIREwT9SvWkn/AOsHpG7RyJyJbkuNhfMKEWEhi+pXYTe5enr6mjtojt/ImTJPZcPvFl/S7bbb+GWeTLLLPKQT1AFectyRMELBkcD3C3v2/NxQkQY1DDPaD/EHyf4knIROxO8eTjh4g0L3co74xnud/tPSWSnHHB/M+6xwZPjY8bb/AMMBnhS1xsQRnRFkLi7z0kCBMYxfLfGPtH2jUfaKUvraMM4b8lot1dMDcsP6rT4llwZ57umZxfaU9cfiNtv5v4bbNfwc09eghBIH+Z0Nn+pHAP7mjviSTMPlhAMv+CwZn7uZd2Ptn/Vhsj43Z/8A2kh1j+m7A/xO7ah/NvIWEfWD88R97J2wpCp0kPne0G6y2Xo6hOHiG4oNyH8QXGcGixtht/Jt8lvnbbbZr5xWcmWhFfa+4zAEAQFh9QJgXVl+46CnjUzpr93AG/qSzX7RX/anfVLoBP03wv8AN8sPqQxE/dqe7jzb620W3o2a/wBWn5w3Pm9yMU/xZiZ8rHbb44Fl4V+XmBnE+i3k0TkkV+0lp/RJt87+GTXz3nfwTMM8QWEi+pgEzZtiXdmQuil/6isTdfuNx/gs/Vgubfu5HA/IxS8P13IOsu04TzOxLFh6IxB5zblLFxzS0YP+ouPiytDjktv0Iyttht/FjxvnbfHM7v1n4yo2PZCgsbp2+3YxGSYnJa3oI3fjEubnx78dd36hnGJ41/mcwCW/fw/DawiYl91m/RAmHhEW+oX+PrwL4JcT+ObY3p14BiPHN6/DfGeNmPxXUj4ss+5w8Nk4uLqD/JYYOyDzyMeI8ex6tLhSOpPiSoFwlx8/hg2WbBzGOG/FtL2of4g2OPbExiq67OIfd/BYemw+LkI5O3g/jwIl8F1422yLfG5evwc2tkyeFkJ6YiDfiJluWseA/wD3+IEf4US/pme0zAJ+9mFfZX/ZNDg+bth+mV0l8L4T5W3yEBhGx8BaW8+9P3fWfxdpv1F9xhBM2nw3fN9Fg/NzHq59SwT2i5xxCxtsPk8h45LdsnYt+pjBB5m1ncA/HzOuYIfux/7+yIkBgBgE8JKEmuLbeId7Et4PyXorPS0/3COH+uGdB93mn+pXAH4XGD4X+b3kHpj7Q77jaMRAYf8Ar/yaEeAt6cnsp+pOiqtiQR4bbabmR8W4IH92bYpPRcCOYtvuEbLI3fHu23JeLLcZ58cSaWeDusrkuukaoMY9q/8AzI8ZCHUTa+rX1fTbwO16hX9TCh+z/q5g56yf8kZQXw2Nz/uZv/bCNf8ALAOt9mD/ADcilpiavtb0Y/OP/k4V/wBBj/WQQX1Ifs7Iz6jFgN9P8Qrbw37RSffxImfMGtmXbqDeWwyz4jjvz0yybAykt7h78O3VzfaeGzqeEPxanFwsJr9xIfIJCiTPtb44+ISG9WnqW9SnqBIfMOWQhxfuD/Sq+5XYCwM+oRgQeEAgjwjBrY4nB9JzG06kfb4Z50vT8Quq/TAzVCNcaJGZeyOLeebprbw3rZNe44Inbm12xW5OXIM4g5xuPUbtzZp4MTmx7kLxM/i2XMryH6uH5n3A8YfZF8EhOAwy8AYGwSEdXPOgF3inHoQEAfBEHVjZkuTCOrxcjnsnmsetkeclsS/VgLH27Kh6fiwY+pXhcZG+4G57ksT9QacFv8x3Z7if3zb/AJl/idfDE+rcwty52xz3J6J5YXDrmMIWvFwvx9wDQR6TybDOmd5P82b2lYHLmgzOhfA3xjdS4tGprAeooZaT4KxCRF4vlmbk14vgTPVo9Wrq3GWR89WGccwHfH7g4Pf8XK5JK8Ma+IfZmfFvONwxx2Hned+Lh9kL2Zc9O7dcEmrxduGYw/LjLpPltOkJhv8A2X/dhYZ3n/yfdhyHH7ntOe5XH/8A3cfEOHyzMf6sQP6Yp4CDJ0Uuqf8AmFqowOr9woGHgzwg8GPckxNm3PEd2MJvCHjSzcjOHCjbc4gkMAzIhOOYg5Ofq4PqH08Xr5lc+/1I/SQmGXy95POZxB23icPX+ojt1B6bU4jnl4JAGtu4HhtPguP/AObOepfqQk9wN3m3XObdUO7XeL7RZJj2sZ85l65f7hYOe1aWaCX0NygP6ikUH2zAGxEEjwa0nVs9Xm1S3wlUwJHTW/zAhn1A2A+r5bH1Yfq0cweyNzPn3cn1AP382jPZYfDGZOJ8x8v9MFevqesDp7ywz2P7n0/3aB/9uN493fGcQd/UC8p/m/h/zMl75kHwAlsblt6tuic9XyE7dnLpxJ8L63SzwD56bBGvsg0mZPqL9l9nik7Z9zNIsJVdYmpw55lrOnfuc4hwRqAdwQB3YEeb1zAnXX3G7ucTy+71zaPXEQN+U8OWgcBe4WdQr2Th2YZKFjeLeM21+/8AM9We/V3MeHgiLOofSYdTvV34SHq+qNiPU3GuIGX1Fct8FA+kV0m4OS1PlAP4Ey068f8A3cwpxGWQQeBqDnmDjyOreLbuTHTq5/i7fM53LsG82/4lb9Jdepch0sst+/Hfl4u7OZPBru+mF9XxJ3qx9Ti5dGWdNh0rT2kOVndIxqwlC6dBPNiej0RgKQgMsyIauInTHg7twhMtjJObcLn1HPdnMR4zni5s262HbNssPGc+GYObObJ/VknF1nUmOBMcAylh8XNEOJO/HTNPKe2ADnNn2WccLIHbLPAWW5Ce7j143Lu3iLL3bCzz4PPvx2yJ4LmJLPCSQWeM8GZ4fDdsNv7RJwj0kfEkHSfHJ8Fh1fh+bPOIscQB1BIZBxZZZxZZHki9eCLjwXHh8jPLB42z8HznnLPBiQ4mVMrxK60tnDhuPBJlEa2IcRg4sbGDiCCz8DyFnhbsuLPw22fGWWRk+M/FPBZZ4yfDOPwg9JPids5pj6sTiI9RFhBBB4zxnk87b+Ow/i3dlsTc/k2WQWWWWSSTCbWQ+rC9QT1H8WfhDIII8Ftv4H47FkTZ+LEvn1+WWWfnlnka/S/SB8RSEyDzn5BH4NkeMs8LbHl/o5+GR4zxlllllllngQILI8H5BZ/R3w/l68b53yeHyeD8Tr+j78HgjrweT8SPwI/D34Yny+P/2Q=='
        ], $this->getHeaderAuthorization());

        $this->seeStatusCode(HttpStatus::CREATED);

        $data = $this->data();

        $this->assertNotNull($data['id']);

        $file = File::find($data['id']);
        $this->assertNotNull($file);
        $this->assertNull($file->url);

        $this->json('PATCH', '/file/' . $file->id . '/product/' . $product->id . '/image', [
            'status' => true
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NO_CONTENT);

        $file->refresh();
        $this->assertNotNull($file->url);
    }
}
