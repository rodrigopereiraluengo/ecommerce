<?php

use App\Domains\TransactionDomain;
use App\Exceptions\ValidatorException;
use App\Models\Transaction;
use Faker\Factory;

class TransactionTest extends TestCase
{
    public function testCreateEmptyValidation()
    {
        $transactionDomain = app(TransactionDomain::class);
        try {
            $transactionDomain->insert([]);
        } catch (Exception $exception) {
            $this->assertInstanceOf(ValidatorException::class, $exception);
            $this->assertArrayHasKey('amount', $exception->errors);
            $this->assertArrayHasKey('payment_method', $exception->errors);
            $this->assertEquals('The amount field is required.', $exception->errors['amount'][0]);
            $this->assertEquals('The payment method field is required.', $exception->errors['payment_method'][0]);
        }
    }

    public function testCreateTypeAmountValidation()
    {
        $transactionDomain = app(TransactionDomain::class);
        try {
            $transactionDomain->insert([
                'amount' => 'sss'
            ]);
        } catch (Exception $exception) {
            $this->assertInstanceOf(ValidatorException::class, $exception);
            $this->assertArrayHasKey('amount', $exception->errors);
            $this->assertEquals('The amount must be a number.', $exception->errors['amount'][0]);
        }
    }

    public function testCreateValueAmountValidation()
    {
        $transactionDomain = app(TransactionDomain::class);
        try {
            $transactionDomain->insert([
                'amount' => '-10'
            ]);
        } catch (Exception $exception) {
            $this->assertInstanceOf(ValidatorException::class, $exception);
            $this->assertArrayHasKey('amount', $exception->errors);
            $this->assertEquals('The amount must be at least 1.', $exception->errors['amount'][0]);
        }
    }

    public function testCreatePaymentMethodTypeValidation()
    {
        $transactionDomain = app(TransactionDomain::class);
        try {
            $transactionDomain->insert([
                'payment_method' => 'sdsds'
            ]);
        } catch (Exception $exception) {
            $this->assertInstanceOf(ValidatorException::class, $exception);
            $this->assertArrayHasKey('payment_method', $exception->errors);
            $this->assertEquals('The selected payment method is invalid.', $exception->errors['payment_method'][0]);
        }
    }

    public function testCreateCreditCardEmptyValidation()
    {
        $transactionDomain = app(TransactionDomain::class);
        try {
            $transactionDomain->insert([
                'payment_method' => Transaction::PAYMENT_METHOD_CREDIT_CARD
            ]);
        } catch (Exception $exception) {
            $this->assertInstanceOf(ValidatorException::class, $exception);
            $this->assertArrayHasKey('card_holder_name', $exception->errors);
            $this->assertEquals('The card holder name field is required when payment method is credit_card.', $exception->errors['card_holder_name'][0]);

            $this->assertArrayHasKey('card_number', $exception->errors);
            $this->assertEquals('The card number field is required when payment method is credit_card.', $exception->errors['card_number'][0]);

            $this->assertArrayHasKey('card_expiration_date', $exception->errors);
            $this->assertEquals('The card expiration date field is required when payment method is credit_card.', $exception->errors['card_expiration_date'][0]);

            $this->assertArrayHasKey('card_cvv', $exception->errors);
            $this->assertEquals('The card cvv field is required when payment method is credit_card.', $exception->errors['card_cvv'][0]);

            $this->assertArrayHasKey('soft_descriptor', $exception->errors);
            $this->assertEquals('The soft descriptor field is required when payment method is credit_card.', $exception->errors['soft_descriptor'][0]);
        }
    }

    public function testCreateCreditCard()
    {
        $transactionDomain = app(TransactionDomain::class);
        $faker = Factory::create('pt_BR');
        try {
            $transactionDomain->insert([
                'payment_method' => Transaction::PAYMENT_METHOD_CREDIT_CARD,

            ]);
        } catch (Exception $exception) {
            $this->assertInstanceOf(ValidatorException::class, $exception);
            $this->assertArrayHasKey('card_hash', $exception->errors);
            //
            //$this->assertEquals('The card hash field is required when payment method is credit_card.', $exception->errors['card_hash'][0]);
            //$this->assertArrayHasKey('soft_descriptor', $exception->errors);
            //$this->assertEquals('The soft descriptor field is required when payment method is credit_card.', $exception->errors['soft_descriptor'][0]);
        }
    }
}
