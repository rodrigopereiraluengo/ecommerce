<?php

use App\Enums\HttpStatus;
use App\Enums\PostgreSQL;
use App\Models\Directory;
use App\Models\File;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File as StorageFile;
use Illuminate\Support\Str;


class FileTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    protected static function path()
    {
        return storage_path('app/tests/file');
    }

    public function model()
    {
        $unserialize = unserialize(StorageFile::get(self::path()));
        $this->assertNotNull($unserialize);

        $id = $unserialize['id'];

        $model = File::find($id);
        $this->assertNotNull($model);

        return $model;
    }

    public function testEmptyInsert()
    {
        $this->json('POST', '/file', [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        return $this;
    }

    public function testMaxInsert()
    {
        $this->json('POST', '/file', [
            'file' => base64_encode(Str::random(1000000 * 15)),
            'name' => Str::random(256)
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();

        $this->assertNotNull($data['file.size']);
        $this->assertNotNull($data['name']);
        return $this;
    }

    public function testInsert()
    {
        $file = base64_encode(Str::random(1000000 * 2));
        $response = $this->json('POST', '/file', [
            'file' => $file
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::CREATED);
        $data = $this->data();

        $this->assertNotNull($data['id']);

        $this->assertNotFalse(StorageFile::put(self::path(), serialize($data)));

        $file = $this->model();
        $this->assertNotNull($file);

        $this->assertTrue(Storage::exists($file->path));

        return $data;
        return $this;
    }

    public function testUpdate()
    {
        $file = $this->model();
        $file_content = base64_encode(Str::random(1000000 * 4));
        $response = $this->json('PUT', '/file/' . $file->id, [
            'file' => $file_content
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NO_CONTENT);

        $updated = $this->model();

        $this->assertTrue(Storage::exists($updated->path));
        $this->assertFalse(Storage::exists($file->path));
        return $this;
    }

    public function testInserValidateName()
    {
        $file = $this->model();
        $file_content = base64_encode(Str::random(1000000 * 4));
        $response = $this->json('POST', '/file/', [
            'file' => $file_content,
            'name' => $file->name
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();

        $this->assertNotNull($data['name']);
        return $this;
    }

    public function testValidateNameAnotherDirectoryId()
    {
        $directory = app(DirectoryTest::class)->model();
        $file = $this->model();
        $file_content = base64_encode(Str::random(1000000 * 4));
        $this->json('POST', '/file', [
            'directory_id' => $directory->id,
            'file' => $file_content,
            'name' => $file->name
        ], $this->getHeaderAuthorization());

        $this->seeStatusCode(HttpStatus::CREATED);
        $data = $this->data();

        $this->assertNotNull($data['id']);
        return $this;
    }

    public function testFileOtherUserUpdate()
    {
        $file = $this->model();
        $file_content = base64_encode(Str::random(1000000 * 4));
        $this->json('PUT', '/file/' . $file->id, [
            'file' => $file_content,
            'name' => $file->name
        ], $this->secondaryUser()->getHeaderAuthorization());

        $this->seeStatusCode(HttpStatus::NOT_FOUND);

        return $this;
    }

    public function testStatusPacth()
    {
        $file = $this->model();
        $file_content = base64_encode(Str::random(1000000 * 4));
        $this->json('PATCH', '/file/' . $file->id, [
            'status' => !$file->status
        ], $this->primaryUser()->getHeaderAuthorization());

        $this->seeStatusCode(HttpStatus::NO_CONTENT);

        return $this;
    }

    public function testPathGet()
    {
        $file = $this->model();
        $this->json('GET', '/file/' . $file->id . '/' . $file->path, $this->primaryUser()->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::OK);
    }

}
