<?php

use App\Enums\HttpStatus;
use App\Models\Subscription;
use App\Models\User;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\File;

class SubscriptionTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    protected static function subscription_path_file()
    {
        return storage_path('app/tests/api/subscription');
    }

    public function testEmptyUserFieldPost()
    {
        $this->json('POST', '/subscription', []);
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $response = $this->getJsonResponse();
        $this->assertObjectHasAttribute('user', $response);
    }

    public function testEmptyUserEmailEmptyUserPasswordEmptyAndConfirmation()
    {
        $this->json('POST', '/subscription', [
            'user' => [
                'email' => '',
                'password' => ''
            ]
        ]);
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $response = $this->getJsonResponse();

        $this->assertObjectHasAttribute('user', $response);
        $this->assertObjectHasAttribute('email', $response->user);
        $this->assertObjectHasAttribute('password', $response->user);
        $this->assertObjectHasAttribute('password_confirmation', $response->user);
    }

    public function testEmailVerification()
    {
        $user = User::first();
        $this->assertNotNull($user);

        $this->post('/subscription', [
            'user' => [
                'email' => $user->email
            ]
        ]);
        $this->seeStatusCode(HttpStatus::BAD_REQUEST);

        $response = $this->getJsonResponse();
        $this->assertObjectHasAttribute('user', $response);
        $this->assertObjectHasAttribute('email', $response->user);
        $this->assertIsArray($response->user->email);
    }

    public function testUserPost()
    {
        // TextBoxMaskTest Successful
        $faker = app(Faker::class);
        $password = $faker->password;

        $this->post('/subscription', [
            'user' => [
                'email' => $faker->email,
                'password' => $password,
                'password_confirmation' => $password
            ]
        ]);

        $response = $this->getJsonResponse();

        $this->seeStatusCode(HttpStatus::CREATED);

        $this->assertNotFalse(File::put(self::subscription_path_file(), serialize($response)));
    }

    public function testPutUser()
    {
        $subscription = unserialize(File::get(self::subscription_path_file()));
        $this->assertNotNull($subscription);

        $faker = app(Faker::class);
        $password = $faker->password;

        $this->put('/subscription/' . $subscription->id, [
            'user' => [
                'email' => $faker->email,
                'password' => $password,
                'password_confirmation' => $password
            ]
        ]);

        $this->seeStatusCode(HttpStatus::OK);
    }

    public function testPutPerson()
    {
        $subscription = unserialize(File::get(self::subscription_path_file()));
        $this->assertNotNull($subscription);

        $this->put('/subscription/' . $subscription->id, [
            'step' => Subscription::STEP_PERSON,
            'person' => [
                'name' => 'Rodrigo Pereira Luengo',
                'company_name' => '',
                'type' => 'F',
                'born_at' => '1983-10-29',
                'sex' => 'M',
                'document_number' => '330.088.868-92',
                'addresses' => [
                    [
                        'name' => 'Comercial',
                        'country_id' => 'br',
                        'address_zipcode' => '08940-000',
                        'address_street' => 'Rua Virgilina da Conceição Camargo',
                        'address_street_number' => '58',
                        'address_neighborhood' => 'Jd. Alvorada',
                        'state_id' => 'SP',
                        'city_id' => 5029,
                    ]
                ]
            ]
        ]);

        $this->seeStatusCode(HttpStatus::OK);
    }

    public function testPutTransactionBoleto()
    {
        $subscription = unserialize(File::get(self::subscription_path_file()));
        $this->assertNotNull($subscription);

        $this->put('/subscription/' . $subscription->id, [
            'step' => Subscription::STEP_TRANSACTION,
            'transaction' => [
                'payment_method' => 'boleto',
            ]
        ]);

        $this->assertResponseStatus(HttpStatus::OK);
    }
}
