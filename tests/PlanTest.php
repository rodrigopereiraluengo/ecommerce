<?php

use App\Models\Plan;

class PlanTest extends TestCase
{
    public function testInsert()
    {
        $plan = new Plan([
            'name' => 'Plan sem nome',
            'status' => true,
            'amount' => 9.99
        ]);
        $plan->save();
    }

    public function testUpdate()
    {
        $plan = Plan::first();
        $plan->name = 'Plan com nome';
        $plan->save();
    }
}
