<?php

use App\Enums\HttpStatus;
use App\Models\Directory;
use App\Models\User;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class DirectoryTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    protected static function path()
    {
        return storage_path('app/tests/directory');
    }

    public function model()
    {
        $unserialize = unserialize(File::get(self::path()));
        $this->assertNotNull($unserialize);

        $id = $unserialize['id'];

        $model = Directory::find($id);
        $this->assertNotNull($model);

        return $model;
    }

    public function testEmptyInsert()
    {
        $this->json('POST', '/directory', [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        return $this;
    }

    public function testMaxLengthInsert()
    {
        $this->json('POST', '/directory', [
            'name' => Str::random(46),
            'description' => Str::random(4001)
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['name']);
        $this->assertNotNull($data['description']);
        return $this;
    }

    public function testValidateTypeInsert()
    {
        $this->json('POST', '/directory', [
            'status' => 'boolean',
            'metadata' => 'array'
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['status']);
        $this->assertNotNull($data['metadata']);
        return $this;
    }

    public function testInsert()
    {
        $this->json('POST', '/directory', [
            'status' => true,
            'name' => 'Directory ' . Str::random(24),
            'description' => Str::random(4000),
            'metadata' => [
                'subsdirectory' => false,
                'version' => 1.0
            ]
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::CREATED);

        $data = $this->data();

        $this->assertNotNull($data['id']);

        $this->assertNotFalse(File::put(self::path(), serialize($data)));
        return $this;
    }

    public function testUpdate()
    {
        $directory = $this->model();
        $directory_name = $directory->name;
        $name = 'Directory ' . Str::random(24);
        $this->json('PUT', '/directory/' . $directory->id, [
            'name' => $name,
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NO_CONTENT);

        $directory = $this->model();
        $this->assertTrue($directory_name !== $directory->name);
        $this->assertTrue($directory->name == $name);
        return $this;
    }

    public function testValidateName()
    {
        $directory = $this->model();
        $this->json('POST', '/directory', [
            'name' => $directory->name,
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();

        $this->assertNotNull($data['name']);
        return $this;
    }

    public function testValidateNameAnotherDirectoryId()
    {
        $directory = $this->model();
        $this->json('POST', '/directory', [
            'directory_id' => $directory->id,
            'name' => $directory->name,
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::CREATED);
        $data = $this->data();

        $this->assertNotNull($data['id']);
        return $this;
    }

    public function testValidateDirectoryIdInvalidUpdate()
    {
        $directory = $this->model();

        $this->json('PUT', '/directory/' . $directory->id, [
            'directory_id' => $directory->id,
            'name' => 'Directory ' . Str::random(24),
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['directory_id']);
        return $this;
    }

    public function testDirectoryIdOtherUserInsert()
    {
        $this->secondaryUser()->testInsert();

        $otherUserDirectory = $this->model();

        $this->json('POST', '/directory', [
            'directory_id' => $otherUserDirectory->id,
            'name' => 'Directory ' . Str::random(24),
        ], $this->primaryUser()->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['directory_id']);
        return $this;
    }

    public function testNotFoundGet()
    {
        $this->json('GET', '/directory/' . rand(10000, 99999), [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NOT_FOUND);
        return $this;
    }

    public function testWrongUserGet()
    {
        $this->testInsert();
        $directory = $this->model();

        $this->json('GET', '/directory/' . $directory->id, [], $this->secondaryUser()->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NOT_FOUND);
        return $this;
    }

    public function testGet()
    {
        $directory = $this->model();

        $this->json('GET', '/directory/' . $directory->id, [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::OK);

        $data = $this->data();

        $this->assertNotNull($data['id']);
        $this->assertArrayHasKey('directory_id', $data);
        $this->assertArrayHasKey('directory_name', $data);
        $this->assertArrayHasKey('status', $data);
        $this->assertNotNull($data['status_name']);
        $this->assertNotNull($data['name']);
        $this->assertArrayHasKey('description', $data);
        $this->assertArrayHasKey('metadata', $data);
        $this->assertNotNull($data['total_directories']);
        $this->assertNotNull($data['total_files']);
        $this->assertNotNull($data['created_at']);
        $this->assertNotNull($data['updated_at']);
        return $this;
    }

    public function testNotFoundDelete()
    {
        $this->json('DELETE', '/directory/' . rand(10000, 99999), [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NOT_FOUND);
        return $this;
    }

    public function testDirectoryIdInsert()
    {
        $directory = $this->model();

        $this->json('POST', '/directory', [
            'directory_id' => $directory->id,
            'name' => 'Directory ' . Str::random(24),
        ], $this->getHeaderAuthorization());

        $this->seeStatusCode(HttpStatus::CREATED);

        $data = $this->data();

        $this->assertNotNull($data['id']);

        $newDirectory = Directory::find($data['id']);
        $this->assertNotNull($newDirectory);
        $this->assertEquals($newDirectory->directory_id, $directory->id);
        return $this;
    }

    public function testDirectoryIdDelete()
    {
        $directory = $this->model();

        $this->json('DELETE', '/directory/' . $directory->id, [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::BAD_REQUEST);
        return $this;
    }

    public function testDelete()
    {
        $user = $this->getUserAdmin();
        $subscription_id = $user->subscription_id;

        $directory_id = DB::selectOne("
            select id
            from directories c
            where c.subscription_id = :subscription_id
              and c.id not in(
                  select directory_id
                  from directories
                  where subscription_id = :subscription_id
                    and directories.directory_id = c.id
                  union select directory_id
                  from files
                  where subscription_id = :subscription_id
                    and files.directory_id = c.id
              )", ['subscription_id' => $subscription_id])->id ?? null;

        $this->assertNotNull($directory_id);

        $this->json('DELETE', '/directory/' . $directory_id, [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NO_CONTENT);
        return $this;
    }

    public function testInvalidSearch()
    {
        $this->json('GET', '/directory?q=' . Str::random(50) . '&directory_id=' . rand(99999, 999999) . '&status=' . rand(99999, 999999) . '&total_directories=a&total_files=-1&page=0&orderBy=a', $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();

        $this->assertNotNull($data['q']);
        $this->assertNotNull($data['directory_id']);
        $this->assertNotNull($data['status']);
        $this->assertNotNull($data['total_directories']);
        $this->assertNotNull($data['total_files']);
        $this->assertNotNull($data['page']);
        $this->assertNotNull($data['orderBy']);
        return $this;
    }

    public function testInvalidOrderByDirectionSearch()
    {
        $this->json('GET', '/directory?orderBy[status_name]=a', $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();
        $this->assertNotNull($data['orderBy.status_name']);
        return $this;
    }

    public function testInvalidOrderByColumnSearch()
    {
        $this->json('GET', '/directory?orderBy[aaa]=asc', $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();
        $this->assertNotNull($data['orderBy']);
        return $this;
    }

    public function testPaginationSearch()
    {
        $this->json('GET', '/directory?page=1', $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::OK);
        $data = $this->data();
        $this->assertNotNull($data['current_page']);
        $this->assertNotNull($data['data']);
        $this->assertArrayHasKey('first_page_url', $data);
        $this->assertNotNull($data['from']);
        $this->assertNotNull($data['last_page']);
        $this->assertArrayHasKey('next_page_url', $data);
        $this->assertNotNull($data['path']);
        $this->assertNotNull($data['per_page']);
        $this->assertArrayHasKey('prev_page_url', $data);
        $this->assertNotNull($data['to']);
        $this->assertNotNull($data['total']);
        return $this;
    }

    /*public function testMove()
    {
        $user = $this->getUserAdmin();
        $subscription_id = $user->subscription_id;

        $service = app(\App\Services\DirectoryService::class);

        $directory_0 = $service->save([
            'subscription_id' => $subscription_id,
            'name' => 'Directory Move 0 ' . rand(1000, 9999)
        ]);
        $this->assertNotNull($directory_0);

        $directory_1 = $service->save([
            'subscription_id' => $subscription_id,
            'name' => 'Directory Move 1 ' . rand(1000, 9999)
        ]);
        $this->assertNotNull($directory_1);

        $directory_2 = $service->save([
            'subscription_id' => $subscription_id,
            'name' => 'Directory Move 2 ' . rand(1000, 9999)
        ]);
        $this->assertNotNull($directory_2);



        $this->json('PUT', '/directory/' . $directory_0->id . '/move', [
            'directory_id' => null,
            'after_directory_id' => $directory_2->id,
        ], $this->getHeaderAuthorization());

        $directory_0->refresh();
        $this->assertTrue($directory_0->after_directory_id === $directory_2->id);

        $this->json('PUT', '/directory/' . $directory_0->id . '/move', [
            'directory_id' => null,
            'after_directory_id' => null,
        ], $this->getHeaderAuthorization());

        $directory_0->refresh();

        var_dump($directory_0->position);
        //$this->assertTrue($directory_0->position === 0);
        $this->assertNull($directory_0->after_directory_id);
    }*/
}
