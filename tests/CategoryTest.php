<?php

use App\Enums\HttpStatus;
use App\Models\Category;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CategoryTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    protected static function path()
    {
        return storage_path('app/tests/category');
    }

    public function model()
    {
        $unserialize = unserialize(File::get(self::path()));
        $this->assertNotNull($unserialize);

        $id = $unserialize['id'];

        $model = Category::find($id);
        $this->assertNotNull($model);

        return $model;
    }

    public function testEmptyInsert()
    {
        $this->json('POST', '/category', [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        return $this;
    }

    public function testMaxLengthInsert()
    {
        $this->json('POST', '/category', [
            'name' => Str::random(46),
            'description' => Str::random(4001)
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['name']);
        $this->assertNotNull($data['description']);
        return $this;
    }

    public function testValidateTypeInsert()
    {
        $this->json('POST', '/category', [
            'status' => 'boolean',
            'metadata' => 'array'
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['status']);
        $this->assertNotNull($data['metadata']);
        return $this;
    }

    public function testInsert()
    {
        $this->json('POST', '/category', [
            'status' => true,
            'name' => 'Category ' . Str::random(24),
            'description' => Str::random(4000),
            'metadata' => [
                'subscategory' => false,
                'version' => 1.0
            ]
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::CREATED);

        $data = $this->data();

        $this->assertNotNull($data['id']);

        $this->assertNotFalse(File::put(self::path(), serialize($data)));
        return $this;
    }

    public function testUpdate()
    {
        $category = $this->model();
        $category_name = $category->name;
        $name = 'Category ' . Str::random(24);
        $this->json('PUT', '/category/' . $category->id, [
            'name' => $name,
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NO_CONTENT);

        $category = $this->model();
        $this->assertTrue($category_name !== $category->name);
        $this->assertTrue($category->name == $name);
        return $this;
    }

    public function testValidateName()
    {
        $category = $this->model();
        $this->json('POST', '/category', [
            'name' => $category->name,
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();

        $this->assertNotNull($data['name']);
        return $this;
    }

    public function testValidateNameAnotherCategoryId()
    {
        $category = $this->model();
        $this->json('POST', '/category', [
            'parent_category_id' => $category->id,
            'name' => $category->name,
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::CREATED);
        $data = $this->data();

        $this->assertNotNull($data['id']);
        return $this;
    }

    public function testValidateCategoryIdInvalidUpdate()
    {
        $category = $this->model();

        $this->json('PUT', '/category/' . $category->id, [
            'parent_category_id' => $category->id,
            'name' => 'Category ' . Str::random(24),
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['parent_category_id']);
        return $this;
    }

    public function testCategoryIdOtherUserInsert()
    {
        $this->secondaryUser()->testInsert();

        $otherUserCategory = $this->model();

        $this->json('POST', '/category', [
            'parent_category_id' => $otherUserCategory->id,
            'name' => 'Category ' . Str::random(24),
        ], $this->primaryUser()->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);

        $data = $this->data();

        $this->assertNotNull($data['parent_category_id']);
        return $this;
    }

    public function testNotFoundGet()
    {
        $this->json('GET', '/category/' . rand(10000, 99999), [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NOT_FOUND);
        return $this;
    }

    public function testWrongUserGet()
    {
        $this->testInsert();
        $category = $this->model();

        $this->json('GET', '/category/' . $category->id, [], $this->secondaryUser()->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NOT_FOUND);
        return $this;
    }

    public function testGet()
    {
        $category = $this->model();

        $this->json('GET', '/category/' . $category->id, [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::OK);

        $data = $this->data();

        $this->assertNotNull($data['id']);
        $this->assertArrayHasKey('parent_category_id', $data);
        $this->assertArrayHasKey('parent_category_name', $data);
        $this->assertArrayHasKey('status', $data);
        $this->assertNotNull($data['status_name']);
        $this->assertNotNull($data['name']);
        $this->assertArrayHasKey('description', $data);
        $this->assertArrayHasKey('metadata', $data);
        $this->assertNotNull($data['total_categories']);
        $this->assertNotNull($data['total_products']);
        $this->assertNotNull($data['created_at']);
        $this->assertNotNull($data['updated_at']);
        return $this;
    }

    public function testNotFoundDelete()
    {
        $this->json('DELETE', '/category/' . rand(10000, 99999), [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NOT_FOUND);
        return $this;
    }

    public function testCategoryIdInsert()
    {
        $category = $this->model();

        $this->json('POST', '/category', [
            'parent_category_id' => $category->id,
            'name' => 'Category ' . Str::random(24),
        ], $this->getHeaderAuthorization());

        $this->seeStatusCode(HttpStatus::CREATED);

        $data = $this->data();

        $this->assertNotNull($data['id']);

        $newCategory = Category::find($data['id']);
        $this->assertNotNull($newCategory);
        $this->assertEquals($newCategory->parent_category_id, $category->id);
        return $this;
    }

    public function testCategoryIdDelete()
    {
        $category = $this->model();

        $this->json('DELETE', '/category/' . $category->id, [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::BAD_REQUEST);
        return $this;
    }

    public function testDelete()
    {
        $user = $this->getUserAdmin();
        $subscription_id = $user->subscription_id;

        $parent_category_id = DB::selectOne("
            select id
            from categories c
            where c.subscription_id = :subscription_id
              and c.id not in(
                  select parent_category_id
                  from categories
                  where subscription_id = :subscription_id
                    and categories.parent_category_id = c.id
                  union select parent_category_id
                  from products
                  where subscription_id = :subscription_id
                    and products.category_id = c.id
              )", ['subscription_id' => $subscription_id])->id ?? null;

        $this->assertNotNull($parent_category_id);

        $this->json('DELETE', '/category/' . $parent_category_id, [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::NO_CONTENT);
        return $this;
    }

    public function testInvalidSearch()
    {
        $this->json('GET', '/category', [
            'q' => Str::random(50),
            'parent_category_id' => rand(99999, 999999),
            'status' => rand(99999, 999999),
            'total_categories' => '@',
            'total_products' => -1,
            'page' => 0,
            'orderBy' => 'a'
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();

        $this->assertNotNull($data['q']);
        $this->assertNotNull($data['parent_category_id']);
        $this->assertNotNull($data['status']);
        $this->assertNotNull($data['total_categories']);
        $this->assertNotNull($data['total_products']);
        $this->assertNotNull($data['page']);
        $this->assertNotNull($data['orderBy']);
        return $this;
    }

    public function testInvalidOrderByDirectionSearch()
    {
        $this->json('GET', '/category', [
            'orderBy' => [
                'status_name' => 'a'
            ]
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();
        $this->assertNotNull($data['orderBy.status_name']);
        return $this;
    }

    public function testInvalidOrderByColumnSearch()
    {
        $this->json('GET', '/category', [
            'orderBy' => [
                'aaa' => 'asc'
            ]
        ], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::UNPROCESSABLE_ENTITY);
        $data = $this->data();
        $this->assertNotNull($data['orderBy']);
        return $this;
    }

    public function testPaginationSearch()
    {
        $this->json('GET', '/category', [
            'page' => 1
        ], $this->getHeaderAuthorization());
        $this->assertResponseOk();

        $this->seeJsonStructure([
            'current_page',
            'data',
            'first_page_url',
            'from',
            'last_page',
            'last_page_url',
            'next_page_url',
            'path',
            'per_page',
            'prev_page_url',
            'to',
            'total',
        ]);
        return $this;
    }

    public function testMove()
    {
        $this->testInsert();
        $category_0 = $this->model();
        $this->assertNotNull($category_0);

        $this->testInsert();
        $category_1 = $this->model();
        $this->assertNotNull($category_1);

        $this->testInsert();
        $category_2 = $this->model();
        $this->assertNotNull($category_2);

        $this->json('PATCH', '/category/' . $category_0->id, [
            'parent_category_id' => null,
            'previous_category_id' => $category_2->id,
        ], $this->getHeaderAuthorization());

        $this->assertResponseStatus(HttpStatus::NO_CONTENT);

        $category_0->refresh();
        $this->assertTrue($category_0->previous_category_id === $category_2->id);

        $this->json('PATCH', '/category/' . $category_0->id, [
            'parent_category_id' => null,
            'previous_category_id' => null,
        ], $this->getHeaderAuthorization());

        $category_0->refresh();

        $this->assertNull($category_0->previous_category_id);
        return $this;
    }

    public function testDefaultTrueIndex()
    {
        $response = $this->json('GET', '/category', ['page' => 1, 'default' => true], $this->getHeaderAuthorization());
        $this->assertResponseOk();
        $response->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'subscription_id',
                    'parent_category_id',
                    'category_name',
                    'status',
                    'status_name',
                    'name',
                    'description',
                    'position',
                    'total_categories',
                    'total_products',
                    'created_at',
                    'updated_at',
                ]
            ]
        ]);
        $data = $response->data();
        foreach ($data['data'] as $category) {
            $this->assertNull($category['subscription_id']);
        }
    }

    public function testDefaultFalseIndex()
    {
        $response = $this->json('GET', '/category', ['page' => 1, 'default' => false], $this->getHeaderAuthorization());
        $this->assertResponseOk();
        $response->seeJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'subscription_id',
                    'parent_category_id',
                    'parent_category_name',
                    'status',
                    'status_name',
                    'name',
                    'description',
                    'position',
                    'total_categories',
                    'total_products',
                    'created_at',
                    'updated_at',
                ]
            ]
        ]);
        $data = $response->data();
        foreach ($data['data'] as $category) {
            $this->assertNotNull($category['subscription_id']);
        }
    }

    public function testSyncCategory()
    {
        $category = Category::defaultSelect()->withCategories()->whereNull('categories.subscription_id')->where('categories.name', 'Medicamentos')->first();
        $this->assertNull($category);

//        if ($category->subscription_id === null) {
//            $replicated = $category->replicate();
//        }
//        $category = Category::defaultSelect()->whereNull('categories.subscription_id')->first();
//
//        if ($default_category->subscription_id === null) {
//            $category = $default_category->replicate();
//            $category->status = true;
//
//        }


        /*$categories = Category::query()->whereNull('subscription_id')->limit(10)->get()->map(fn($category) => [$category->id => ]);

        $response = $this->json('POST', '/category/sync', [
            'id' => $categories->toArray()
        ], $this->getHeaderAuthorization());
        $this->assertResponseOk();

        $unloaded = [
            '1' => []
        ];

        $loaded = [
            '2' => [
                '3' => []
            ]
        ];*/
    }
}
