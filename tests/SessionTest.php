<?php

use App\Enums\HttpStatus;
use App\Models\Subscription\Session;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;

class SessionTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    public function testEmptyType()
    {
        $this->json('POST', '/session', [

        ], $this->getHeaderAuthorization());

        $this->assertResponseStatus(HttpStatus::UNPROCESSABLE_ENTITY);
    }

    public function testInsert()
    {
        $this->json('POST', '/session', [
            'type' => Session::PRODUCT['type']
        ], $this->getHeaderAuthorization());

        $this->assertResponseStatus(HttpStatus::CREATED);
    }

    public function testSearch()
    {
        $this->json('GET', '/session', [], $this->getHeaderAuthorization());
        $this->assertResponseOk();
    }
}
