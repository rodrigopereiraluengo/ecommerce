<?php

use App\Enums\HttpStatus;
use App\Traits\Test\AuthTestTrait;
use App\Traits\Test\RestTrait;

class AuthTest extends TestCase
{
    use AuthTestTrait, RestTrait;

    public function testPost()
    {
        $this->assertNotEmpty($this->getToken($this->getEmail(), $this->getPassword()));
    }

    public function testGet()
    {
        $this->json('GET', '/auth', [], $this->getHeaderAuthorization());
        $this->assertResponseOk();
    }

    public function testDelete()
    {
        $this->json('DELETE', '/auth', [], $this->getHeaderAuthorization());
        $this->seeStatusCode(HttpStatus::OK);

        $authorization =  ['Authorization' => $this->response->headers->get('authorization')];
        $this->json('GET', '/auth', $authorization);
        $this->seeStatusCode(HttpStatus::UNAUTHORIZED);
    }
}
