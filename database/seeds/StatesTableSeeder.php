<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('AC', 'br', 'Norte', 'Acre')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('AL', 'br', 'Nordeste', 'Alagoas')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('AP', 'br', 'Norte', 'Amapá')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('AM', 'br', 'Norte', 'Amazonas')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('BA', 'br', 'Nordeste', 'Bahia')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('CE', 'br', 'Nordeste', 'Ceará')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('DF', 'br', 'Centro-Oeste', 'Distrito Federal')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('ES', 'br', 'Sudeste', 'Espírito Santo')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('GO', 'br', 'Centro-Oeste', 'Goiás')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('MA', 'br', 'Nordeste', 'Maranhão')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('MT', 'br', 'Centro-Oeste', 'Mato Grosso')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('MS', 'br', 'Centro-Oeste', 'Mato Grosso do Sul')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('MG', 'br', 'Sudeste', 'Minas Gerais')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('PA', 'br', 'Norte', 'Pará')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('PB', 'br', 'Nordeste', 'Paraíba')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('PR', 'br', 'Sul', 'Paraná')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('PE', 'br', 'Nordeste', 'Pernambuco')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('PI', 'br', 'Nordeste', 'Piauí')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('RJ', 'br', 'Sudeste', 'Rio de Janeiro')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('RN', 'br', 'Nordeste', 'Rio Grande do Norte')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('RS', 'br', 'Sul', 'Rio Grande do Sul')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('RO', 'br', 'Norte', 'Rondônia')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('RR', 'br', 'Norte', 'Roraima')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('SC', 'br', 'Sul', 'Santa Catarina')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('SP', 'br', 'Sudeste', 'São Paulo')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('SE', 'br', 'Nordeste', 'Sergipe')");
        DB::statement("INSERT INTO states(id, country_id, region, name) VALUES('TO', 'br', 'Norte', 'Tocantins')");
    }
}
