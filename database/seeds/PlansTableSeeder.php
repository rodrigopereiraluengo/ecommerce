<?php

use App\Models\Plan;
use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan = new Plan(['name' => 'Plano 1', 'status' => true, 'amount' => 9.99, 'days' => 30]);
        $plan->save();

        $plan = new Plan(['name' => 'Plano 2', 'status' => true, 'amount' => 19.99, 'days' => 30]);
        $plan->save();

        $plan = new Plan(['name' => 'Plano 3', 'status' => true, 'amount' => 29.99, 'days' => 30]);
        $plan->save();
    }
}
