<?php

use App\Models\Address;
use App\Models\User;
use App\Models\Person;
use App\Models\Plan;
use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subscription = new Subscription();
        $subscription->save();

        $user = new User([
            'email' => env('USER_ADM_EMAIL_0'),
            'password' => env('USER_ADM_PASSWORD_0')
        ]);
        $user->status = true;
        $user->role = User::ROLE_ADMIN;
        $user->subscription()->associate($subscription);
        $user->save();

        $person = new Person(['name' => 'Rodrigo Pereira Luengo', 'type' => 'F']);
        $person->user()->associate($user)->save();

        $address = new Address([
            'country_id' => 'br',
            'address_zipcode' => '08940-000',
            'address_street' => 'Rua Virgilina da Conceição Camargo',
            'address_street_number' => '58',
            'address_neighborhood' => 'Jd. Alvorada',
            'state_id' => 'SP',
            'city_id' => 5029
        ]);
        $address->save();
        $person->addresses()->sync([$address->id => ['name' => 'Principal']]);

        $user->person()->associate($person)->save();




        $subscription = new Subscription();
        $subscription->save();

        $user = new User([
            'email' => env('USER_ADM_EMAIL_1'),
            'password' => env('USER_ADM_PASSWORD_1')
        ]);
        $user->status = true;
        $user->role = User::ROLE_ADMIN;
        $user->subscription()->associate($subscription);
        $user->save();

        $person = new Person(['name' => 'Rodrigo Pereira Luengo', 'type' => 'F']);
        $person->user()->associate($user)->save();

        $address = new Address([
            'country_id' => 'br',
            'address_zipcode' => '08940-000',
            'address_street' => 'Rua Virgilina da Conceição Camargo',
            'address_street_number' => '58',
            'address_neighborhood' => 'Jd. Alvorada',
            'state_id' => 'SP',
            'city_id' => 5029
        ]);
        $address->save();
        $person->addresses()->sync([$address->id => ['name' => 'Principal']]);

        $user->person()->associate($person)->save();
    }
}
