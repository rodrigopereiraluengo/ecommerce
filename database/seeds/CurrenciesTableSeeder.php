<?php

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $currencies = [
            ["name" =>"Brazilian real","symbol" =>"R$","id" =>"BRL"],
            ["name" =>"Euro","symbol" =>"€","id" =>"EUR"],
            ["name" =>"United States dollar","symbol" =>"$","id" =>"USD"]
        ];

        foreach ($currencies as $currency) {
            Currency::create($currency);
        }
    }
}
