<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->char('country_id', 2)->default('br');
            $table->string('address_zipcode')->nullable();
            $table->string('address_street')->nullable();
            $table->string('address_street_number')->nullable();
            $table->string('address_complementary')->nullable();
            $table->string('address_neighborhood')->nullable();
            $table->char('state_id', 2)->nullable();
            $table->string('address_state')->nullable();
            $table->bigInteger('city_id')->nullable();
            $table->string('address_city')->nullable();
            $table->timestamps();

            $table
                ->foreign('country_id')
                ->references('id')
                ->on('public.countries')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table
                ->foreign('state_id')
                ->references('id')
                ->on('public.states')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table
                ->foreign('city_id')
                ->references('id')
                ->on('public.cities')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
