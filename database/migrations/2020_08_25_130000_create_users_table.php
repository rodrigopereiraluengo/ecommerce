<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subscription_id')->nullable();
            $table->char('role', 3)->nullable(); /* MST, ADM, USR */
            $table->boolean('status')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->json('metadata')->nullable();
            $table->timestamps();

            $table
                ->foreign('subscription_id')
                ->references('id')
                ->on('public.subscriptions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $subscriptions = DB::select("select id from subscriptions");
        foreach ($subscriptions as $subscription) {
            DB::unprepared("DROP schema subscription_{$subscription->id} cascade");
        }
        Schema::dropIfExists('users');
    }
}
