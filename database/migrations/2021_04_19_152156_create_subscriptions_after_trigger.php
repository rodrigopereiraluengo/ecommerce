<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsAfterTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
create function fn_subscriptions_after() returns trigger as $$
begin

    if TG_OP = 'INSERT' then
        execute format('create schema subscription_%s', new.id);
        return new;
    end if;

    if TG_OP = 'DELETE' then
        execute format('drop schema if exists subscription_%s cascade', old.id);
        return old;
    end if;


end;
$$
language plpgsql;

create trigger tg_subscriptions_after after insert or delete
on subscriptions
for each row when(pg_trigger_depth() = 0)
execute procedure fn_subscriptions_after();
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = <<<SQL
drop trigger if exists tg_subscriptions_after on subscriptions;
drop function fn_subscriptions_after();
SQL;
        DB::unprepared($sql);
    }
}
