<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->id();
            
            $table->foreignId('subscription_id')->nullable();
            
            $table->string('name');
            $table->boolean('status')->nullable();
            $table->json('metadata')->nullable();
            $table->json('published')->nullable();
            
            $table->timestamps();

            $table->unique(['subscription_id', 'name']);

            $table
                ->foreign('subscription_id')
                ->references('id')
                ->on('subscriptions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themes');
    }
}
