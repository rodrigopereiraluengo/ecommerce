<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->boolean('status')->nullable();
            $table->string('name');
            $table->decimal('amount');
            $table->smallInteger('days')->nullable();
            $table->string('color')->nullable();
            $table->smallInteger('trial_days')->nullable();
            $table->json('payment_methods')->nullable();
            $table->json('metadata')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
