<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AlterCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $master_subscription_id = User::query()->where('email', env('USER_MST_EMAIL'))->first()?->subscription_id;
        $from_schema = "subscription_$master_subscription_id";

        $current_categories_id_seq = DB::selectOne("select last_value from $from_schema.categories_id_seq")->last_value;

        $schema = Config::get('database.default');
        $sql = <<<SQL
        create sequence {$schema}.categories_id_seq start $current_categories_id_seq;
        alter table {$schema}.categories
            alter column id set default nextval('{$schema}.categories_id_seq'),
            add constraint {$schema}_pk_categories primary key(id),
            add constraint {$schema}_fk_parent_category_id
                foreign key (parent_category_id)
                references $schema.categories(id)
                    on delete cascade
                    on update cascade,
            add constraint {$schema}_fk_previous_category_id
                foreign key (previous_category_id)
                references $schema.categories(id)
                    on delete cascade
                    on update cascade,
            add constraint {$schema}_categories_unique_name unique (parent_category_id, name)
SQL;

        DB::unprepared($sql);
    }
}
