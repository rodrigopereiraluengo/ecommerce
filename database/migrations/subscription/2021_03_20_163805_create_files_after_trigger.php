<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Config;

class CreateFilesAfterTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
create function fn_files_after() returns trigger as $$
declare
	_category record;
	_previous_file_id bigint;
begin
	if TG_OP in ('INSERT', 'UPDATE') then
		update {$scheme}.files set
			previous_file_id = new.id
		where coalesce(directory_id, 0) = coalesce(new.directory_id, 0)
		and coalesce(previous_file_id, 0) = coalesce(new.previous_file_id, 0)
		and id <> new.id;

		for _category in
			select
				id,
				case when id = new.id then 0 else 1 end as order_id,
				(row_number() over (order by position)) - 1 as position
			from {$scheme}.files
			where coalesce(directory_id, 0) = coalesce(new.directory_id, 0)
			order by position, order_id
		loop
			update {$scheme}.files set
				position = _category.position,
				previous_file_id = case when _category.position = 0 then null else previous_file_id end
			where id = _category.id;
		end loop;
	end if;

	if (TG_OP = 'UPDATE' and coalesce(new.directory_id, 0) <> coalesce(old.directory_id, 0)) or TG_OP = 'DELETE' then
		update {$scheme}.files set previous_file_id = old.previous_file_id where previous_file_id = old.id;
		for _category in
			select id, (row_number() over (order by position)) - 1 as position
			from {$scheme}.files
			where coalesce(directory_id, 0) = coalesce(old.directory_id, 0)
			order by position
		loop
			update {$scheme}.files set position = _category.position where id = _category.id;
		end loop;

	end if;

return new;
end;
$$
language plpgsql;

create trigger tg_files_after after insert or update of directory_id, previous_file_id or delete
on {$scheme}.files
for each row
when(pg_trigger_depth() = 0)
execute procedure fn_files_after();
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
drop trigger if exists tg_files_after on {$scheme}.files;
drop function if exists fn_files_after();
SQL;
        DB::unprepared($sql);
    }
}
