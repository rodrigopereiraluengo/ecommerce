<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;

class AlterFileProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $schema = Config::get('database.default');
        $sql = <<<SQL
        alter table $schema.file_product
            add constraint fk_file_id
                foreign key (file_id)
                references {$schema}.files(id)
                    on delete cascade
                    on update cascade,
            add constraint {$schema}_fk_product_id
                foreign key (product_id)
                references $schema.products(id)
                    on delete cascade
                    on update cascade,
            add constraint {$schema}_file_product_unique unique (file_id, product_id)
SQL;

        DB::unprepared($sql);
    }

}
