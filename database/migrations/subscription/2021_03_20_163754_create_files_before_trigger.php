<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Config;

class CreateFilesBeforeTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
create function fn_files_before() returns trigger as $$
declare
	_previous_file record;
begin

	if (
		TG_OP = 'INSERT'
		and new.previous_file_id is null
		and exists(select 1 from {$scheme}.files where coalesce(directory_id, 0) = coalesce(new.directory_id, 0))
	) OR (
		TG_OP = 'UPDATE'
		and new.previous_file_id is null
		and coalesce(new.directory_id, 0) <> coalesce(old.directory_id, 0)
		and exists(select 1 from {$scheme}.files where coalesce(directory_id, 0) = coalesce(new.directory_id, 0))
	)
	then
		select id, position + 1 as position into _previous_file from {$scheme}.files where coalesce(directory_id, 0) = coalesce(new.directory_id, 0) order by position desc limit 1;
		new.previous_file_id = _previous_file.id;
		new.position = _previous_file.position;
	end if;

	if new.previous_file_id is null then
		new.position = 0;
	elsif TG_OP = 'INSERT' then
		select
			id,
			position + 1 as position into _previous_file
		from {$scheme}.files
		where coalesce(directory_id, 0) = coalesce(new.directory_id, 0)
		and id = new.previous_file_id;
		new.position = _previous_file.position;
	end if;

return new;
end;
$$
language plpgsql;

create trigger tg_files_before before insert or update of directory_id, previous_file_id
on {$scheme}.files
for each row
execute procedure fn_files_before();
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = <<<SQL
drop trigger if exists tg_files_before on {$scheme}.files;
drop function fn_files_before();
SQL;
        DB::unprepared($sql);
    }
}
