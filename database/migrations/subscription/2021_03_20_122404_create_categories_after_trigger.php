<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Config;

class CreateCategoriesAfterTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
create function fn_categories_after() returns trigger as $$
declare
	_category record;
	_previous_category_id bigint;
begin
	if TG_OP in ('INSERT', 'UPDATE') then
		update {$scheme}.categories set
			previous_category_id = new.id
		where coalesce(parent_category_id, 0) = coalesce(new.parent_category_id, 0)
		and coalesce(previous_category_id, 0) = coalesce(new.previous_category_id, 0)
		and id <> new.id;

		for _category in
			select
				id,
				case when id = new.id then 0 else 1 end as order_id,
				(row_number() over (order by position)) - 1 as position
			from {$scheme}.categories
			where coalesce(parent_category_id, 0) = coalesce(new.parent_category_id, 0)
			order by position, order_id
		loop
			update {$scheme}.categories set
				position = _category.position,
				previous_category_id = case when _category.position = 0 then null else previous_category_id end
			where id = _category.id;
		end loop;
	end if;

	if (TG_OP = 'UPDATE' and coalesce(new.parent_category_id, 0) <> coalesce(old.parent_category_id, 0)) or TG_OP = 'DELETE' then
		update {$scheme}.categories set previous_category_id = old.previous_category_id where previous_category_id = old.id;
		for _category in
			select id, (row_number() over (order by position)) - 1 as position
			from categories
			where coalesce(parent_category_id, 0) = coalesce(old.parent_category_id, 0)
			order by position
		loop
			update {$scheme}.categories set position = _category.position where id = _category.id;
		end loop;

	end if;

return new;
end;
$$
language plpgsql;

create trigger tg_categories_after after insert or update of parent_category_id, previous_category_id or delete
on {$scheme}.categories
for each row
when(pg_trigger_depth() = 0)
execute procedure fn_categories_after();
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
drop trigger if exists tg_categories_after on {$scheme}.categories;
drop function if exists fn_categories_after();
SQL;
        DB::unprepared($sql);
    }
}
