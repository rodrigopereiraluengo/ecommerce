<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Config;

class CreateSessionsAfterTigger extends Migration
{

    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
create function fn_sessions_after() returns trigger as $$
begin

    if new.status = true then
        if new.type in('SEARCH') then
            update {$scheme}.sessions set status = false where user_id = new.user_id and id <> new.id and type = new.type;
        else
            update {$scheme}.sessions set status = false where user_id = new.user_id and id <> new.id and type not in('SEARCH');
        end if;
    end if;

return new;
end;
$$
language plpgsql;

create trigger tg_sessions_after after insert or update of status
on {$scheme}.sessions
for each row when(pg_trigger_depth() = 0)
execute procedure fn_sessions_after();
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
drop trigger if exists tg_sessions_after on {$scheme}.sessions;
drop function fn_sessions_after();
SQL;
        DB::unprepared($sql);
    }
}
