<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('directories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('directory_id')->nullable();
            $table->boolean('status')->nullable();
            $table->string('name', 45);
            $table->string('description', 4000)->nullable();
            $table->json('metadata')->nullable();
            $table->timestamps();

            $table
              ->foreign('directory_id')
              ->references('id')
              ->on('directories')
              ->cascadeOnDelete()
              ->cascadeOnUpdate();

            $table->unique(['directory_id', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('directories');
    }
}
