<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->foreignId('directory_id')->nullable();
            $table->foreignId('previous_file_id')->nullable();
            $table->boolean('status')->nullable();
            $table->string('path', 4000);
            $table->string('url', 4000)->nullable();
            $table->string('name');
            $table->string('description', 4000)->nullable();
            $table->string('extension', 10)->nullable();
            $table->string('mime')->nullable();
            $table->integer('size');
            $table->integer('position')->nullable();
            $table->json('metadata')->nullable();
            $table->json('thumbnails')->nullable();
            $table->timestamps();

            $table
                ->foreign('directory_id')
                ->references('id')
                ->on('directories')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table
                ->foreign('previous_file_id')
                ->references('id')
                ->on('files')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->unique(['directory_id', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
