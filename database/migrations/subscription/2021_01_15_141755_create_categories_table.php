<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parent_category_id')->nullable();
            $table->foreignId('previous_category_id')->nullable();
            $table->boolean('status')->nullable();
            $table->string('name');
            $table->string('description', 4000)->nullable();
            $table->integer('position')->nullable();
            $table->json('metadata')->nullable();
            $table->timestamps();

            $table
                ->foreign('parent_category_id')
                ->references('id')
                ->on('categories')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table
                ->foreign('previous_category_id')
                ->references('id')
                ->on('categories')
                ->onDelete('set null');

            $table->unique(['parent_category_id', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
