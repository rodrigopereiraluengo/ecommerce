<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;

class AlterProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $master_subscription_id = User::query()->where('email', env('USER_MST_EMAIL'))->first()?->subscription_id;
        $from_schema = "subscription_$master_subscription_id";

        $current_products_id_seq = DB::selectOne("select last_value from $from_schema.products_id_seq")->last_value;

        $schema = Config::get('database.default');
        $sql = <<<SQL
        create sequence $schema.products_id_seq start $current_products_id_seq;
        alter table $schema.products
            alter column id set default nextval('$schema.products_id_seq'),
            add constraint {$schema}_pk_products primary key(id),
            add constraint {$schema}_fk_category_id
                foreign key (category_id)
                references $schema.categories(id)
                    on delete cascade
                    on update cascade,
            add constraint {$schema}_products_unique_name unique (category_id, name)
SQL;

        DB::unprepared($sql);
    }
}
