<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AlterFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $master_subscription_id = User::query()->where('email', env('USER_MST_EMAIL'))->first()?->subscription_id;
        $from_schema = "subscription_$master_subscription_id";

        $current_files_id_seq = DB::selectOne("select last_value from $from_schema.files_id_seq")->last_value;

        $schema = Config::get('database.default');
        $sql = <<<SQL
        create sequence $schema.files_id_seq start $current_files_id_seq;
        alter table $schema.files
            alter column id set default nextval('$schema.files_id_seq'),
            add constraint {$schema}_pk_files primary key(id),
            add constraint {$schema}_fk_directory_id
                foreign key (directory_id)
                references $schema.directories(id)
                    on delete cascade
                    on update cascade,
            add constraint {$schema}_fk_previous_file_id
                foreign key (previous_file_id)
                references $schema.files(id)
                    on delete cascade
                    on update cascade,
            add constraint {$schema}_files_unique_name unique (directory_id, name)
SQL;

        DB::unprepared($sql);
    }
}
