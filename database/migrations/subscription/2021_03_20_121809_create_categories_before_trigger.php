<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class CreateCategoriesBeforeTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
create function fn_categories_before() returns trigger as $$
declare
	_previous_category record;
begin

	if (
		TG_OP = 'INSERT'
		and new.previous_category_id is null
		and exists(select 1 from {$scheme}.categories where coalesce(parent_category_id, 0) = coalesce(new.parent_category_id, 0))
	) OR (
		TG_OP = 'UPDATE'
		and new.previous_category_id is null
		and coalesce(new.parent_category_id, 0) <> coalesce(old.parent_category_id, 0)
		and exists(select 1 from {$scheme}.categories where coalesce(parent_category_id, 0) = coalesce(new.parent_category_id, 0))
	)
	then
		select id, position + 1 as position into _previous_category from {$scheme}.categories where coalesce(parent_category_id, 0) = coalesce(new.parent_category_id, 0) order by position desc limit 1;
		new.previous_category_id = _previous_category.id;
		new.position = _previous_category.position;
	end if;

	if new.previous_category_id is null then
		new.position = 0;
	elsif TG_OP = 'INSERT' then
		select
			id,
			position + 1 as position into _previous_category
		from {$scheme}.categories
		where coalesce(parent_category_id, 0) = coalesce(new.parent_category_id, 0)
		and id = new.previous_category_id;
		new.position = _previous_category.position;
	end if;

return new;
end;
$$
language plpgsql;

create trigger tg_categories_before before insert or update of parent_category_id, previous_category_id
on {$scheme}.categories
for each row
execute procedure fn_categories_before();
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $scheme = Config::get('database.default');
        $sql = <<<SQL
drop trigger if exists tg_categories_before on {$scheme}.categories;
drop function fn_categories_before();
SQL;
        DB::unprepared($sql);
    }
}
