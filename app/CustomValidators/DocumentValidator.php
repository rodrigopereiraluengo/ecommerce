<?php
namespace App\CustomValidators;

use Bissolli\ValidadorCpfCnpj\CPF;
use Bissolli\ValidadorCpfCnpj\CNPJ;

class DocumentValidator
{
  public function cpf_cnpj($attribute, $value, $parameters, $validator)
  {
    $value = preg_replace('/[^0-9]/', '', $value);
    if (strlen($value) < 14) {
      return $this->cpf($attribute, $value, $parameters, $validator);
    }
    return $this->cnpj($attribute, $value, $parameters, $validator);
  }

  public function cpf($attribute, $value, $parameters, $validator)
  {
    $value = preg_replace('/[^0-9]/', '', $value);
    if (empty(trim($value)) === true) {
      return true;
    }
    $document = new CPF($value);
    return $document->isValid();
  }

  public function cnpj($attribute, $value, $parameters, $validator)
  {
    $value = preg_replace('/[^0-9]/', '', $value);
    if (empty(trim($value)) === true) {
      return true;
    }
    $document = new CNPJ($value);
    return $document->isValid();
  }
}
