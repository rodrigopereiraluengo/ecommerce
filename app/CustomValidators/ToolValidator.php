<?php
namespace App\CustomValidators;

use App\Repositories\ToolRepository;

class ToolValidator
{
  public function tool_unique_name($attribute, $value, $parameters, $validator)
  {
    if (empty($value) === false && empty($parameters[0]) === false && empty($parameters[1]) === false) {
      $repository = app(ToolRepository::class);
      return $repository->existsToolsByName($parameters[1], $parameters[0], $value, $parameters[2], $parameters[3]) === false;
    }

    return true;
  }

  public function tool_unique_path($attribute, $value, $parameters, $validator)
  {
    if (empty($value) === false && empty($parameters[0]) === false && empty($parameters[1]) === false) {
      $repository = app(ToolRepository::class);
      return $repository->existsToolsByPath($parameters[1], $parameters[0], $value, $parameters[2], $parameters[3]) === false;
    }
  }
}
