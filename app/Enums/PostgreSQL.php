<?php
namespace App\Enums;

class PostgreSQL
{
  const INTEGER_MIN = -2147483647;
  const INTEGER_MAX = 2147483647;
}
