<?php

namespace App\Enums;

use Illuminate\Support\Facades\Storage;

class Response
{
    static function default($status = HttpStatus::OK)
    {
        return response()->json(null, $status);
    }

    static function created($model)
    {
        return response()->json(['id' => $model->id], HttpStatus::CREATED);
    }

    static function noContent()
    {
        return self::default(HttpStatus::NO_CONTENT);
    }

    static function file($path)
    {
        return Storage::response($path);
    }
}
