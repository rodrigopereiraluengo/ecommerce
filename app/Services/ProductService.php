<?php

namespace App\Services;

use App\Exceptions\ServiceException;
use App\Exceptions\ValidatorException;
use App\Models\Subscription\File;
use App\Validators\ProductValidator;
use App\Repositories\ProductRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductService
{
    public function __construct(private ProductValidator $validator, private ProductRepository $repository) { }

    public function save(array $data, $id = null)
    {
        $product = null;
        if ($id !== null && ($product = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Product [$id] not found");
        }

        if ($this->validator->save($data, $product) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        if ($product === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($product, $data);
    }

    public function patch(array $data, $id)
    {
        $category = $this->find($id);
        if ($this->validator->patch($data, $category) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }
        return $this->repository->update($category, $data);
    }

    public function find($id)
    {
        if (($product = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Product [$id] not found");
        }

        return $product;
    }

    public function index(array $data)
    {
        if ($this->validator->index($data) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->index($data);
    }

    public function delete($id)
    {
        $product = $this->find($id);

        if ($product->products()->exists()) {
            throw new ServiceException("There are products relationship with that category");
        }

        return $this->repository->delete($product);
    }

    public function file(File $file, array $data, $id)
    {
        $product = $this->find($id);
        $product->files()->attach($file->id);
    }
}
