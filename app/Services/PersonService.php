<?php
namespace App\Services;

use App\Domains\AddressDomain;
use App\Domains\PersonDomain;
use App\Exceptions\ValidatorException;
use App\Models\Address;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class PersonService
{
    private $domain;
    private $addressDomain;

    public function __construct(PersonDomain $domain, AddressDomain $addressDomain)
    {
        $this->domain = $domain;
        $this->addressDomain = $addressDomain;
    }

    public function save(array $data, $id = null)
    {
        DB::beginTransaction();
        try {

            try {
                $person = $this->domain->save($data, $id);
            } catch (ValidatorException $exception) {
                throw new ValidatorException($exception->errors, 'person');
            }

            $addresses = &$data['addresses'];
            if (is_array($addresses)) {
                foreach ($addresses as &$data_address) {
                    $address_id     = Arr::get($data_address, 'id');
                    $address_name   = $data_address['name'];

                    if ($address_id === null && $address_name) {
                        $address = $person->addresses()->where('name', $address_name)->first();
                        if ($address) {
                            Arr::set($data_address, 'id', $address->id);
                        }
                    }
                }
            }

            if ($this->domain->validator->addresses($data, $id) === false) {
                throw new ValidatorException($this->domain->validator->getErrors(), 'person');
            }

            if (is_array($addresses)) {
                $addresses_sync = [];
                foreach ($addresses as $data_address) {
                    $address_id     = Arr::get($data_address, 'id');
                    $address_name   = $data_address['name'];
                    $address        = $this->addressDomain->save($data_address, $address_id);
                    $addresses_sync[$address->id] = ['name' => $address_name];
                }

                $addresses_id = $person->addresses()->select('id')->get()->map(function ($address) {
                    return $address->id;
                })->toArray();

                $person->addresses()->sync($addresses_sync);

                $addresses_id = array_diff($addresses_id, array_keys($addresses_sync));

                Address::query()->whereIn('id', $addresses_id)->delete();
            }

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }

        return $person;
    }
}
