<?php

namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Repositories\SessionRepository;
use App\Validators\SessionValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

class SessionService
{
    public function __construct(private SessionValidator $validator, private SessionRepository $repository)
    {
    }

    public function insert(array $data)
    {
        if ($this->validator->insert($data) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->insert($data);
    }

    public function patch(array $data, $id)
    {
        if (($session = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Session [$id] not found");
        }

        if ($this->validator->patch($data, $session) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->update($session, $data);
    }

    public function index(array $data)
    {
        if ($this->validator->index($data) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->index($data);
    }
}
