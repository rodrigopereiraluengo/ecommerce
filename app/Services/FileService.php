<?php

namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Exceptions\ServiceException;
use App\Models\File;
use App\Validators\FileValidator;
use App\Repositories\FileRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

class FileService
{
    public function __construct(private FileValidator $validator, private FileRepository $repository)
    {
    }

    public function save(array $data, $id = null)
    {
        $file = null;
        if ($id && ($file = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("File [$id] not found");
        }

        if ($this->validator->save($data, $file) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        $model      = &$data['model'];
        $model_id   = &$data['model_id'];

        DB::beginTransaction();
        try {
            $file = ($file ? $this->repository->update($file, $data) : $this->repository->insert($data));

            if ($model) {
                $service = app('App\Services\\' . Str::ucfirst($model) . 'Service');
                $service->file($file, $data, $model_id);
            }

            DB::commit();
            return $file;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function patch(array $data, $id)
    {
        $file = $this->find($id);
        if ($this->validator->patch($data, $file) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        $model      = &$data['model'];
        $model_id   = &$data['model_id'];

        DB::beginTransaction();
        try {
            $file = $this->repository->update($file, $data);
            if ($model) {
                $service = app('App\Services\\' . Str::ucfirst($model) . 'Service');
                $service->file($file, $data, $model_id);
            }

            DB::commit();
            return $file;
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function find($id)
    {
        if (($file = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("File [$id] not found");
        }

        return $file;
    }

    public function index(array $data)
    {
        if ($this->validator->index($data) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->index($data);
    }

    public function path($id, $path)
    {
        $file = $this->find($id);
        if ($file->path !== $path) {
            throw new ModelNotFoundException("File [$id] not found");
        }

        return $file;
    }

    public function thumbnail($id, $width, $height, $path)
    {
        $file = $this->find($id);
        if ($file->path !== $path) {
            throw new ModelNotFoundException("File [$id] not found");
        }

        $thumbnail = $file->{"thumbnail_{$width}_{$height}"};

        if ($thumbnail === null) {
            throw new ServiceException("Invalid size");
        }

        return $thumbnail;
    }

    public function rotate($id, $angular, $path)
    {
        $file = $this->find($id);
        if ($file->path !== $path) {
            throw new ModelNotFoundException("File [$id] not found");
        }

        if ($angular < 10 || $angular > 360) {
            throw new ServiceException("Invalid angular");
        }

        return $file->{"rotate_{$angular}"};
    }

    public function rotateSave($id, $angular, $path)
    {
        $file = $this->find($id);
        if ($file->path !== $path) {
            throw new ModelNotFoundException("File [$id] not found");
        }

        if ($angular < 10 || $angular > 360) {
            throw new ServiceException("Invalid angular");
        }

        return $file->{"rotate_{$angular}_save"};
    }
}
