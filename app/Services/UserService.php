<?php

namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserService
{
    public function __construct(private UserValidator $validator, private UserRepository $repository)
    {
    }

    public function save(array $data, $id = null)
    {
        $user = null;
        if ($id !== null && ($user = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("User [$id] not found");
        }

        if ($this->validator->save($data, $user) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        if ($user === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($user, $data);
    }

    public function patch(array $data, $id)
    {
        $user = $this->find($id);
        if ($this->validator->patch($data, $user) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->update($user, $this->validator->getValidated());
    }

    public function find($id)
    {
        if (($user = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("User [$id] not found");
        }

        return $user;
    }

}
