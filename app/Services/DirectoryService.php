<?php

namespace App\Services;

use App\Exceptions\ServiceException;
use App\Exceptions\ValidatorException;
use App\Repositories\DirectoryRepository;
use App\Validators\DirectoryValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DirectoryService
{

    private $validator;
    private $repository;

    public function __construct(DirectoryValidator $validator, DirectoryRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $directory = null;
        if ($id !== null && ($directory = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Directory [$id] not found");
        }

        if ($this->validator->save($data, $directory) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        if ($directory === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($directory, $data);
    }

    public function patch(array $data, $id)
    {
        $directory = $this->find($id);
        if ($this->validator->patch($data, $directory) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->update($directory, $data);
    }

    public function delete($id)
    {
        $directories = $this->find($id);

        if ($directories->directories()->exists()) {
            throw new ServiceException("There are products relationship with that directories");
        }

        if ($directories->files()->exists()) {
            throw new ServiceException("There are categories relationship with that directories");
        }

        return $this->repository->delete($directories);
    }

    public function find($id)
    {
        if (($directory = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Directory [$id] not found");
        }

        return $directory;
    }

    public function index(array $data)
    {
        if ($this->validator->index($data) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->index($data);
    }
}
