<?php

namespace App\Services;

use App\Exceptions\ValidatorException;
use App\Exceptions\ServiceException;
use App\Validators\CategoryValidator;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryService
{

    public function __construct(private CategoryValidator $validator, private CategoryRepository $repository)
    {
    }

    public function save(array $data, $id = null)
    {
        $category = null;
        if ($id !== null && ($category = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Category [$id] not found");
        }

        if ($this->validator->save($data, $category) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        if ($category === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($category, $data);
    }

    public function patch(array $data, $id)
    {
        $category = $this->find($id);

        if ($this->validator->patch($data, $category) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->update($category, $data);
    }

    public function delete($id)
    {
        $category = $this->find($id);

        if ($category->products()->exists()) {
            throw new ServiceException("There are products relationship with that category");
        }

        if ($category->categories()->exists()) {
            throw new ServiceException("There are categories relationship with that category");
        }

        return $this->repository->delete($category);
    }

    public function find($id)
    {
        if (($category = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Category [$id] not found");
        }

        return $category;
    }

    public function index(array $data)
    {
        if ($this->validator->index($data) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        return $this->repository->index($data);
    }

}
