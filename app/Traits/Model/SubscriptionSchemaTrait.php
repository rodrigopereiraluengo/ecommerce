<?php

namespace App\Traits\Model;

use App\Models\Subscription;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

trait SubscriptionSchemaTrait
{
    public function getSchema()
    {
        $database = Config::get('database.default');
        if (str_starts_with($database, 'subscription_')) {
            return $database;
        }

        return "public";
    }

    public function getTable()
    {
        $table = parent::getTable();
        $table = Str::replaceFirst('public.', '', $table);
        $database = Config::get('database.default');
        if (!str_starts_with($table, 'subscription_') && str_starts_with($database, 'subscription_')) {
            return $database . '.' . $table;
        }

        if (!str_starts_with($table, 'subscription_')) {
            throw new \Exception("Database unsetted");
        }

        return parent::getTable();
    }
}
