<?php

namespace App\Traits\Test;

use App\Enums\HttpStatus;
use App\Models\User;

trait AuthTestTrait
{
    private $user_number = 0;

    public function getToken($email, $password)
    {
        $this->json('POST', '/auth', ['email' => $email, 'password' => $password]);
        $this->assertResponseOk();
        if ($this->response->status() === HttpStatus::OK) {
            $response = json_decode($this->response->content());
            return $response->token->access_token;
        }
    }

    public function getHeaderAuthorization($email = null, $password = null)
    {
        if ($email === null) {
            $email = $this->getEmail();
        }

        if ($password === null) {
            $password = $this->getPassword();
        }

        return ['Authorization' => 'Bearer ' . $this->getToken($email, $password)];
    }

    public function getUserAdmin()
    {
        return User::query()->where('email', $this->getEmail())->first();
    }

    public function getSubscriptionId()
    {
        return User::query()->where('email', $this->getEmail())->first()?->subscription_id;
    }

    public function primaryUser()
    {
        $this->user_number = 0;
        return $this;
    }

    public function secondaryUser()
    {
        $this->user_number = 1;
        return $this;
    }

    public function getEmail()
    {
        return env('USER_ADM_EMAIL_' . $this->user_number);
    }

    public function getPassword()
    {
        return env('USER_ADM_PASSWORD_' . $this->user_number);
    }
}
