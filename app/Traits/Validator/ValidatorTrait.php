<?php

namespace App\Traits\Validator;

use App\Validators\DefaultValidator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

trait ValidatorTrait
{
    private $validator = null;
    private $errors = [];

    private $rules = null;
    private $data = null;
    private $messages = null;
    private $validated = [];

    private $validators = [];

    public function setRules(array $rules)
    {
        $this->rules = $rules;
        return $this;
    }

    public function getRules()
    {
        return $this->rules;
    }

    public function addRules(array $rules)
    {
        if ($this->rules === null) {
            $this->setRules($rules);
        } else {

            foreach ($rules as $key => &$rule) {

                if (is_string($rule) === true) {
                    $rule = explode('|', $rule);
                }

                if (array_key_exists($key, $this->rules) && is_string($this->rules[$key]) === true) {
                    $this->rules[$key] = explode('|', $this->rules[$key]);
                }

                if (array_key_exists($key, $this->rules)) {
                    $this->rules[$key] = array_merge_recursive($this->rules[$key], $rule);
                }

                $this->rules[$key] = array_key_exists($key, $this->rules) ? array_merge_recursive($this->rules[$key], $rule) : $rule;
            }
        }

        return $this;
    }

    public function setData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    public function addData(array $data)
    {
        if ($this->data === null) {
            $this->data = $data;
        } else {
            foreach ($data as $key => $value) {
                $this->data[$key] = $value;
            }
        }

        return $this;
    }

    public function setMessages(array $messages)
    {
        $this->messages = $messages;
        return $this;
    }

    public function addMessages(array $messages)
    {
        $this->messages = $this->messages === null ? $messages : array_merge_recursive($this->messages, $messages);
        return $this;
    }

    public function addError($arg0, $arg1 = null)
    {
        if ($arg1 === null) {
            $arg1 = $arg0;
            $arg0 = 'message';
        }

        if (array_key_exists($arg0, $this->errors) === false) {
            $this->errors[$arg0] = [];
        }

        if (in_array($arg1, $this->errors[$arg0]) === false) {
            $this->errors[$arg0][] = trans($arg1);
        }

        return $this;
    }

    public function addErrors($arg0, $arg1 = null)
    {
        $category = null;
        $errors = null;

        if (gettype($arg0) == 'string')      $category = $arg0;
        if (gettype($arg0) == 'array')       $errors = $arg0;
        if ($arg0 instanceof MessageBag)     $errors = $arg0->toArray();
        if (gettype($arg1) == 'array')       $errors = $arg1;
        if ($arg1 instanceof MessageBag)     $errors = $arg1->toArray();

        if (is_array($errors) and !empty($errors)) {
            foreach ($errors as $_category => $_errors) {
                foreach ($_errors as $error) {
                    $this->addError(($category ? $category . '.' : '') . $_category, $error);
                }
            }
        }

        return $this;
    }

    public function getErrors()
    {
        if (isset($this->validator)) {
            $this->addErrors($this->validator->errors());
        }

        return $this->errors;
    }

    public function hasError()
    {
        return empty($this->getErrors()) === false;
    }

    public function isValid(): bool
    {
        if (($this->validator instanceof \Illuminate\Validation\Validator) === false) {
            $this->validator = Validator::make($this->data, $this->rules, $this->messages ?? []);
        }
        return !$this->hasError();
    }

    public function addValidator(DefaultValidator $validator)
    {
        $this->validators[] = $validator;
        return $validator;
    }

    public function getValidated()
    {
        if ($this->validator) {
            $this->validated = $this->validator->validated();
        }

        foreach ($this->validators as $validator) {
            $this->validated = array_merge($this->validated, $validator->getValidated());
        }

        return $this->validated;
    }

    public function getRulesData(array $rules, array $data): array
    {
        $rules_keys = array_keys($rules);
        $rules_data = [];

        foreach ($rules_keys as $rule_key) {
            if (Arr::has($data, $rule_key)) {
                $rules_data[$rule_key] = $rules[$rule_key];
            }
        }

        return $rules_data;
    }

    public function setRulesData(array $rules, array $data)
    {
        $this->setRules($this->getRulesData($rules, $data))->setData($data);
        return $this;
    }
}
