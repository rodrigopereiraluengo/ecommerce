<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use PagarMe\Client;

class PagarmeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Client::class, function () {
            return new Client(env('PAGARME_API'));
        });
    }
}
