<?php

namespace App\Providers;

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class SubscriptionServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Subscription::class, function () {

            if (Auth::check() && Auth::user()->role === User::ROLE_ADMIN && Auth::user()->subscription) {
                return Auth::user()->subscription;
            }

            if (PHP_SAPI === 'cli') {
                return Subscription::query()
                        ->whereRaw('id in (select subscription_id from public.users where email = ?)', [env('USER_MST_EMAIL')])
                        ->first();
            }

            return null;
        });
    }
}
