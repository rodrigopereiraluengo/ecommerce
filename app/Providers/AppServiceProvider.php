<?php

namespace App\Providers;

use App\Models\Plan;
use App\Models\Subscription;
use App\Models\Subscription\File;
use App\Observers\FileObserver;
use App\Observers\PlanObserver;
use App\Observers\SubscriptionObserver;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Validator::extend('cpf', 'App\CustomValidators\DocumentValidator@cpf');
        Validator::extend('cnpj', 'App\CustomValidators\DocumentValidator@cnpj');
        Validator::extend('cpf_cnpj', 'App\CustomValidators\DocumentValidator@cpf_cnpj');

        // Validator::extend('credit_card', 'App\CustomValidators\CreditCardValidator@credit_card');


        Validator::extend('tool_unique_name', 'App\CustomValidators\ToolValidator@tool_unique_name');
        Validator::extend('tool_unique_path', 'App\CustomValidators\ToolValidator@tool_unique_path');
    }

    public function boot()
    {
        Plan::observe(PlanObserver::class);
        File::observe(FileObserver::class);
        Subscription::observe(SubscriptionObserver::class);
    }
}
