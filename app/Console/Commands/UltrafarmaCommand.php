<?php

namespace App\Console\Commands;

use App\Models\Subscription\Category;
use App\Models\Subscription\Product;
use App\Models\Subscription;
use App\Models\User;
use App\Services\FileService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Mimey\MimeTypes;

class UltrafarmaCommand extends Command
{
    protected $signature = 'ultrafarma:import';

    protected $description = "Store to database Ultrafarma's categories and products";

    public function handle()
    {

        $subscription = app(Subscription::class);
        $schema = 'subscription_' . $subscription->id;
        Config::set("database.connections.$schema", [
            'driver'   => env('DB_CONNECTION'),
            'host'     => env('DB_HOST'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => $schema,
        ]);

        Config::set('database.default', $schema);

        $files = Storage::allFiles('ultrafarma');
        $fileService = app(FileService::class);

        if (Storage::exists('public/files/' . $subscription->id)) {
            Storage::deleteDirectory('public/files/' . $subscription->id);
        }

        foreach ($files as $file) {

            $data_product = json_decode(Storage::get($file), true);

            foreach ($data_product['category'] as $i => $category_name) {
                $query = Category::where('categories.name', $category_name);

                switch ($i) {
                    case 0:
                        $query->whereNull('categories.parent_category_id');
                        break;
                    case 1:
                        $query->whereRaw('categories.parent_category_id in (
                                                select c1.id
                                                from categories c1
                                                where c1.name = ?
                                                and c1.parent_category_id is null
                                            )', [$data_product['category'][$i - 1]]);
                        break;
                    default:
                        $query->whereRaw('categories.parent_category_id in (
                                                select c2.id
                                                from categories c2
                                                where c2.name = ?
                                                and c2.parent_category_id in (
                                                    select c1.id
                                                    from categories c1
                                                    where c1.name = ?
                                                    and c1.parent_category_id is null
                                                )
                                            )', [
                            $data_product['category'][$i - 1],
                            $data_product['category'][$i - 2]
                        ]);
                }

                $category = $query->firstOrNew([
                   'name' => $category_name
                ]);

                switch ($i) {
                    case 1:
                        $category->parent_category_id = DB::selectOne('
                            select id
                            from categories
                            where name = ?
                              and parent_category_id is null
                          ', [
                            $data_product['category'][$i - 1]
                        ])?->id;
                        break;
                    case 2:
                        $category->parent_category_id = DB::selectOne('
                            select c2.id
                            from categories c2
                            where c2.name = ?
                              and c2.parent_category_id in(
                                  select c1.id
                                  from categories c1
                                  where c1.name = ?
                                    and c1.parent_category_id is null
                                  )
                            ', [
                            $data_product['category'][$i - 1],
                            $data_product['category'][$i - 2]
                        ])->id;
                        break;
                }
                $category->save();
            }

            $product = Product::where(['category_id' => $category->id, 'name' => $data_product['name']])->firstOrNew();

            if ($product->id) {
                printf("REPEATED [%s] \n", $data_product['name']);
                unset($data_product['local_images']);
                foreach ($product->files()->orderBy('position')->get() as $product_file) {
                    if (Storage::exists($product_file->path) && !Storage::exists("default/products/{$product_file->name}.{$product_file->extension}")) {
                        Storage::copy($product_file->path, "default/products/{$product_file->name}.{$product_file->extension}");
                        $data_product['local_images'][] = "default/products/{$product_file->name}.{$product_file->extension}";
                    }
                }
                Storage::put($file, json_encode($data_product));
                continue;
            }

            $product->fill([
                'category_id' => $category->id,
                'name' => $data_product['name'],
                'price' => $data_product['price_old'] ?? $data_product['price_new'],
                'description' => $data_product['description'],
                'metadata' => $data_product,
                'status' => true
            ]);
            $product->save();

            foreach ($data_product['images'] as $i => $image) {
                if (str_starts_with($image, "https://cdn.ultrafarma.com.br/static/mockups")) {
                    continue;
                }

                if (isset($data_product['local_images'][$i]) && Storage::exists($data_product['local_images'][$i])) {
                    $content = Storage::get($data_product['local_images'][$i]);
                } else {
                    $content = @file_get_contents($image);
                }
                if (!$content) continue;

                $data = [
                    'model' => 'product',
                    'model_id' => $product->id,
                    'type' => 'image',
                    'status' => true
                ];
                $tmpfile            = tmpfile();
                fwrite($tmpfile, $content);
                $uri                = stream_get_meta_data($tmpfile)['uri'];
                $mime               = mime_content_type($uri);
                $mimes              = new MimeTypes;
                $extension          = $mimes->getExtension($mime);
                $name               = (string) Str::uuid();
                if (!isset($data['name'])) {
                    $data['name']   = $name;
                }
                $filename           = $name . '.' . $extension;
                $size               = filesize($uri);

                $data['file']       = [
                    'uri'       => $uri,
                    'filename'  => $filename,
                    'mime'      => $mime,
                    'extension' => $extension,
                    'size'      => $size,
                ];

                $fileService->save($data);
            }
            printf("Product [%s] Saved\n", $data_product['name']);
        }
    }
}
