<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class ValidatorException extends Exception
{
    public $errors;
    public $category;

    public function __construct(array|string $errors, $category = null, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        if (is_string($errors)) {
            $errors = ['errors' => [$errors]];
        }
        $this->errors = $errors;
        $this->category = $category;
    }
}
