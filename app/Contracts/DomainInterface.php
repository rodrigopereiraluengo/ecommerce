<?php
namespace App\Contracts;

interface DomainInterface
{
  public function save(array $data, $id = null);

  public function insert(array $data);

  public function update(array $data, $id);

  public function find($id);

  public function index(array $data);
}
