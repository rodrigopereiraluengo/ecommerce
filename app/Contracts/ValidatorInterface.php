<?php
namespace App\Contracts;

interface ValidatorInterface
{
  public function save(array &$data, $model = null): bool;

  public function insert(array &$data): bool;

  public function update(array &$data, $model): bool;

  public function index(array &$data): bool;
}
