<?php
namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Traits\Validator\ValidatorTrait;

class SessionValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function insert(array &$data): bool
    {
        $rules = [
            'type' => 'required|in:STORE,PRODUCT,COSTUMER,SALE,PROFILE,SETTING,USER,SEARCH'
        ];

        return $this->setRules($rules)->setData($data)->isValid();
    }

    public function patch(array &$data, $session): bool
    {
        $rules = [
            'status' => 'required|boolean',
            'data' => 'required|array'
        ];

        return $this->setRulesData($rules, $data)->isValid();
    }

    public function index(array &$data): bool
    {
        return true;
    }
}
