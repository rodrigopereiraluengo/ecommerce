<?php

namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Exceptions\ValidatorException;
use App\Models\File;
use App\Models\Subscription;
use App\Repositories\FileRepository;
use App\Traits\Validator\ValidatorTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class FileValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function rules(array $data, $file = null): array
    {
        $directory_id = &$data['directory_id'];
        $rules = [
            'directory_id' => ['nullable', Rule::exists('directories', 'id')],
            'previous_file_id' => [
                'nullable',
                Rule::exists('files', 'id')
                    ->where(function ($query) use ($file, $directory_id) {
                        $query
                            ->where('directory', $directory_id);

                        if ($file) {
                            $query->where('id', '<>', $file->id);
                        }
                    })
            ],
            'file' => ['required', 'array'],
            'file.size' => 'required_with:file|integer|max:' . env('FILE_MAX'),
            'file.extension' => ['required_with:file'],
            'file.mime' => 'required_with:file',
            'status' => 'nullable|boolean',
            'name' => ['required_with:file', 'max:255', Rule::unique('files')->where(function ($query) use ($file, $directory_id) {
                $query->where('directory_id', $directory_id);
                if ($file) {
                    $query->where('id', '<>', $file->id);
                }
            })],
            'metadata'  => 'nullable|array',
            'model'     => ['nullable', 'in:product'],
            'model_id'  => ['required_with:model'],
            'type'      => ['nullable', 'in:image']
        ];

        $type = &$data['type'];
        if ($type === 'image') {
            $rules['file.extension'][] = 'in:jpg,png';
        }

        return $rules;
    }

    public function save(array &$data, $file = null): bool
    {
        $rules = $this->rules($data, $file);
        return $this->setRules($rules)->setData($data)->isValid();
    }

    public function patch(array &$data, $file): bool
    {
        $rules = $this->rules($data, $file);
        $this->setRulesData($rules, $data);
        if (empty($this->getRules())) {
            throw new ValidatorException('No one properties found.');
        }
        return $this->isValid();
    }

    public function index(array &$data): bool
    {
        $rules = [
            'q' => 'nullable|max:45',
            'directory_id' => [
                'nullable',
                Rule::exists('directories', 'id')->where('subscription_id', $this->subscription->id)
            ],
            'status' => 'nullable|boolean',
            'extension' => [
                'nullable',
                'array'
            ],
            'extension.*' => [
                Rule::in(
                    File::query()
                        ->where('subscription_id', $this->subscription->id)
                        ->distinct('extension')
                        ->select('extension')
                        ->get()
                        ->map(fn ($row) => $row->extension)
                )
            ],
            'mime' => [
                'nullable',
                'array'
            ],
            'mime.*' => [
                Rule::in(
                    File::query()
                        ->where('subscription_id', $this->subscription->id)
                        ->distinct('mime')
                        ->select('mime')
                        ->get()
                        ->map(fn ($row) => $row->mime)
                )
            ],
            'orderBy' => 'nullable|array',
            'page' => 'nullable|integer|min:1|max:' . PostgreSQL::INTEGER_MAX
        ];

        if (isset($data['orderBy']) && is_array($data['orderBy'])) {
            $orderBy = &$data['orderBy'];

            $selectColumns = [
                'id',
                'directory_id',
                'directory_name',
                'status',
                'status_name',
                'name',
                'path',
                'url',
                'extension',
                'mime',
                'size',
                'created_at',
                'updated_at'
            ];

            foreach ($selectColumns as $column) {
                $rules["orderBy.$column"] = 'nullable|in:asc,desc';
            }

            $columns = array_keys($orderBy);
            if (empty(array_intersect($selectColumns, $columns)) === true) {
                $this->addError('orderBy', 'Invalid order by column');
            }
        }

        return $this->setRules($rules)->setData($data)->isValid();
    }
}
