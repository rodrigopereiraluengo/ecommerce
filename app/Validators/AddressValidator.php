<?php

namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Traits\Validator\ValidatorTrait;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class AddressValidator extends DefaultValidator implements ValidatorInterface
{
  use ValidatorTrait;

  public function save(array &$data, $user = null): bool
  {
    $country_id = Arr::get($data, 'country_id');
    $state_id = Arr::get($data, 'state_id');

    $rules = [
      'country_id' => 'required|exists:countries,id',
      'state_id' => [
        'required_if:country_id,br',
        Rule::exists('states', 'id')->where('country_id', $country_id)
      ],
      'city_id' => [
        'required_if:country_id,br',
        Rule::exists('cities', 'id')->where('state_id', $state_id)
      ],
      'address_zipcode' => 'required|max:255',
      'address_street' => 'required|max:255',
      'address_street_number' => 'required|max:255',
      'address_complementary' => 'nullable|max:255',
      'address_neighborhood' => 'required|max:255',
      'address_state' => 'required_if:country_id,<>,br|max:255',
      'address_city' => 'required_if:country_id,<>,br|max:255'
    ];

    return $this->setRules($rules)->setData($data)->isValid();
  }
}
