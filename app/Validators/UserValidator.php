<?php
namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Exceptions\ValidatorException;
use App\Traits\Validator\ValidatorTrait;
use Illuminate\Validation\Rule;

class UserValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function rules(array $data, $file = null): array
    {
        return $rules = [
            'status' => 'nullable|boolean',
            'email' => ['required', 'max:255', 'email', Rule::unique('users')->ignore($user->id ?? null)],
            'password' => 'required|min:6|max:32',
            'password_confirmation' => 'required|same:password',
            'metadata' => 'nullable|array'
        ];
    }

    public function save(array &$data, $user = null): bool
    {
        $rules = $this->rules($data, $file);
        return $this->setRules($rules)->setData($data)->isValid();
    }

    public function patch(array &$data, $file): bool
    {
        $rules = $this->rules($data, $file);
        $this->setRulesData($rules, $data);
        if (empty($this->getRules())) {
            throw new ValidatorException('No one properties found.');
        }
        return $this->isValid();
    }
}
