<?php

namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Exceptions\ValidatorException;
use App\Models\Subscription;
use App\Traits\Validator\ValidatorTrait;
use Illuminate\Validation\Rule;

class CategoryValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function rules(array $data, $category = null): array
    {
        $parent_category_id = &$data['parent_category_id'];
        return [
            'parent_category_id' => [
                'nullable',
                Rule::exists('categories', 'id')
                    ->where(function ($query) use ($category) {

                        if ($category) {
                            $query->where('id', '<>', $category->id);
                        }
                    })
            ],
            'previous_category_id' => [
                'nullable',
                Rule::exists('categories', 'id')
                    ->where(function ($query) use ($parent_category_id, $category) {

                        $query->where('parent_category_id', $parent_category_id);

                        if ($category) {
                            $query->where('id', '<>', $category->id);
                        }
                    })
            ],
            'status' => 'nullable|boolean',
            'name' => [
                'required',
                'max:45',
                Rule::unique('categories')->where('parent_category_id', $parent_category_id)
            ],
            'description' => 'nullable|max:4000',
            'metadata' => 'nullable|array'
        ];
    }

    public function save(array &$data, $category = null): bool
    {
        return $this->setRules($this->rules($data, $category))->setData($data)->isValid();
    }

    public function patch(array $data, $category): bool
    {
        $rules = $this->rules($data, $category);
        $this->setRulesData($rules, $data);
        if (empty($this->getRules())) {
            throw new ValidatorException('No one properties found.');
        }
        return $this->isValid();
    }

    public function index(array &$data): bool
    {
        $rules = [
            'default' => 'nullable|boolean',
            'q' => 'nullable|max:45',
            'parent_category_id' => [
                'nullable',
                Rule::exists('categories', 'id')
            ],
            'status' => 'nullable|boolean',
            'total_categories' => 'nullable|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'total_products' => 'nullable|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'orderBy' => 'nullable|array',
            'page' => 'nullable|integer|min:1|max:' . PostgreSQL::INTEGER_MAX
        ];

        if (isset($data['orderBy']) && is_array($data['orderBy'])) {
            $orderBy = &$data['orderBy'];

            $selectColumns = [
                'id',
                'parent_category_id',
                'category_name',
                'status',
                'status_name',
                'name',
                'description',
                'total_categories',
                'total_products',
                'created_at',
                'updated_at'
            ];

            foreach ($selectColumns as $column) {
                $rules["orderBy.$column"] = 'nullable|in:asc,desc';
            }

            $columns = array_keys($orderBy);
            if (empty(array_intersect($selectColumns, $columns)) === true) {
                $this->addError('orderBy', 'Invalid order by column');
            }
        }

        return $this->setRules($rules)->setData($data)->isValid();
    }

    public function enable(array &$data): bool
    {
        $rules = [
            'id' => 'required|array',
            'id.*' => [
                'required',
                Rule::exists('categories', 'id')
            ]
        ];

        return $this->setRules($rules)->setData($data)->isValid();
    }
}
