<?php
namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Traits\Validator\ValidatorTrait;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class PersonValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $person = null): bool
    {
        $rules = [
            'name' => 'required|max:255',
            'company_name' => 'nullable|max:255',
            'type' => 'required|in:F,J',
            'born_at' =>  ['required', 'date_format:Y-m-d'],
            'sex' => 'required_if:type,F|in:F,M',
            'document_number' => [
                'required',
                Rule::unique('people')->ignore($person->id ?? null)
            ],
        ];

        $type = Arr::get($data, 'type');
        if ($type) {
            $rules['document_number'][] = $type === 'F' ? 'cpf' : 'cnpj';
        }

        $born_at = Arr::get($data, 'born_at');
        if ($born_at) {
            try {
                $born_at = Carbon::createFromFormat('Y-m-d', $data['born_at']);
                if ($born_at->isAfter(Carbon::now()->subYears(18))) {
                    $this->addError('born_at', 'Você deve ter ao menos 18 anos de idade para poder prosseguir.');
                }
            } catch (\Exception $e) {}
        }

        return $this->setRules($rules)->setData($data)->isValid();
    }

    public function addresses(array &$data, $id): bool
    {
        $rules = [
            'addresses' => 'required|array'
        ];

        $addresses = Arr::get($data, 'addresses');

        if (is_array($addresses)) {
            foreach ($addresses as $i => $address) {

                $address_id = Arr::get($address, 'id');

                $rules["addresses.$i.name"] = [
                    'required',
                    'max:255',
                    Rule::unique('address_person', 'name')
                        ->where('person_id', $id)
                        ->ignore($address_id, 'address_id')
                ];
            }
        }

        return $this->setRules($rules)->setData($data)->isValid();
    }
}
