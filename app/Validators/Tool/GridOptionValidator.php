<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class GridOptionValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $id = $model->id ?? null;
        $subscription_id = $data['subscription_id'];
        $tool_id = &$data['tool_id'];

        $rules = [
            'tool_id' => [
                'required',
                Rule::exists('tools', 'id')
                    ->where('type', Tool::TYPE_FORM)
                    ->where('subscription_id', $subscription_id)
            ],
            'metadata.position' => 'required|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.name' => "required|max:255|tool_unique_name:gridOption,$subscription_id,$id,$tool_id",
            'metadata.description' => 'nullable|max:4000',
            'metadata.show_name' => 'nullable|boolean',
            'metadata.required' => 'nullable|boolean',
            'metadata.readonly' => 'nullable|boolean',
            'metadata.min' => 'nullable|integer|min:0',
            'metadata.max' => 'nullable|integer|min:0',
            'metadata.show_not_applicable' => 'nullable|boolean',
            'metadata.not_applicable_selected' => 'nullable|boolean',
            'metadata.not_applicable' => 'nullable|required_if:metadata.show_not_applicable,true|max:255',
            'metadata.rows' => 'required|array',
            'metadata.rows.*.name' => 'required|max:255|distinct',
            'metadata.rows.*.status' => 'nullable|boolean',
            'metadata.cols' => 'required|array',
            'metadata.cols.*.name' => 'required|max:255|distinct',
            'metadata.cols.*.status' => 'nullable|boolean',
            'metadata.cols.*.selected' => 'nullable|boolean',
        ];

        if ($model) {
            unset($rules['tool_id']);
        }

        $cols = Arr::get($data, 'metadata.cols');
        $min = Arr::get($data, 'metadata.min');
        $max = Arr::get($data, 'metadata.max');
        $show_not_applicable = Arr::get($data, 'metadata.show_not_applicable');
        $not_applicable_selected = Arr::get($data, 'metadata.not_applicable_selected');
        $not_applicable = Arr::get($data, 'metadata.not_applicable');
        $rows = Arr::get($data, 'metadata.rows');

        $this->validator = Validator::make($data, $rules);
        $this->validator->after(function ($validator) use ($cols, $min, $max, $show_not_applicable, $not_applicable_selected, $not_applicable, $rows) {

            $cols_count = 0;
            if ($this->validator->errors()->has('metadata.cols') === false) {
                $cols_count = collect($cols)->where('status', true)->count();
            }

            if ($show_not_applicable) {
                $cols_count++;

                if (
                    $not_applicable !== null
                    && $this->validator->errors()->has('metadata.not_applicable') === false
                    && $this->validator->errors()->has('metadata.cols') === false
                    && collect($cols)->where('status', true)->where('name', $not_applicable)->isNotEmpty()
                ) {
                    $validator
                        ->errors()
                        ->add('metadata.not_applicable', __('validation.gridOption.not_applicable.there_are_cols_with_name'));
                }
            }

            if ($validator->errors()->has('metadata.cols') === false) {
                $selected = 0;
                foreach ($cols as $i => $item) {
                    $itemSelected = Arr::get($item, 'selected');
                    if ($itemSelected) {
                        $selected++;
                    }
                    if ($selected > 1 && $itemSelected) {
                        $validator->errors()->add("metadata.cols.$i.selected", "Apenas uma escolha pode ser selecionada.");
                    }
                }

                if ($not_applicable_selected === true) {
                    $selected++;
                    if ($selected > 1) {
                        $validator->errors()->add("metadata.not_applicable_selected", "Apenas uma escolha pode ser selecionada.");
                    }
                }
            }

            if (
                $min !== null
                && $this->validator->errors()->has('metadata.min') === false
                && empty($cols_count) === false
                && $min > $cols_count
            ) {
                $validator->errors()->add('metadata.min', __('validation.gridOption.min.min_greater_than_cols'));
            }

            if (
                $max !== null
                && $this->validator->errors()->has('metadata.max') === false
                && empty($cols_count) === false
                && $max > $cols_count
            ) {
                $validator->errors()->add('metadata.max', __('validation.gridOption.max.max_greater_than_cols'));
            }

            if (
                $min !== null
                && $max !== null
                && $this->validator->errors()->has('metadata.min') === false
                && $this->validator->errors()->has('metadata.max') === false) {

                if ($max < $min) {
                    $validator->errors()->add('metadata.max', __('validation.gridOption.max.max_greater_min_invalid'));
                }
            }

            if ($this->validator->errors()->has('metadata.rows') === false && collect($rows)->where('status', true)->isEmpty()) {
                $validator->errors()->add('metadata.rows', 'Ao menos uma linha de estar ativa.');
            }

            if ($this->validator->errors()->has('metadata.cols') === false && collect($rows)->where('status', true)->isEmpty()) {
                $validator->errors()->add('metadata.cols', 'Ao menos uma coluna de estar ativa.');
            }
        });

        return $this->isValid();
    }
}
