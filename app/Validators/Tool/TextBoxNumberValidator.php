<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TextBoxNumberValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $id = $model->id ?? null;
        $subscription_id = $data['subscription_id'];
        $tool_id = &$data['tool_id'];

        $rules = [
            'tool_id' => [
                'required',
                Rule::exists('tools', 'id')
                    ->where('type', Tool::TYPE_FORM)
                    ->where('subscription_id', $subscription_id)
            ],
            'metadata.position' => 'required|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.name' => "required|max:255|tool_unique_name:textBoxNumber,$subscription_id,$id,$tool_id",
            'metadata.size' => ['required', Rule::in(Tool::SIZE_SMALL, Tool::SIZE_MEDIUM, Tool::SIZE_BIG)],
            'metadata.decimal_places' => 'required|integer|min:0|max:8',
            'metadata.currency' => 'nullable|exists:currencies,id',
            'metadata.description' => 'nullable|max:4000',
            'metadata.show_name' => 'nullable|boolean',
            'metadata.required' => 'nullable|boolean',
            'metadata.readonly' => 'nullable|boolean',
            'metadata.not_duplicated' => 'nullable|boolean',
            'metadata.min' => 'nullable|numeric|min:' . PostgreSQL::INTEGER_MIN . '|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.max' => 'nullable|numeric|min:' . PostgreSQL::INTEGER_MIN . '|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.default_value' => 'nullable|numeric|min:' . PostgreSQL::INTEGER_MIN . '|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.placeholder' => 'nullable|max:125'
        ];

        if ($model) {
            unset($rules['tool_id']);
        }

        $decimal_places = Arr::get($data, 'metadata.decimal_places');
        $min            = Arr::get($data, 'metadata.min');
        $max            = Arr::get($data, 'metadata.max');
        $default_value  = Arr::get($data, 'metadata.default_value');

        $this->validator = Validator::make($data, $rules);
        $this->validator->after(function ($validator) use ($decimal_places, $min, $max, $default_value) {

            if ($min !== null && $this->validator->errors()->has('metadata.min') === false) {
                list($value, $places) = sscanf($min, '%d.%d');
                if ($places !== null && strlen($places) > $decimal_places) {
                    $validator->errors()->add('metadata.min', __('validation.textBoxNumber.min.decimal_places_invalid'));
                }
            }

            if ($max !== null && $this->validator->errors()->has('metadata.max') === false) {
                list($value, $places) = sscanf($max, '%d.%d');
                if ($places !== null && strlen($places) > $decimal_places) {
                    $validator->errors()->add('metadata.max', __('validation.textBoxNumber.max.decimal_places_invalid'));
                }
            }

            if (
                $min !== null
                && $max !== null
                && $this->validator->errors()->has('metadata.min') === false
                && $this->validator->errors()->has('metadata.max') === false) {

                if ($max < $min) {
                    $validator->errors()->add('metadata.max', __('validation.textBoxNumber.max.max_greater_min_invalid'));
                }
            }

            if (
                empty($default_value) === false
                && ($min !== null || $max !== null)
                && $this->validator->errors()->has('metadata.min') === false
                && $this->validator->errors()->has('metadata.max') === false
                && $this->validator->errors()->has('metadata.default_value') === false) {
                if ($min && $default_value < $min) {
                    $validator->errors()->add('metadata.default_value', __('validation.textBoxNumber.default_value.min_invalid'));
                }

                if ($max && $default_value > $max) {
                    $validator->errors()->add('metadata.default_value', __('validation.textBoxNumber.default_value.max_invalid'));
                }
            }

            if (empty($default_value) === false
                && empty($decimal_places) === false
                && $this->validator->errors()->has('metadata.default_value') === false
                && $this->validator->errors()->has('metadata.decimal_places') === false
                && $decimal_places > 0) {

                list($value, $places) = sscanf($default_value, '%d.%d');
                if ($places !== null && strlen($places) > $decimal_places) {
                    $validator->errors()->add('metadata.default_value', __('validation.textBoxNumber.default_value.decimal_places_invalid'));
                }
            }

        });

        return $this->isValid();
    }
}
