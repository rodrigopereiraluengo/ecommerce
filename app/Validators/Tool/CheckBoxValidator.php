<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CheckBoxValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $id = $model->id ?? null;
        $subscription_id = $data['subscription_id'];
        $tool_id = &$data['tool_id'];

        $rules = [
            'tool_id' => [
                'required',
                Rule::exists('tools', 'id')
                    ->where('type', Tool::TYPE_FORM)
                    ->where('subscription_id', $subscription_id)
            ],
            'metadata.position' => 'required|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.name' => "required|max:255|tool_unique_name:checkBox,$subscription_id,$id,$tool_id",
            'metadata.description' => 'nullable|max:4000',
            'metadata.show_name' => 'nullable|boolean',
            'metadata.required' => 'nullable|boolean',
            'metadata.readonly' => 'nullable|boolean',
            'metadata.min' => 'nullable|integer|min:0',
            'metadata.max' => 'nullable|integer|min:0',
            'metadata.show_other' => 'nullable|boolean',
            'metadata.other_selected' => 'nullable|boolean',
            'metadata.other' => 'nullable|required_if:metadata.show_other,true|max:255',
            'metadata.items' => 'required|array',
            'metadata.items.*.name' => 'required|max:255|distinct',
            'metadata.items.*.status' => 'nullable|boolean',
            'metadata.items.*.selected' => 'nullable|boolean',
        ];

        if ($model) {
            unset($rules['tool_id']);
        }

        $items = Arr::get($data, 'metadata.items');
        $min = Arr::get($data, 'metadata.min');
        $max = Arr::get($data, 'metadata.max');
        $show_other = Arr::get($data, 'metadata.show_other');
        $other_selected = Arr::get($data, 'metadata.other_selected');
        $other = Arr::get($data, 'metadata.other');

        $this->validator = Validator::make($data, $rules);
        $this->validator->after(function ($validator) use ($items, $min, $max, $show_other, $other_selected, $other) {

            $items_active = 0;
            if ($this->validator->errors()->has('metadata.items') === false) {
                $items_active = collect($items)->where('status', true)->count();
            }

            if ($show_other) {
                $items_active++;

                if (
                    $other !== null
                    && $this->validator->errors()->has('metadata.other') === false
                    && $this->validator->errors()->has('metadata.items') === false
                    && collect($items)->where('status', true)->where('name', $other)->isNotEmpty()
                ) {
                    $validator
                        ->errors()
                        ->add('metadata.other', __('validation.checkBox.other.there_are_items_with_name'));
                }
            }

            if ($items_active === 0) {
                $validator->errors()->add("metadata.items", "Ao menos uma escolha dever estar ativa.");
            }

            if (
                $min !== null
                && $this->validator->errors()->has('metadata.min') === false
                && empty($items_active) === false
                && $min > $items_active
            ) {
                $validator->errors()->add('metadata.min', __('validation.checkBox.min.min_greater_than_items'));
            }

            if (
                $max !== null
                && $this->validator->errors()->has('metadata.max') === false
                && empty($items_active) === false
                && $max > $items_active
            ) {
                $validator->errors()->add('metadata.max', __('validation.checkBox.max.max_greater_than_items'));
            }

            if (
                $min !== null
                && $max !== null
                && $this->validator->errors()->has('metadata.min') === false
                && $this->validator->errors()->has('metadata.max') === false) {

                if ($max < $min) {
                    $validator->errors()->add('metadata.max', __('validation.checkBox.max.max_greater_min_invalid'));
                }
            }

        });

        return $this->isValid();
    }
}
