<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TextBoxEmailValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $id = $model->id ?? null;
        $subscription_id = $data['subscription_id'];
        $tool_id = &$data['tool_id'];

        $rules = [
            'tool_id' => [
                'required',
                Rule::exists('tools', 'id')
                    ->where('type', Tool::TYPE_FORM)
                    ->where('subscription_id', $subscription_id)
            ],
            'metadata.position' => 'required|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.name' => "required|max:255|tool_unique_name:textBoxEmail,$subscription_id,$id,$tool_id",
            'metadata.size' => ['required', Rule::in(Tool::SIZE_SMALL, Tool::SIZE_MEDIUM, Tool::SIZE_BIG)],
            'metadata.description' => 'nullable|max:4000',
            'metadata.show_name' => 'nullable|boolean',
            'metadata.required' => 'nullable|boolean',
            'metadata.readonly' => 'nullable|boolean',
            'metadata.not_duplicated' => 'nullable|boolean',
            'metadata.min' => 'nullable|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.max' => 'nullable|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.default_value' => 'nullable|max:8000|email',
            'metadata.placeholder' => 'nullable|max:8000'
        ];

        if ($model) {
            unset($rules['tool_id']);
        }

        $min            = Arr::get($data, 'metadata.min');
        $max            = Arr::get($data, 'metadata.max');
        $default_value  = Arr::get($data, 'metadata.default_value');

        $this->validator = Validator::make($data, $rules);
        $this->validator->after(function ($validator) use ($min, $max, $default_value) {

            if (
                $min !== null
                && $max !== null
                && $this->validator->errors()->has('metadata.min') === false
                && $this->validator->errors()->has('metadata.max') === false) {

                if ($max < $min) {
                    $validator->errors()->add('metadata.max', __('validation.textBoxMask.max.max_greater_min_invalid'));
                }
            }

            if (
                empty($default_value) === false
                && ($min !== null || $max !== null)
                && $this->validator->errors()->has('metadata.min_max_type') === false
                && $this->validator->errors()->has('metadata.min') === false
                && $this->validator->errors()->has('metadata.max') === false
                && $this->validator->errors()->has('metadata.default_value') === false
            ) {
                if ($min && strlen($default_value) < $min) {
                    $validator->errors()->add('metadata.default_value', __('validation.textBoxEmail.default_value.min_invalid'));
                }

                if ($max && strlen($default_value) > $max) {
                    $validator->errors()->add('metadata.default_value', __('validation.textBoxEmail.default_value.max_invalid'));
                }
            }
        });

        return $this->isValid();
    }
}
