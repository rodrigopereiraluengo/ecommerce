<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class DateTimeValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $id = $model->id ?? null;
        $subscription_id = $data['subscription_id'];
        $tool_id = &$data['tool_id'];

        $rules = [
            'tool_id' => [
                'required',
                Rule::exists('tools', 'id')
                    ->where('type', Tool::TYPE_FORM)
                    ->where('subscription_id', $subscription_id)
            ],
            'metadata.position' => 'required|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.name' => "required|max:255|tool_unique_name:textBox,$subscription_id,$id,$tool_id",
            'metadata.type' => [
                'required', Rule::in(Tool::DATE_TIME_TYPE_DATE, Tool::DATE_TIME_TYPE_DATE_TIME, Tool::DATE_TIME_TYPE_TIME)
            ],
            'metadata.description' => 'nullable|max:4000',
            'metadata.placeholder' => 'nullable|max:4000',
            'metadata.show_name' => 'nullable|boolean',
            'metadata.required' => 'nullable|boolean',
            'metadata.readonly' => 'nullable|boolean',

            'metadata.min_type' => 'nullable|in:now,other',
            'metadata.min_now_period' => ['nullable'],
            'metadata.min_now_value' => 'nullable|required_with:metadata.min_now_period|integer|min:' . PostgreSQL::INTEGER_MIN . '|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.min_other' => ['nullable', 'required_if:metadata.min_type,other'],

            'metadata.max_type' => 'nullable|in:now,other',
            'metadata.max_now_period' => ['nullable'],
            'metadata.max_now_value' => 'nullable|required_with:metadata.max_now_period|integer|min:' . PostgreSQL::INTEGER_MIN . '|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.max_other' => ['nullable', 'required_if:metadata.min_type,other'],

            'metadata.default_type' => 'nullable|in:now,other',
            'metadata.default_now_period' => ['nullable'],
            'metadata.default_now_value' => 'nullable|required_with:metadata.default_now_period|integer|min:' . PostgreSQL::INTEGER_MIN . '|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.default_other' => ['nullable', 'required_if:metadata.default_type,other'],
        ];

        if ($model) {
            unset($rules['tool_id']);
        }

        $type = Arr::get($data, 'metadata.type');
        switch ($type) {
            case Tool::DATE_TIME_TYPE_DATE:
                $rules['metadata.min_now_period'][] = Rule::in('days', 'weeks', 'months', 'years');
                $rules['metadata.max_now_period'][] = Rule::in('days', 'weeks', 'months', 'years');
                $rules['metadata.default_now_period'][] = Rule::in('days', 'weeks', 'months', 'years');
                $rules['metadata.min_other'][] = 'date_format:Y-m-d';
                $rules['metadata.max_other'][] = 'date_format:Y-m-d';
                $rules['metadata.default_other'][] = 'date_format:Y-m-d';
                break;
            case Tool::DATE_TIME_TYPE_DATE_TIME:
                $rules['metadata.min_now_period'][] = Rule::in('days', 'weeks', 'months', 'years', 'hours', 'minutes');
                $rules['metadata.max_now_period'][] = Rule::in('days', 'weeks', 'months', 'years', 'hours', 'minutes');
                $rules['metadata.default_now_period'][] = Rule::in('days', 'weeks', 'months', 'years', 'hours', 'minutes');
                $rules['metadata.min_other'][] = 'date_format:Y-m-d H:i';
                $rules['metadata.max_other'][] = 'date_format:Y-m-d H:i';
                $rules['metadata.default_other'][] = 'date_format:Y-m-d H:i';
                break;
            case Tool::DATE_TIME_TYPE_TIME:
                $rules['metadata.min_now_period'][] = Rule::in('hours', 'minutes');
                $rules['metadata.max_now_period'][] = Rule::in('hours', 'minutes');
                $rules['metadata.default_now_period'][] = Rule::in('hours', 'minutes');
                $rules['metadata.min_other'][] = 'date_format:H:i';
                $rules['metadata.max_other'][] = 'date_format:H:i';
                $rules['metadata.default_other'][] = 'date_format:H:i';
                break;
        }

        $this->validator = Validator::make($data, $rules);

        $this->validator->after(function ($validator) use ($type, $data) {

            $min = null;
            $min_type = Arr::get($data, 'metadata.min_type');
            $min_period = Arr::get($data, 'metadata.min_now_period');
            $min_now_value = Arr::get($data, 'metadata.min_now_value');
            $min_other = Arr::get($data, 'metadata.min_other');
            if ((
                $min_type === 'now'
                && $validator->errors()->has('metadata.min_now_period') === false
                && $validator->errors()->has('metadata.min_now_value') === false
            ) || (
                $min_type === 'other'
                && $validator->errors()->has('metadata.min_other') === false
                )
            ) {
                $min = Tool::makeDateFromSettings($min_type, $min_period, $min_now_value, $min_other);
            }

            $max = null;
            $max_type = Arr::get($data, 'metadata.max_type');
            $max_period = Arr::get($data, 'metadata.max_now_period');
            $max_now_value = Arr::get($data, 'metadata.max_now_value');
            $max_other = Arr::get($data, 'metadata.max_other');
            if ((
                    $max_type === 'now'
                    && $validator->errors()->has('metadata.max_now_period') === false
                    && $validator->errors()->has('metadata.max_now_value') === false
                ) || (
                    $max_type === 'other'
                    && $validator->errors()->has('metadata.max_other') === false
                )
            ) {
                $max = Tool::makeDateFromSettings($max_type, $max_period, $max_now_value, $max_other);
            }

            $default = null;
            $default_type = Arr::get($data, 'metadata.default_type');
            $default_period = Arr::get($data, 'metadata.default_now_period');
            $default_now_value = Arr::get($data, 'metadata.default_now_value');
            $default_other = Arr::get($data, 'metadata.default_other');
            if ((
                    $default_type === 'now'
                    && $validator->errors()->has('metadata.default_now_period') === false
                    && $validator->errors()->has('metadata.default_now_value') === false
                ) || (
                    $default_type === 'other'
                    && $validator->errors()->has('metadata.default_other') === false
                )
            ) {
                $default = Tool::makeDateFromSettings($default_type, $default_period, $default_now_value, $default_other);
            }

            if ($min instanceof Carbon && $max instanceof Carbon && $max->isBefore($min)) {
                $validator->errors()->add('metadata.max', 'A data máxima não deve ser inferior a data mínima.');
            }

            if ($min instanceof Carbon && $default instanceof Carbon && $default->isBefore($min)) {
                $validator->errors()->add('metadata.default', 'A data padrão não deve ser inferior a data mínima.');
            }

            if ($max instanceof Carbon && $default instanceof Carbon && $default->isAfter($max)) {
                $validator->errors()->add('metadata.default', 'A data padrão não deve ser superior a data máxima.');
            }

        });

        return $this->isValid();
    }

}
