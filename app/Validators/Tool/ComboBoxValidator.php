<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ComboBoxValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $id = $model->id ?? null;
        $subscription_id = $data['subscription_id'];
        $tool_id = &$data['tool_id'];

        $rules = [
            'tool_id' => [
                'required',
                Rule::exists('tools', 'id')
                    ->where('type', Tool::TYPE_FORM)
                    ->where('subscription_id', $subscription_id)
            ],
            'metadata.position' => 'required|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.name' => "required|max:255|tool_unique_name:comboBox,$subscription_id,$id,$tool_id",
            'metadata.size' => ['required', Rule::in(Tool::SIZE_SMALL, Tool::SIZE_MEDIUM, Tool::SIZE_BIG)],
            'metadata.description' => 'nullable|max:4000',
            'metadata.show_name' => 'nullable|boolean',
            'metadata.required' => 'nullable|boolean',
            'metadata.readonly' => 'nullable|boolean',
            'metadata.not_duplicated' => 'nullable|boolean',
            'metadata.placeholder' => 'nullable|max:255',
            'metadata.items' => 'required|array',
            'metadata.items.*.name' => 'required|max:255|distinct',
            'metadata.items.*.status' => 'nullable|boolean',
            'metadata.items.*.selected' => 'nullable|boolean',
        ];

        if ($model) {
            unset($rules['tool_id']);
        }

        $this->validator = Validator::make($data, $rules);

        $this->validator->after(function ($validator) use ($data) {

            if ($validator->errors()->has('metadata.items') === false) {

                if (collect($data['metadata']['items'])->where('status', true)->isEmpty()) {
                    $validator->errors()->add("metadata.items", "Ao menos uma escolha dever estar ativa.");
                }

                $selected = 0;
                foreach ($data['metadata']['items'] as $i => $item) {
                    $itemSelected = Arr::get($item, 'selected');
                    if ($itemSelected) {
                        $selected++;
                    }
                    if ($selected > 1 && $itemSelected) {
                        $validator->errors()->add("metadata.items.$i.selected", "Apenas uma escolha pode ser selecionada.");
                    }
                }
            }

        });

        return $this->isValid();
    }
}
