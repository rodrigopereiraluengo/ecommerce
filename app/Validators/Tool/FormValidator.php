<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class FormValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $id = $model->id ?? null;
        $subscription_id = $data['subscription_id'];
        $tool_id = &$data['tool_id'];

        $rules = [
            'metadata.name' => "required|max:255|tool_unique_name:form,$subscription_id,$id,$tool_id",
            'metadata.sync_path' => "required|boolean",
            'metadata.path' => "required|max:255|tool_unique_path:form,$subscription_id,$id,$tool_id",
            'metadata.description' => 'nullable|max:4000',
            'metadata.captcha' => 'nullable|boolean',
            'metadata.one_entry_session' => 'nullable|boolean',
            'metadata.protected' => 'nullable|boolean',
            'metadata.password' => 'required_if:metadata.protected,true|max:255',
            'metadata.qty_access' => 'nullable|integer|min:1|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.qty_register' => 'nullable|integer|min:1|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.date_start_at' => 'nullable|date_format:Y-m-d',
            'metadata.time_start_at' => 'nullable|date_format:H:i',
            'metadata.date_end_at' => 'nullable|date_format:Y-m-d',
            'metadata.time_end_at' => 'nullable|date_format:H:i',
            'metadata.submit' => 'required|max:50',
            'metadata.confirm.type' => [
                'required',
                Rule::in(Tool::CONFIRM_SHOW_MESSAGE, Tool::CONFIRM_REDIRECT)
            ],
            'metadata.confirm.message' => 'nullable|required_if:metadata.confirm.type,' . Tool::CONFIRM_SHOW_MESSAGE . '|max:4000',
            'metadata.confirm.url' => 'nullable|required_if:metadata.confirm.type,' . Tool::CONFIRM_REDIRECT . '|max:8000|url',
            'metadata.confirm.send_email' => 'nullable|boolean',
            'metadata.confirm.email_name' => 'nullable|required_if:metadata.confirm.send_email,true|max:50',
            'metadata.confirm.email_address' => 'nullable|required_if:metadata.confirm.send_email,true|max:255|email',
            'metadata.confirm.email_cc' => 'nullable|array',
            'metadata.confirm.email_cc.*' => 'nullable|email',
            'metadata.confirm.email_bcc' => 'nullable|array',
            'metadata.confirm.email_bcc.*' => 'nullable|email',
            'metadata.confirm.email_subject' => 'nullable|required_if:metadata.confirm.send_email,true|max:255',
            'metadata.confirm.email_message' => 'nullable|required_if:metadata.confirm.send_email,true|max:4000',
        ];

        $this->validator = Validator::make($data, $rules);

        $this->validator->after(function ($validator) use ($data, $model) {

            $name       = &$data['metadata']['name'];
            $sync_path  = &$data['metadata']['sync_path'];
            $path       = &$data['metadata']['path'];

            if ($sync_path === true && empty($path) === false && $path !== Str::slug($name)) {
                $validator->errors()->add('metadata.path', 'The path column needs be sync as name.');
            }

            if ($sync_path === false
                && empty($path) === false
                && preg_match('/^[a-z0-9]+(?:-[a-z0-9]+)*$/', $path) === 0) {
                $validator->errors()->add('metadata.path', 'The path column needs be a valid slug.');
            }

            $date_start_at  = Arr::get($data, 'metadata.date_start_at');
            $time_start_at  = Arr::get($data,'metadata.time_start_at');
            $date_end_at    = Arr::get($data,'metadata.date_end_at');
            $time_end_at    = Arr::get($data,'metadata.time_end_at');

            if (
                $validator->errors()->has('metadata.date_start_at') === false
                && $validator->errors()->has('metadata.time_start_at') === false
                && $validator->errors()->has('metadata.date_end_at') === false
                && $validator->errors()->has('metadata.time_end_at') === false) {

                if ($date_start_at && $time_start_at) {
                    $date_start_at = Carbon::createFromFormat('Y-m-d H:i', $date_start_at . ' ' . $time_start_at);
                } elseif ($date_start_at) {
                    $date_start_at = Carbon::createFromFormat('Y-m-d', $date_start_at);
                    $date_start_at->setTime(0, 0);
                } elseif ($time_start_at) {
                    $time_start_at = Carbon::createFromFormat('H:i', $time_start_at);
                }

                if ($date_end_at && $time_end_at) {
                    $date_end_at = Carbon::createFromFormat('Y-m-d H:i', $date_end_at . ' ' . $time_end_at);
                } elseif ($date_end_at) {
                    $date_end_at = Carbon::createFromFormat('Y-m-d', $date_end_at);
                    $date_end_at->setTime(0, 0);
                } elseif ($time_end_at) {
                    $time_end_at = Carbon::createFromFormat('H:i', $time_end_at);
                }

                if ($date_start_at && $date_end_at && $date_end_at->isBefore($date_start_at)) {
                    $column = $date_end_at->diffInDays($date_start_at) ? 'date_end_at' : 'time_end_at';
                    $validator->errors()->add("metadata.$column", 'The end of the activities needs be after start of the activities.');
                } elseif ($time_start_at instanceof Carbon && $time_end_at instanceof Carbon && $time_end_at->isBefore($time_start_at)) {
                    $validator->errors()->add("metadata.time_end_at", 'The end of the activities needs be after start of the activities.');
                }

                if ($model == null) {
                    $now = Carbon::now();
                    if ($date_start_at && $date_start_at->isBefore($now)) {
                        $column = $date_start_at->diffInDays($now) ? 'date_start_at' : 'time_start_at';
                        $validator->errors()->add("metadata.$column", 'The start of the activities needs be after or equal now.');
                    }
                }
            }

        });

        return $this->isValid();
    }
}
