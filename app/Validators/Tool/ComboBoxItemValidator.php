<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ComboBoxItemValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $id = $model->id ?? null;
        $subscription_id = $data['subscription_id'];
        $tool_id = &$data['tool_id'];

        $rules = [
            'tool_id' => [
                'required',
                Rule::exists('tools', 'id')
                    ->where('type', Tool::TYPE_COMBO_BOX)
                    ->where('subscription_id', $subscription_id)
            ],
            'metadata.position' => 'required|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata.name' => "required|max:255|tool_unique_name:comboBoxItem,$subscription_id,$id,$tool_id",
            'metadata.selected' => 'nullable|boolean',
        ];

        if ($model) {
            unset($rules['tool_id']);
        }

        $this->validator = Validator::make($data, $rules);

        return $this->isValid();
    }
}
