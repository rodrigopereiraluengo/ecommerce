<?php
namespace App\Validators\Tool;

use App\Contracts\ValidatorInterface;
use App\Models\Tool;
use App\Traits\Validator\ValidatorTrait;
use App\Validators\DefaultValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ToolValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $model = null): bool
    {
        $rules = [
            'subscription_id' => 'nullable|exists:subscriptions,id',
            'tool_id' => 'nullable|exists:tools,id',
            'theme_id' => 'nullable|exists:themes,id',
            'type' => [
                'required',
                Rule::in(
                    Tool::TYPE_FORM,
                    Tool::TYPE_TEXT_BOX,
                    Tool::TYPE_TEXT_BOX_NAME,
                    Tool::TYPE_TEXT_BOX_EMAIL,
                    Tool::TYPE_TEXT_BOX_MASK,
                    Tool::TYPE_TEXT_BOX_NUMBER,
                    Tool::TYPE_TEXT_BOX_URL,
                    Tool::TYPE_TEXT_AREA,
                    Tool::TYPE_DATE_TIME,
                    Tool::TYPE_COMBO_BOX,
                    Tool::TYPE_COMBO_BOX_ITEM,
                    Tool::TYPE_CHECK_BOX,
                    Tool::TYPE_RADIO_BUTTON,
                    Tool::TYPE_GRID_OPTION,
                    Tool::TYPE_RATING,
                    Tool::TYPE_UPLOAD_FILE
                )
            ],
            'status' => 'nullable|boolean',
            'metadata' => 'required|array'
        ];

        $this->validator = Validator::make($data, $rules);

        if ($this->validator->errors()->has('type') === false && $this->validator->errors()->has('metadata') === false) {

            $validatorClassName = 'App\\Validators\\Tool\\' . Str::studly($data['type']) . 'Validator';
            if (class_exists($validatorClassName)) {

                $validator = app($validatorClassName);

                $this->addValidator($validator);
                if ($validator->save($data, $model) === false) {
                    $this->addErrors($validator->getErrors());
                }
            }
        }

        return $this->isValid();
    }

    public function index(array &$data): bool
    {
        return true;
    }

    public function publish($model): bool
    {
        return true;
    }
}
