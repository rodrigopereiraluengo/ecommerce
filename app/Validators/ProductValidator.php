<?php

namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Exceptions\ValidatorException;
use App\Models\Subscription;
use App\Traits\Validator\ValidatorTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ProductValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function rules(array $data, $product): array
    {
        return [
            'category_id' => [
                'required',
                Rule::exists('categories', 'id')
            ],
            'status' => 'nullable|boolean',
            'name' => [
                'required',
                'max:45',
                Rule::unique('products')
            ],
            'description' => 'nullable|max:4000',
            'quantity' => 'nullable|numeric|min:' . PostgreSQL::INTEGER_MIN . '|max:' . PostgreSQL::INTEGER_MAX,
            'price' => 'required|numeric|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'metadata' => 'nullable|array'
        ];
    }

    public function save(array &$data, $product = null): bool
    {
        return $this->setRules($this->rules($data, $product))->setData($data)->isValid();
    }

    public function patch(array $data, $product): bool
    {
        $rules = $this->rules($data, $product);
        $this->setRulesData($rules, $data);
        if (empty($this->getRules())) {
            throw new ValidatorException('No one properties found.');
        }
        return $this->isValid();
    }

    public function index(array &$data): bool
    {
        $rules = [
            'q' => 'nullable|max:45',
            'category_id' => [
                'nullable',
                Rule::exists('categories', 'id')
            ],
            'status' => 'nullable|boolean',
            'quantity' => 'nullable|numeric|min:' . PostgreSQL::INTEGER_MIN . '|max:' . PostgreSQL::INTEGER_MAX,
            'price' => 'nullable|numeric|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'total_products' => 'nullable|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'orderBy' => 'nullable|array',
            'page' => 'nullable|integer|min:1|max:' . PostgreSQL::INTEGER_MAX
        ];

        if (isset($data['orderBy']) && is_array($data['orderBy'])) {

            $orderBy = $data['orderBy'];

            $selectColumns = [
                'id',
                'category_id',
                'category_name',
                'status',
                'status_name',
                'name',
                'description',
                'quantity',
                'price',
                'total_products',
                'created_at',
                'updated_at'
            ];

            foreach ($selectColumns as $column) {
                $rules["orderBy.$column"] = 'nullable|in:asc,desc';
            }

            $columns = array_keys($orderBy);
            if (empty(array_intersect($selectColumns, $columns)) === true) {
                $this->addError('orderBy', 'Invalid order by column');
            }
        }

        return $this->setRules($rules)->setData($data)->isValid();
    }
}
