<?php

namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Enums\PostgreSQL;
use App\Exceptions\ValidatorException;
use App\Models\Subscription;
use App\Traits\Validator\ValidatorTrait;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class DirectoryValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function rules(array $data, $directory = null): array
    {
        $directory_id = &$data['directory_id'];
        return [
            'directory_id' => [
                'nullable',
                Rule::exists('directories', 'id')->where(function ($query) use ($directory) {
                    if ($directory) {
                        $query->where('directory_id', '<>', $directory->id);
                    }
                })
            ],
            'status' => 'nullable|boolean',
            'name' => [
                'required',
                'max:45',
                Rule::unique('directories')->where(function ($query) use ($directory, $directory_id) {
                    $query->where('directory_id', $directory_id);

                    if ($directory) {
                        $query->where('id', '<>', $directory->id);
                    }
                })
            ],
            'description' => 'nullable|max:4000',
            'metadata' => 'nullable|array'
        ];
    }

    public function save(array &$data, $directory = null): bool
    {
        return $this->setRules($this->rules($data, $directory))->setData($data)->isValid();
    }

    public function patch(array $data, $directory): bool
    {
        $rules = $this->rules($data, $directory);
        $this->setRulesData($rules, $data);
        if (empty($this->getRules())) {
            throw new ValidatorException('No one properties found.');
        }
        return $this->isValid();
    }

    public function index(array &$data): bool
    {
        $rules = [
            'q' => 'nullable|max:45',
            'directory_id' => [
                'nullable',
                Rule::exists('categories', 'id')
            ],
            'status' => 'nullable|boolean',
            'total_directories' => 'nullable|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'total_files' => 'nullable|integer|min:0|max:' . PostgreSQL::INTEGER_MAX,
            'orderBy' => 'nullable|array',
            'page' => 'nullable|integer|min:1|max:' . PostgreSQL::INTEGER_MAX
        ];

        if (isset($data['orderBy']) && is_array($data['orderBy'])) {
            $orderBy = &$data['orderBy'];

            $selectColumns = [
                'id',
                'directory_id',
                'directory_name',
                'status',
                'status_name',
                'name',
                'description',
                'total_directories',
                'total_files',
                'created_at',
                'updated_at'
            ];

            foreach ($selectColumns as $column) {
                $rules["orderBy.$column"] = 'nullable|in:asc,desc';
            }

            $columns = array_keys($orderBy);
            if (empty(array_intersect($selectColumns, $columns)) === true) {
                $this->addError('orderBy', 'Invalid order by column');
            }
        }

        return $this->setRules($rules)->setData($data)->isValid();
    }
}
