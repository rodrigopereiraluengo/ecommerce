<?php
namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Models\Subscription;
use App\Traits\Validator\ValidatorTrait;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class SubscriptionValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function save(array &$data, $subscription = null): bool
    {
        $rules = [
            'step' => [
                'required',
                Rule::in($subscription === null ? [Subscription::STEP_USER] : [
                    Subscription::STEP_USER,
                    Subscription::STEP_PERSON,
                    Subscription::STEP_TRANSACTION
                ])
            ]
        ];

        $step = Arr::get($data, 'step');

        switch ($step) {
            case Subscription::STEP_USER:
                $rules['user'] = ['required', 'array'];
                break;
            case Subscription::STEP_PERSON:
                $rules['person'] = ['required', 'array'];
                break;
            case Subscription::STEP_TRANSACTION:
                $rules['transaction'] = ['required', 'array'];
                break;
        }

        return $this->addRules($rules)->addData($data)->isValid();
    }
}
