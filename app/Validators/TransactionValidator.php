<?php
namespace App\Validators;

use App\Contracts\ValidatorInterface;
use App\Traits\Validator\ValidatorTrait;

class TransactionValidator extends DefaultValidator implements ValidatorInterface
{
    use ValidatorTrait;

    public function insert(array &$data): bool
    {
        $rules = [
            'amount' => 'required|numeric|min:1',
            'payment_method' => 'required|in:credit_card,boleto',

            'card_holder_name' => 'required_if:payment_method,credit_card',
            'card_number' => 'required_if:payment_method,credit_card',
            'card_expiration_date' => 'required_if:payment_method,credit_card|date_format:m/y',
            'card_cvv' => 'required_if:payment_method,credit_card',

            'soft_descriptor' => 'required_if:payment_method,credit_card|max:13',

            'boleto_expiration_date' => 'required_if:payment_method,boleto',
            'boleto_instructions' => 'required_if:payment_method,boleto|max:255'
        ];

        return $this->setRules($rules)->setData($data)->isValid();
    }
}
