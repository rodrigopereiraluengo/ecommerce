<?php

namespace App\Observers;

use App\Models\Category;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class SubscriptionObserver
{
    public function created(Subscription $subscription)
    {
        $master = User::query()->where('email', env('USER_MST_EMAIL'))->first();

        $migrations = [
            '2020_08_25_130000_create_users_table.php',
            '2020_08_25_130001_create_password_resets_table.php',
            '2020_08_25_130002_create_people_table.php',
            '2020_08_27_111937_create_addresses_table.php',
            '2020_08_27_112443_create_address_person_table.php',
            '2020_09_02_103832_add_person_id_on_users_table.php',
            'subscription/2021_01_15_144727_create_directories_table.php',
            'subscription/2021_04_02_092422_create_sessions_table.php',
            'subscription/2021_04_02_110937_create_sessions_after_tigger.php',
        ];

        if ($master  === null) {
            $migrations = array_merge($migrations, [
                'subscription/2021_01_15_141755_create_categories_table.php',
                'subscription/2021_01_15_142640_create_products_table.php',
                'subscription/2021_01_15_144728_create_files_table.php',
                'subscription/2021_01_15_161533_create_file_product_table.php',
                'subscription/2021_03_20_121809_create_categories_before_trigger.php',
                'subscription/2021_03_20_122404_create_categories_after_trigger.php',
                'subscription/2021_03_20_163754_create_files_before_trigger.php',
                'subscription/2021_03_20_163805_create_files_after_trigger.php',
            ]);
        }

        $schema = 'subscription_' . $subscription->id;

        Config::set("database.connections.$schema", [
            'driver'   => env('DB_CONNECTION'),
            'host'     => env('DB_HOST'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => $schema,
        ]);

        foreach ($migrations as $migration) {
            Artisan::call('migrate', [
                '--database' => $schema,
                '--path' => "database/migrations/$migration"
            ]);
        }


        if ($master) {
            $schema_from = "subscription_{$master->subscription->id}";
            DB::unprepared("create table $schema.categories as (select * from $schema_from.categories) with data;");
            DB::unprepared("create table $schema.products as (select * from $schema_from.products) with data;");
            DB::unprepared("create table $schema.files as (select * from $schema_from.files) with data;");
            DB::unprepared("create table $schema.file_product as (select * from $schema_from.file_product) with data;");

            DB::unprepared("update $schema.categories set status = null");

            $migrations = [
                'subscription/2021_01_15_141755_alter_categories_table.php',
                'subscription/2021_01_15_142640_alter_products_table.php',
                'subscription/2021_01_15_144728_alter_files_table.php',
                'subscription/2021_01_15_161533_alter_file_product_table.php',
                'subscription/2021_03_20_121809_create_categories_before_trigger.php',
                'subscription/2021_03_20_122404_create_categories_after_trigger.php',
                'subscription/2021_03_20_163754_create_files_before_trigger.php',
                'subscription/2021_03_20_163805_create_files_after_trigger.php',
            ];
            foreach ($migrations as $migration) {
                Artisan::call('migrate', [
                    '--database' => $schema,
                    '--path' => "database/migrations/$migration"
                ]);
            }
        }
    }
}
