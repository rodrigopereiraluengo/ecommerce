<?php

namespace App\Observers;

use App\Models\Plan;
use PagarMe\Client;

class PlanObserver
{
    private $pagarme;

    public function __construct(Client $pagarme)
    {
        $this->pagarme = $pagarme;
    }

    public function creating(Plan $plan)
    {
        if (!$plan->days) {
            $plan->setAttribute('days', 30);
        }

        $created = $this->pagarme->plans()->create([
            'name' => $plan->name,
            'amount' => number_format($plan->amount, 2, '',  ''),
            'days' => $plan->days,
            'trial_days' => $plan->trial_days,
            'invoice_reminder' => $plan->invoice_reminder
        ]);

        $plan->setAttribute('id', $created->id);
    }

    public function updated(Plan $plan)
    {
        $this->pagarme->plans()->update([
            'id' => $plan->id,
            'name' => $plan->name,
            'trial_days' => $plan->trial_days,
            'invoice_reminder' => $plan->invoice_reminder
        ]);
    }

    public function deleted(Plan $plan)
    {
    }
}
