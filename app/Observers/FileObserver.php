<?php

namespace App\Observers;

use App\Models\Subscription\File;
use App\Models\Subscription;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class FileObserver
{
    public function __construct(private Subscription $subscription)
    {
    }

    public function saving(File $file)
    {
        $path = env('FILE_PATH') . '/' . $this->subscription->id;
        if ($file->status) {
            $path = "public/$path";
        }

        if ($file->file) {
            if (!file_exists($file->file['uri'])) {
                throw new FileNotFoundException($file->file);
            }

            $path = $path . '/' . $file->file['filename'];
            $file->setAttribute('extension', $file->file['extension']);
            $file->setAttribute('mime', $file->file['mime']);
            $file->setAttribute('size', $file->file['size']);

            if ($file->id) {
                Storage::delete($file->getOriginal('path'));
            }

            Storage::put($path, file_get_contents($file->file['uri']));
        } else {
            $path = $path . '/' . pathinfo($file->path)['basename'];
        }
        $file->setAttribute('path', $path);

        $url = null;
        if ($file->status) {
            $url = str_replace('public', 'storage', $file->path);
        }
        $file->setAttribute('url', $url);

        if ($file->id && !$file->file && $file->getOriginal('status') !== $file->status) {
            Storage::move($file->getOriginal('path'), $file->path);
            if (is_array($file->thumbnails)) {
                $new_dirname = pathinfo($file->path)['dirname'];
                $thumbnails = $file->thumbnails;
                foreach ($thumbnails as &$thumbnail) {
                    $pathinfo_thumbnail = pathinfo($thumbnail['path']);
                    $new_path = "$new_dirname/{$pathinfo_thumbnail['basename']}";
                    if (Storage::exists($thumbnail['path'])) {
                        Storage::move($thumbnail['path'], $new_path);
                    }
                    $thumbnail['path'] = $new_path;
                }
                $file->setAttribute('thumbnails', $thumbnails);
            }
        }

        if ($file->id && !$file->file && $file->getOriginal('path') !== $file->path) {
            Storage::delete($file->getOriginal('path'));
            $file->thumbnails = null;
        }

        if ($file->id && $file->file && is_array($file->thumbnails)) {
            foreach ($file->thumbnails as $thumbnail) {
                if (Storage::exists($thumbnail['path']) && $thumbnail['path'] !== $file->path) {
                    Storage::delete($thumbnail['path']);
                }
            }
            $file->thumbnails = null;
        }

        if ($file->id && is_array($file->getOriginal('thumbnails')) && $file->thumbnails === null) {
            foreach ($file->getOriginal('thumbnails') as $thumbnail) {
                if (Storage::exists($thumbnail['path']) && $thumbnail['path'] !== $file->path) {
                    Storage::delete($thumbnail['path']);
                }
            }
        }

        unset($file->file);
    }

    public function deleted(File $file)
    {
        Storage::delete($file->path);
        if (is_array($file->thumbnails)) {
            foreach ($file->thumbnails as $thumbnail) {
                Storage::delete($thumbnail['path']);
            }
        }
    }
}
