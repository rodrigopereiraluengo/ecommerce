<?php
namespace App\Domains;

use App\Contracts\DomainInterface;
use App\Exceptions\ValidatorException;
use App\Repositories\AddressRepository;
use App\Validators\AddressValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AddressDomain extends Domain implements DomainInterface
{
    public function __construct(AddressValidator $validator, AddressRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $address = null;
        if ($id !== null && ($address = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Address [$id] not found");
        }

        if ($this->validator->save($data, $address) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        if ($address === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($address, $data);
    }
}
