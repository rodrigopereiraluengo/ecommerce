<?php
namespace App\Domains;

use App\Contracts\DomainInterface;
use App\Exceptions\ValidatorException;
use App\Repositories\FormRepository;
use App\Validators\FormValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class FormDomain extends Domain implements DomainInterface
{
    public function __construct(FormValidator $validator, FormRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $form = null;
        if ($id !== null && ($form = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Form [$id] not found");
        }

        if ($this->validator->save($data, $form) === false) {
            throw new ValidatorException($this->validator->getErrors(), 'form');
        }

        if ($form === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($form, $data);
    }

    public function find($id)
    {
        return $this->repository->find($id);
    }
}
