<?php
namespace App\Domains;

use App\Contracts\DomainInterface;
use App\Exceptions\ValidatorException;
use App\Repositories\TextBoxRepository;
use App\Validators\TextBoxValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TextBoxDomain extends Domain implements DomainInterface
{
    public function __construct(TextBoxValidator $validator, TextBoxRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $text_box = null;
        if ($id !== null && ($text_box = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("TextBox [$id] not found");
        }

        if ($this->validator->save($data, $text_box) === false) {
            throw new ValidatorException($this->validator->getErrors(), 'text_box');
        }

        if ($text_box === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($text_box, $data);
    }
}
