<?php
namespace App\Domains;

use App\Contracts\DomainInterface;

class Domain implements DomainInterface
{
    public $validator;
    public $repository;

    public function save(array $data, $id = null)
    {

    }

    public function insert(array $data)
    {

    }

    public function update(array $data, $id)
    {

    }

    public function find($id)
    {

    }

    public function index(array $data)
    {

    }
}
