<?php
namespace App\Domains;

use App\Contracts\DomainInterface;
use App\Exceptions\ValidatorException;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Validation\UnauthorizedException;

class UserDomain extends Domain implements DomainInterface
{
    public function __construct(UserValidator $validator, UserRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $user = null;
        if ($id !== null && ($user = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("User [$id] not found.");
        }

        $email = Arr::get($data, 'email');
        if ($email) {
            $email_user = $this->repository->findByEmail($email);

            if ($email_user) {
                $id = $email_user->id;
            }
        }

        if ($user && $user->can('update', $user) === false) {
            throw new UnauthorizedException("E-mail unavailable");
        }

        if ($this->validator->save($data, $user) === false) {
            throw new ValidatorException($this->validator->getErrors(), 'user');
        }

        if ($id === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($user, $data);
    }
}
