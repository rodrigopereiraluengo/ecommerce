<?php
namespace App\Domains;

use App\Contracts\DomainInterface;
use App\Exceptions\ValidatorException;
use App\Models\Subscription;
use App\Repositories\SubscriptionRepository;
use App\Validators\SubscriptionValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SubscriptionDomain extends Domain implements DomainInterface
{
    public function __construct(SubscriptionValidator $validator, SubscriptionRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $subscription = null;
        if ($id !== null && ($subscription = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Subscription [$id] not found");
        }

        $step = &$data['step'];
        if ($step === null) {
            $step = Subscription::STEP_USER;
        }

        if ($this->validator->save($data, $subscription) === false) {
            throw new ValidatorException($this->validator->getErrors());
        }

        switch ($step) {
            case Subscription::STEP_USER:
                $data['step'] = Subscription::STEP_PERSON;
                break;
            case Subscription::STEP_PERSON:
                $data['step'] = Subscription::STEP_TRANSACTION;
                break;
        }

        if ($subscription === null) {
            $subscription = $this->repository->insert($data);
        }

        return $this->repository->update($subscription, $data);
    }
}
