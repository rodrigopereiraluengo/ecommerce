<?php
namespace App\Domains;

use App\Contracts\DomainInterface;
use App\Exceptions\ValidatorException;
use App\Repositories\TransactionRepository;
use App\Validators\TransactionValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TransactionDomain extends Domain implements DomainInterface
{
    public function __construct(TransactionValidator $validator, TransactionRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function insert(array $data)
    {
        if ($this->validator->insert($data) === false) {
            throw new ValidatorException($this->validator->getErrors(), 'transaction');
        }
    }
}
