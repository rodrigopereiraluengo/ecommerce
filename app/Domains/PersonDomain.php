<?php
namespace App\Domains;

use App\Contracts\DomainInterface;
use App\Exceptions\ValidatorException;
use App\Repositories\PersonRepository;
use App\Validators\PersonValidator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PersonDomain extends Domain implements DomainInterface
{
    public function __construct(PersonValidator $validator, PersonRepository $repository)
    {
        $this->validator = $validator;
        $this->repository = $repository;
    }

    public function save(array $data, $id = null)
    {
        $person = null;
        if ($id !== null && ($person = $this->repository->find($id)) === null) {
            throw new ModelNotFoundException("Person [$id] not found");
        }

        if ($this->validator->save($data, $person) === false) {
            throw new ValidatorException($this->validator->getErrors(), 'person');
        }

        if ($person === null) {
            return $this->repository->insert($data);
        }

        return $this->repository->update($person, $data);
    }
}
