<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\Subscription\Product;
use Illuminate\Support\Arr;

class ProductRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Product::class;

    public function insert(array $data)
    {
        $product = new Product($data);
        $product->save();
        return $product;
    }

    public function find($id)
    {
        return Product::defaultSelect()
            ->where('products.id', $id)
            ->first();
    }

    public function index(array $data)
    {
        $q = &$data['q'];
        $category_id = &$data['category_id'];
        $page = &$data['page'];
        $query = Product::defaultSelect();

        if (empty($q) === false) {
            $query->where(function ($query) use ($q) {
                $query
                    ->where('products.name', 'ilike', "%$q%")
                    ->orWhere('products.description', 'ilike', "%$q%");
            });
        }

        if ($category_id) {
            $query->where('products.category_id', $category_id);
        }

        if (array_key_exists('status', $data)) {
            if ($data['status'] === null) {
                $query->whereNull('products.status');
            } else {
                $query->where('products.status', $data['status']);
            }
        }

        $orderBy = Arr::get($data, 'orderBy', ['name' => 'asc']);
        foreach ($orderBy as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        if ($page) {
            return $query->paginate();
        }

        return $query->limit(env('QUERY_LIMIT'))->get();
    }
}
