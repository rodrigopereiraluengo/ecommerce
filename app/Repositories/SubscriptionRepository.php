<?php
namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\Subscription;

class SubscriptionRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Subscription::class;
}
