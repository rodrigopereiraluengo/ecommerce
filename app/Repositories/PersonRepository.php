<?php
namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\Person;

class PersonRepository extends Repository implements RepositoryInterface
{
  protected $entityClass = Person::class;

  public function insert(array $data)
  {
    $person = new Person($data);
    if (isset($data['user_id'])) {
        $person->setAttribute('user_id', $data['user_id']);
    }
    $person->save();
    return $person;
  }
}
