<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\Subscription\File;
use Illuminate\Support\Arr;

class FileRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = File::class;

    public function insert(array $data)
    {
        unset($data['model']);
        unset($data['model_id']);
        unset($data['type']);
        $file = new File($data);
        $file->setAttribute('extension', $data['file']['extension']);
        $file->setAttribute('mime', $data['file']['mime']);
        $file->setAttribute('size', $data['file']['size']);
        $file->setAttribute('file', $data['file']);
        $file->save();
        return $file;
    }

    public function exists($directory_id, $name, $id = null): bool
    {
        $query = File::query()
            ->where([
                'directory_id' => $directory_id,
                'name' => $name,
            ]);
        if ($id) {
            $query->where('id', '<>', $id);
        }

        return $query->exists();
    }

    public function update($file, array $data)
    {
        $file->fill($data);
        //if (isset($data['url'])) $file->setAttribute('url', $data['url']);
        //if (isset($data['path'])) $file->setAttribute('path', $data['path']);
        if (isset($data['extension'])) $file->setAttribute('extension', $data['extension']);
        if (isset($data['mime'])) $file->setAttribute('mime', $data['mime']);
        if (isset($data['size'])) $file->setAttribute('size', $data['size']);
        if (isset($data['file'])) $file->setAttribute('file', $data['file']);
        $file->save();
        return $file;
    }

    public function find($id)
    {
        return File::query()
            ->defaultSelect()
            ->first();
    }

    public function index(array $data)
    {
        $q = &$data['q'];
        $directory_id = &$data['directory_id'];
        $extension = &$data['extension'];
        $mime = &$data['mime'];
        $page = &$data['page'];
        $query = File::defaultSelect();

        if (empty($q) === false) {
            $query->where(function ($query) use ($q) {
                $query
                    ->where('files.name', 'ilike', "%$q%")
                    ->orWhere('files.extension', 'ilike', "%$q%");
            });
        }

        if ($directory_id) {
            $query->where('files.directory_id', $directory_id);
        }

        if (array_key_exists('status', $data)) {
            if ($data['status'] === null) {
                $query->whereNull('files.status');
            } else {
                $query->where('files.status', $data['status']);
            }
        }

        if (empty($extension) === false) {
            $query->whereIn('files.extension', $extension);
        }

        if (empty($mime) === false) {
            $query->whereIn('files.mime', $mime);
        }

        $orderBy = Arr::get($data, 'orderBy', ['name' => 'asc']);
        foreach ($orderBy as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        if ($page) {
            return $query->paginate();
        }

        return $query->limit(env('QUERY_LIMIT'))->get();
    }
}
