<?php
namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\Transaction;

class TransactionRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Transaction::class;
}
