<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\Subscription\Directory;
use Illuminate\Support\Arr;

class DirectoryRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Directory::class;

    public function find($id)
    {
        return Directory::defaultSelect()
            ->where('directories.id', $id)
            ->first();
    }

    public function index(array $data)
    {
        $q                  = &$data['q'];
        $total_directories  = &$data['total_directories'];
        $total_files        = &$data['total_files'];
        $page               = &$data['page'];
        $query              = Directory::defaultSelect();

        if (empty($q) === false) {
            $query->where(function ($query) use ($q) {
                $query
                    ->where('directories.name', 'ilike', "%$q%")
                    ->orWhere('directories.description', 'ilike', "%$q%");
            });
        }

        if (array_key_exists('directory_id', $data)) {
            $query->where('directories.directory_id', $data['directory_id']);
        }

        if (array_key_exists('status', $data)) {
            $query->where('directories.status', $data['status']);
        }

        if ($total_directories !== null) {
            $query->whereRaw('exists(
                select 1
                from directories as children
                where children.directory_id = directories.id
                having count(1) >= ?
              )', [$total_directories]);
        }

        if ($total_files !== null) {
            $query->whereRaw('exists(
                select 1
                from files
                where files.directory_id = directories.id
                having count(1) >= ?
              )', [$total_files]);
        }

        $orderBy = Arr::get($data, 'orderBy', ['name' => 'asc']);
        foreach ($orderBy as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        if ($page) {
            return $query->paginate();
        }

        return $query->limit(env('QUERY_LIMIT'))->get();
    }
}
