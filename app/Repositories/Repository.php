<?php
namespace App\Repositories;

use App\Contracts\RepositoryInterface;

class Repository implements RepositoryInterface
{
    protected $entityClass;

    public function find($id)
    {
        return app($this->entityClass)->find($id);
    }

    public function findOrFail($id)
    {
        return app($this->entityClass)->findOrFail($id);
    }

    public function findAll()
    {
        return app($this->entityClass)->all();
    }

    public function insert(array $data)
    {
        $model = app($this->entityClass);
        $model->fill($data)->save();
        return $model;
    }

    public function update($entity, array $data)
    {
        $entity->fill($data)->save();
        return $entity;
    }

    public function delete($entity)
    {
        return $entity->delete();
    }
}
