<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\Subscription\Session;
use Illuminate\Support\Facades\Auth;

class SessionRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Session::class;

    public function insert(array $data)
    {
        $session = new Session($data);
        $session->type = $data['type'];
        $session->user()->associate(Auth::user());
        $session->save();
        return $session;
    }

    public function index(array $data)
    {
        $query = Session::query()
                            ->where('user_id', Auth::user()->id)
                            ->orderBy('id');

        return $query->get();
    }
}
