<?php

namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\Subscription\Category;
use Illuminate\Support\Arr;

class CategoryRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = Category::class;

    public function find($id)
    {
        return Category::defaultSelect()
            ->where('categories.id', $id)
            ->first();
    }

    public function index(array $data)
    {
        $q                  = &$data['q'];
        $total_categories   = &$data['total_categories'];
        $total_products     = &$data['total_products'];
        $page               = &$data['page'];
        $query              = Category::defaultSelect();

        if (!empty($q)) {
            $query->where(function ($query) use ($q) {
                $query
                    ->where('categories.name', 'ilike', "%$q%")
                    ->orWhere('categories.description', 'ilike', "%$q%");
            });
        }

        if (array_key_exists('parent_category_id', $data)) {
            $query->where('categories.parent_category_id', $data['parent_category_id']);
        }

        if (array_key_exists('status', $data)) {
            $query->where('categories.status', $data['status']);
        }

        if ($total_categories) {
            $query->whereRaw('exists(
                select 1
                from categories as children
                where children.category_id = categories.id
                having count(1) >= ?
              )', [$total_categories]);
        }

        if ($total_products) {
            $query->whereRaw('exists(
                select 1
                from products
                where products.category_id = categories.id
                having count(1) >= ?
              )', [$total_products]);
        }

        $orderBy = Arr::get($data, 'orderBy', ['position' => 'asc']);
        foreach ($orderBy as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        if ($page) {
            return $query->paginate();
        }

        return $query->limit(env('QUERY_LIMIT'))->get();
    }

}
