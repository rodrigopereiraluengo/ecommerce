<?php
namespace App\Repositories;

use App\Contracts\RepositoryInterface;
use App\Models\User;

class UserRepository extends Repository implements RepositoryInterface
{
    protected $entityClass = User::class;

    public function insert(array $data)
    {
        $user = new User($data);
        $user->setAttribute('subscription_id', $data['subscription_id']);
        $user->save();
        return $user;
    }

    public function findByEmail($email)
    {
        return User::query()
            ->where('email', $email)
            ->first();
    }
}
