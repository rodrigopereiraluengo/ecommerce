<?php
namespace App\Policies;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserPolicy
{
    public function update(User $user, User $model)
    {
        if (Auth::check() && Auth::user()->id === $model->id) {
            return true;
        }

        if (Auth::check() === false && !$model->status && $user->id === $model->id) {
            return true;
        }

        return false;
    }
}
