<?php
namespace App\Models;

use App\Traits\Model\SubscriptionSchemaTrait;

class Person extends BaseModel
{
    protected $table = 'public.people';

    protected $fillable = [
        'name'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addresses()
    {
        return $this->belongsToMany(Address::class);
    }
}
