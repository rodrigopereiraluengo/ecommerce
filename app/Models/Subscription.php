<?php
namespace App\Models;

class Subscription extends BaseModel
{
    const STEP_USER = 0;
    const STEP_PERSON = 1;
    const STEP_TRANSACTION = 2;
    const STEP_RESUME = 3;

    protected $table = 'public.subscriptions';

    protected $fillable = [
        'step'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
