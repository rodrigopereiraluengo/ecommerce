<?php

namespace App\Models;

use App\Traits\Model\SubscriptionSchemaTrait;

class Address extends BaseModel
{
    protected $table = 'public.addresses';

    protected $fillable = [
        'country_id',
        'address_zipcode',
        'address_street',
        'address_street_number',
        'address_complementary',
        'address_neighborhood',
        'state_id',
        'address_state',
        'city_id',
        'address_city',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
