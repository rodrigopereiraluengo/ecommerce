<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

abstract class BaseModel extends Model
{
    protected static function booted()
    {
        static::updating(function ($model) {
            $class = get_class($model);
            $user_id = Auth::id() ?? null;
            //$current = self::defaultSelect()->where('id', $model->id)->first();
            // var_dump($model);

        });
    }
}
