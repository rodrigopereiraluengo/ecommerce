<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'public.currencies';

    protected $fillable = [
        "name",
        "symbol",
        "iso"
    ];

    protected $casts = [
        'id' => 'string'
    ];

    public $timestamps = false;
}
