<?php
namespace App\Models;

class Transaction extends BaseModel
{
    const PAYMENT_METHOD_CREDIT_CARD = 'credit_card';
    const PAYMENT_METHOD_BOLETO = 'boleto';

    const STATUS_PROCESSING = 'processing';
    const STATUS_AUTHORIZED = 'authorized';
    const STATUS_PAID = 'paid';
    const STATUS_REFUNDED = 'refunded';
    const STATUS_WAITING_PAYMENT = 'waiting_payment';
    const STATUS_PENDING_REFUND = 'pending_refund';
    const STATUS_REFUSED = 'refused';

    protected $table = 'public.transactions';

    protected $fillable = [
        'id',
        'amount',
        'payment_method',
        'status',
        'card_id',
        'boleto_url',
        'boleto_barcode',
        'boleto_expiration_date',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'boleto_expiration_date'
    ];

    protected $casts = [
        'amount' => 'decimal'
    ];

    public function subscription()
    {
        return $this->hasOne(Subscription::class);
    }
}
