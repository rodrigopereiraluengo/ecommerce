<?php
namespace App\Models;

class Plan extends BaseModel
{
    protected $table = 'public.plans';

    protected $fillable = [
        'name',
        'status',
        'amount',
        'days',
        'trial_days',
        'payment_methods',
        'color',
        'metadata'
    ];

    protected $casts = [
        'status' => 'boolean',
        'amount' => 'decimal:2',
        'metadata' => 'array'
    ];
}
