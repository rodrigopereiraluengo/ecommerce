<?php

namespace App\Models\Subscription;

use App\Models\BaseModel;
use App\Traits\Model\SubscriptionSchemaTrait;
use Illuminate\Support\Facades\DB;

class Directory extends BaseModel
{
    use SubscriptionSchemaTrait;

    protected $fillable = [
        'directory_id',
        'status',
        'name',
        'description',
        'metadata',
    ];

    protected $casts = [
        'status' => 'boolean',
        'metadata' => 'array'
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function directory()
    {
        return $this->belongsTo(Directory::class);
    }

    public function directories()
    {
        return $this->hasMany(Directory::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function scopeDefaultSelect($query)
    {
        return $query
            ->leftJoin('directories as directory', 'directories.directory_id', '=', 'directory.id')
            ->select([
                'directories.id',
                'directories.directory_id',
                'directory.name as directory_name',
                'directories.status',
                DB::raw("case
                  when directories.status is null then 'Stand By'
                  when directories.status = true then 'Active'
                  else 'Inactive' end
                  as status_name"),
                'directories.name',
                'directories.description',
                'directories.metadata',
                DB::raw("(select count(1) from directories as children where children.directory_id = directories.id) as total_directories"),
                DB::raw("(select count(1) from files as files where files.directory_id = directories.id) as total_files"),
                'directories.created_at',
                'directories.updated_at',
            ]);
    }
}
