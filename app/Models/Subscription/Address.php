<?php

namespace App\Models\Subscription;

use App\Models\Address as AddressModel;
use App\Traits\Model\SubscriptionSchemaTrait;

class Address extends AddressModel
{
    use SubscriptionSchemaTrait;
}
