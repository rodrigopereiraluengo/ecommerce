<?php

namespace App\Models\Subscription;

use App\Models\BaseModel;
use App\Traits\Model\SubscriptionSchemaTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;

class File extends BaseModel
{
    use SubscriptionSchemaTrait;

    public function __get($key)
    {
        if ($key === 'is_image') {
            return str_starts_with($this->mime, 'image') && in_array($this->extension, ['jpg', 'jpeg', 'png']);
        }

        // Thumbnail
        if (
            preg_match('/thumbnail_[0-9]{2,4}_[0-9]{2,4}/', $key)
            && isset($this->path)
            && $this->is_image
            && Storage::exists($this->path)
        ) {

            $key = explode('_', $key);
            $width = $key[1];
            $height = $key[2];
            $size = "{$width}_{$height}";

            $pathinfo = pathinfo($this->path);
            $path = "{$pathinfo['dirname']}/{$size}_{$pathinfo['basename']}";
            if (isset($this->thumbnails[$size])) {
                return $this->thumbnails[$size];
            }

            $fullpath = storage_path("app/{$path}");
            $imagine = new Imagine();
            $image = $imagine->open(storage_path("app/{$this->path}"));
            if ($width < $image->getSize()->getWidth() && $height < $image->getSize()->getHeight()) {
                $box = new Box($width, $height);
                $image->thumbnail($box, ImageInterface::THUMBNAIL_INSET)->save($fullpath);
                unset($box);
                unset($image);
                unset($imagine);
            } else {
                $path = $this->path;
                $fullpath = storage_path("app/{$path}");
            }

            $thumbnails = $this->thumbnails;
            if (!is_array($thumbnails)) {
                $thumbnails = [];
            }

            $thumbnails[$size] = [
                'path' => $path,
                'size' => filesize($fullpath),
                'url' => null
            ];
            if ($this->url) {
                $pathinfo = pathinfo($this->url);
                $thumbnail_pathinfo = pathinfo($path);
                $thumbnails[$size]['url'] = "{$pathinfo['dirname']}/{$thumbnail_pathinfo['basename']}";
            }

            $this->setAttribute('thumbnails', $thumbnails)->save();

            return $thumbnails[$size];
        }

        // Rotate
        if (
            preg_match('/rotate_[0-9]{2,3}(_save)?$/', $key)
            && isset($this->path)
            && $this->is_image
            && Storage::exists($this->path)
        ) {
            $key        = explode('_', $key);
            $angular    = $key[1];
            $save       = &$key[2];

            if ($angular < 10) {
                $angular = 10;
            }

            if ($angular > 360) {
                $angular = 360;
            }

            $imagine = new Imagine();
            $image = $imagine->open(storage_path("app/{$this->path}"));
            $image->rotate($angular);

            if ($save) {
                $path = $this->path;
                $pathinfo = pathinfo($path);
                $path = $pathinfo['dirname'] . '/' . Str::uuid() . '.' . $this->extension;
                $fullpath = storage_path('app/' . $path);
                $image->save($fullpath);
                unset($image);
                unset($imagine);
                $this
                    ->setAttribute('path', $path)
                    ->setAttribute('size', filesize($fullpath))
                    ->save();
                return $this;
            }
            $this->binary = $image->get($this->extension);
            unset($image);
            unset($imagine);
            return $this;
        }

        return parent::__get($key);
    }

    protected $fillable = [
        'directory_id',
        'previous_file_id',
        'status',
        'name',
        'metadata'
    ];

    protected $casts = [
        'status' => 'boolean',
        'size' => 'integer',
        'metadata' => 'array',
        'thumbnails' => 'array'
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function directory()
    {
        return $this->belongsTo(Directory::class);
    }

    public function previous()
    {
        return $this->belongsTo(File::class, 'previous_file_id');
    }

    public function scopeDefaultSelect($query)
    {
        return $query
            ->leftJoin('directories', 'files.directory_id', '=', 'directories.id')
            ->select([
                'files.id',
                'files.subscription_id',
                'files.directory_id',
                'directories.name as directory_name',
                'files.status',
                DB::raw("case
                  when files.status is null then 'Stand By'
                  when files.status = true then 'Active'
                  else 'Inactive' end
                  as status_name"),
                'files.name',
                'files.path',
                'files.url',
                'files.extension',
                'files.mime',
                'files.size',
                'files.metadata',
                'files.thumbnails',
                'files.created_at',
                'files.updated_at',
            ]);
    }
}
