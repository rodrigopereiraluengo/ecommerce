<?php

namespace App\Models\Subscription;

use App\Models\User;
use App\Models\BaseModel;
use App\Traits\Model\SubscriptionSchemaTrait;

class Session extends BaseModel
{
    use SubscriptionSchemaTrait;

    const STORE     = ['type' => 'STORE'];
    const PRODUCT   = ['type' => 'PRODUCT'];
    const COSTUMER  = ['type' => 'COSTUMER'];
    const SALE      = ['type' => 'SALE'];
    const PROFILE   = ['type' => 'PROFILE'];
    const SETTING   = ['type' => 'SETTING'];
    const USER      = ['type' => 'USER'];
    const SEARCH    = ['type' => 'SEARCH'];

    protected $fillable = [
        'status',
        'data'
    ];

    protected $casts = [
        'status' => 'boolean',
        'data' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
