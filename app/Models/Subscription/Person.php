<?php
namespace App\Models\Subscription;

use App\Models\Person as PersonModel;
use App\Models\Subscription;
use App\Traits\Model\SubscriptionSchemaTrait;

class Person extends PersonModel
{
    use SubscriptionSchemaTrait;
}
