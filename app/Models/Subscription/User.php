<?php
namespace App\Models\Subscription;

use App\Models\User as UserModel;
use App\Models\Subscription;
use App\Traits\Model\SubscriptionSchemaTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends UserModel
{
    use SubscriptionSchemaTrait;
}
