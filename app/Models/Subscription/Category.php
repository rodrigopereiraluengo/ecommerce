<?php

namespace App\Models\Subscription;

use App\Models\BaseModel;
use App\Traits\Model\SubscriptionSchemaTrait;
use Illuminate\Support\Facades\DB;

class Category extends BaseModel
{
    use SubscriptionSchemaTrait;

    protected $fillable = [
        'parent_category_id',
        'previous_category_id',
        'status',
        'name',
        'description',
        'position',
        'metadata'
    ];

    protected $casts = [
        'status' => 'boolean',
        'position' => 'integer',
        'metadata' => 'array'
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_category_id');
    }

    public function previous()
    {
        return $this->belongsTo(Category::class, 'previous_category_id');
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'parent_category_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function scopeDefaultSelect($query)
    {
        $schema = $this->getSchema();
        return $query
            ->leftJoin("categories as parent_category", "categories.parent_category_id", '=', 'parent_category.id')
            ->select([
                "categories.id",
                "categories.parent_category_id",
                "parent_category.name as parent_category_name",
                "categories.status",
                DB::raw("case
                  when categories.status is null then 'Stand By'
                  when categories.status = true then 'Active'
                  else 'Inactive' end
                  as status_name"),
                "categories.name",
                "categories.description",
                "categories.position",
                "categories.metadata",
                DB::raw("(select count(1) from categories as children where children.parent_category_id = categories.id) as total_categories"),
                DB::raw("(select count(1) from products as products where products.category_id = categories.id) as total_products"),
                "categories.created_at",
                "categories.updated_at",
            ])
            ->orderBy('position');
    }

    public function scopeWithCategories($query)
    {
        return $query->with(['categories' => fn($query) => $query->orderBy('position')->withCategories()]);
    }
}
