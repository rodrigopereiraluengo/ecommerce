<?php

namespace App\Models\Subscription;

use App\Models\BaseModel;
use App\Traits\Model\SubscriptionSchemaTrait;
use Illuminate\Support\Facades\DB;

class Product extends BaseModel
{
    use SubscriptionSchemaTrait;

    protected $fillable = [
        'category_id',
        'status',
        'name',
        'description',
        'quantity',
        'price',
        'metadata'
    ];

    protected $casts = [
        'status' => 'boolean',
        'quantity' => 'float',
        'price' => 'float',
        'metadata' => 'array'
    ];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function files()
    {
        return $this->belongsToMany(File::class);
    }

    public function scopeDefaultSelect($query)
    {
        return $query
            ->images()
            ->leftJoin('categories as category', 'products.category_id', '=', 'category.id')
            ->select([
                'products.id',
                'products.category_id',
                'products.name as category_name',
                'products.status',
                DB::raw("case
                  when products.status is null then 'Stand By'
                  when products.status = true then 'Active'
                  else 'Inactive' end
                  as status_name"),
                'products.name',
                'products.description',
                'products.quantity',
                'products.price',
                'products.metadata',
                'products.created_at',
                'products.updated_at'
            ]);
    }

    public function scopeImages($query)
    {
        return $query->with(['files' => function ($query) {
            $query->where('mime',  'ilike', 'image%');
        }]);
    }
}
