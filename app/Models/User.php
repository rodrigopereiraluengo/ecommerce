<?php
namespace App\Models;

use App\Traits\Model\SubscriptionSchemaTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    protected $table = 'public.users';

    use Authenticatable, Authorizable;

    const ROLE_MASTER = 'MST';

    const ROLE_ADMIN = 'ADM';

    const ROLE_USER = 'USR';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'metadata', 'password'
    ];

    protected $casts = [
        'status' => 'boolean',
        'metadata' => 'array'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getNameAttribute()
    {
        return $this->person->name;
    }

    public function getShortNameAttribute()
    {
        $name = explode(' ', $this->person->name);
        $first_name = array_shift($name);
        $last_name = array_pop($name);
        return "$first_name $last_name";
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = app('hash')->make($value);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
