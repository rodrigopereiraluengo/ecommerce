<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    protected $table = 'public.themes';

    protected $fillable = [
        'name',
        'status',
        'metadata',
        'published',
    ];

    protected $casts = [
        'status' => 'boolean',
        'metadata' => 'json',
        'published' => 'json',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
