<?php

namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function post(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');

        if ($token = auth()->attempt($credentials)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], HttpStatus::UNAUTHORIZED);
    }

    public function get()
    {
        return auth()->user();
    }

    public function delete()
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        $expires_at = auth()->factory()->getTTL() * 60;
        $datetime_expires_at = Carbon::now()->addSeconds($expires_at);
        $user = Auth::user();

        return response()->json([
            'token' => [
                'token_type' => 'Bearer',
                'access_token' => $token,
                'expires_at' => $expires_at,
                'datetime_expires_at' => $datetime_expires_at->format('Y-m-d H:i:s'),
                'timezone' => $datetime_expires_at->getTimezone()
            ],
            'user' => [
                'id' => $user->id,
                'email' => $user->email,
                'name' => $user->short_name
            ]
        ]);
    }
}
