<?php

namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Enums\Response;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct(private ProductService $service) { }

    public function post(Request $request)
    {
        $product = $this->service->save($request->all());
        return Response::created($product);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return Response::noContent();
    }

    public function patch(Request $request, $id)
    {
        $this->service->patch($request->all(), $id);
        return Response::noContent();
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return Response::noContent();
    }

    public function get($id)
    {
        return $this->service->find($id);
    }

    public function index(Request $request)
    {
        return $this->service->index($request->all());
    }
}
