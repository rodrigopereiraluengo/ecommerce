<?php

namespace App\Http\Controllers;

use App\Enums\Response;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(private UserService $service)
    {
    }

    public function post(Request $request)
    {
        $user = $this->service->save($request->all());
        return Response::created($model);
    }

    public function patch(Request $request, $id)
    {
        $user = $this->service->patch($request->all());
        return Response::noContent();
    }

}
