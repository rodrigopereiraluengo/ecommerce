<?php

namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Enums\Response;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CategoryController extends Controller
{
    private $service;

    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        return Response::created($this->service->save($request->all()));
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return Response::noContent();
    }

    public function patch(Request $request, $id)
    {
        $this->service->patch($request->all(), $id);
        return Response::noContent();
    }

    public function index(Request $request)
    {
        return $this->service->index($request->all());
    }

    public function get($id)
    {
        return $this->service->find($id);
    }

    public function delete($id)
    {
        $this->service->delete($id);
        return Response::noContent();
    }
}
