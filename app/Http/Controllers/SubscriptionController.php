<?php
namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Services\SubscriptionService;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
  private $service;

  public function __construct(SubscriptionService $service)
  {
    $this->service = $service;
  }

  public function post(Request $request)
  {
    $subscription = $this->service->save($request->all());
    return response()->json(['id' => $subscription->id], HttpStatus::CREATED);
  }

  public function put(Request $request, $id)
  {
    $this->service->save($request->all(), $id);
  }
}
