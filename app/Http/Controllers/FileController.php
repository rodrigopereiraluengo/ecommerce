<?php

namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Enums\Response;
use App\Services\FileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    private $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    public function post(Request $request)
    {
        $file = $this->service->save($request->all());
        return Response::created($file);
    }

    public function put(Request $request, $id)
    {
        $this->service->save($request->all(), $id);
        return Response::noContent();
    }

    public function patch(Request $request, $id)
    {
        $this->service->patch($request->all(), $id);
        return Response::noContent();
    }

    public function get($id)
    {
        return $this->service->find($id);
    }

    public function index(Request $request)
    {
        return $this->service->index($request->all());
    }

    public function path($id, $path)
    {
        $file = $this->service->path($id, $path);
        return Storage::response($file->path);
    }

    public function thumbnail($id, $width, $height, $path)
    {
        $thumbnail = $this->service->thumbnail($id, $width, $height, $path);
        return Storage::response($thumbnail['path']);
    }

    public function rotate($id, $angular, $path)
    {
        $file = $this->service->rotate($id, $angular, $path);
        return response($file->binary)
            ->header("Content-Type", $file->mime)
            ->header("Content-length", strlen($file->binary));
    }

    public function rotateSave($id, $angular, $path)
    {
        $this->service->rotateSave($id, $angular, $path);
        return Response::noContent();
    }
}
