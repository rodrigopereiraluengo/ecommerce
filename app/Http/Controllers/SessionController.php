<?php

namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Enums\Response;
use App\Services\SessionService;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function __construct(private SessionService $service)
    {
    }

    public function post(Request $request)
    {
        return response()->json($this->service->insert($request->all()), HttpStatus::CREATED);
    }

    public function patch(Request $request, $id)
    {
        return $this->service->patch($request->all(), $id);
    }

    public function index(Request $request)
    {
        return $this->service->index($request->all());
    }

}
