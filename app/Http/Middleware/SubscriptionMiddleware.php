<?php
namespace App\Http\Middleware;

use App\Models\Subscription;
use Closure;

class SubscriptionMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $data = $request->all();
        $subscription = app(Subscription::class);
        $data['subscription_id'] = $subscription->id ?? null;
        $request->replace($data);
        return $next($request);
    }
}
