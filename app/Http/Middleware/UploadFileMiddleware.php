<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\File;
use SplFileInfo;
use Mimey\MimeTypes;

class UploadFileMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $data       = $request->all();
        $model      = $request?->model;
        $model_id   = $request?->model_id;
        $type       = $request?->type;
        if ($model && $model_id) {
            $data['model'] = $model;
            $data['model_id'] = $model_id;
            $data['type'] = $type;
        }
        $file = base64_decode($request?->file);
        if ($file) {
            $tmpfile            = tmpfile();
            fwrite($tmpfile, $file);
            $uri                = stream_get_meta_data($tmpfile)['uri'];
            $mime               = mime_content_type($uri);
            $mimes              = new MimeTypes;
            $extension          = $mimes->getExtension($mime);
            $name               = (string) Str::uuid();
            if (!isset($data['name'])) {
                $data['name']   = $name;
            }
            $filename           = $name . '.' . $extension;
            $size               = filesize($uri);

            $data['file']       = [
                'uri' => $uri,
                'filename' => $filename,
                'mime' => $mime,
                'extension' => $extension,
                'size' => $size,
            ];
        } else {
            unset($data['file']);
        }

        $request->replace($data);
        $response = $next($request);

        if (isset($tmpfile)) {
            fclose($tmpfile);
        }

        return $response;
    }
}
