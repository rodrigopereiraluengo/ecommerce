<?php

namespace App\Http\Middleware;

use App\Models\Subscription;
use Closure;
use Illuminate\Support\Facades\Config;

class DatabaseMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $subscription = app(Subscription::class);
        $schema = 'subscription_' . $subscription->id;
        Config::set('database.connections.' . $schema, [
            'driver'   => env('DB_CONNECTION'),
            'host'     => env('DB_HOST'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset'  => 'utf8',
            'prefix'   => '',
            'schema'   => $schema,
        ]);
        Config::set('database.default', $schema);

        return $next($request);
    }
}
