<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class ToolRouteMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $data = $request->all();
        $data['type'] = explode('/', $request->decodedPath())[0];
        $request->replace($data);
        return $next($request);
    }
}
