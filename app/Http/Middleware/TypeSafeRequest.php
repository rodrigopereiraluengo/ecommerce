<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Arr;

class TypeSafeRequest
{
    public function handle($request, Closure $next, $guard = null)
    {
        $data = $request->all();
        self::parse($data);
        $request->replace($data);
        return $next($request);
    }

    public static function parse(&$data)
    {
        foreach ($data as $key => &$value) {

            if (Arr::exists(['password', 'password_confirmation'], $key)) {
                continue;
            }

            if (is_string($value)) {
                $value = trim($value);

                if (empty($value) && is_numeric($value) === false) {
                    $value = null;
                }

                if (is_string($value) && strtolower($value) === 'true') {
                    $value = true;
                }

                if (is_string($value) && strtolower($value) === 'false') {
                    $value = false;
                }

                if (is_string($value) && strtolower($value) === 'null') {
                    $value = null;
                }
            }

            if (is_array($value)) {
                self::parse($value);
            }
        }
    }
}
