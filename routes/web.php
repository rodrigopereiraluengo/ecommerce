<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Authentication
$router->post('/auth', 'AuthController@post');
$router->delete('/auth', ['uses' => 'AuthController@delete', 'middleware' => 'auth:api']);

// Subscription
$router->post('/subscription', 'SubscriptionController@post');
$router->put('/subscription/{id}', 'SubscriptionController@put');

$router->get('/city', ['middleware' => ['auth:api'], function () {
    return \App\Models\City::all();
}]);

$router->group(['middleware' => ['auth:api', 'refresh']], function () use ($router) {

    // Auth
    $router->get('/auth', 'AuthController@get');

    // User
    $router->patch('/user/{id:[0-9]+}', 'UserController@patch');

    // Session
    $router->post('/session', ['uses' => 'SessionController@post', 'middleware' => 'database']);
    $router->patch('/session/{id:[0-9]+}', ['uses' => 'SessionController@patch', 'middleware' => 'database']);
    $router->get('/session', ['uses' => 'SessionController@index', 'middleware' => 'database']);

    // Category
    $router->post('/category', ['uses' => 'CategoryController@post', 'middleware' => 'database']);
    $router->put('/category/{id:[0-9]+}', ['uses' => 'CategoryController@put', 'middleware' => 'database']);
    $router->patch('/category/{id:[0-9]+}', ['uses' => 'CategoryController@patch', 'middleware' => 'database']);
    $router->delete('/category/{id:[0-9]+}', ['uses' => 'CategoryController@delete', 'middleware' => 'database']);
    $router->get('/category/{id:[0-9]+}', ['uses' => 'CategoryController@get', 'middleware' => 'database']);
    $router->get('/category', ['uses' =>'CategoryController@index', 'middleware' => 'database']);

    // Product
    $router->post('/product', 'ProductController@post');
    $router->put('/product/{id:[0-9]+}', 'ProductController@put');
    $router->patch('/product/{id:[0-9]+}', 'ProductController@patch');
    $router->delete('/product/{id:[0-9]+}', 'ProductController@delete');
    $router->get('/product/{id:[0-9]+}', 'ProductController@get');
    $router->get('/product', 'ProductController@index');

    // Directory
    $router->post('/directory', 'DirectoryController@post');
    $router->put('/directory/{id}', 'DirectoryController@put');
    $router->patch('/directory/{id}', 'DirectoryController@patch');
    $router->delete('/directory/{id}', 'DirectoryController@delete');
    $router->get('/directory/{id}', 'DirectoryController@get');
    $router->get('/directory', 'DirectoryController@index');

    // File
    $router->post('/file', ['uses' => 'FileController@post', 'middleware' => 'upload-file']);
    $router->put('/file/{id:[0-9]+}', ['uses' => 'FileController@put', 'middleware' => 'upload-file']);
    $router->patch('/file/{id:[0-9]+}', ['uses' => 'FileController@patch', 'middleware' => 'upload-file']);
    $router->get('/file/{id:[0-9]+}', 'FileController@get');
    $router->get('/file', 'FileController@index');
    $router->get('/file/{id:[0-9]+}/rotate/{angular:[0-9]{2,3}}/{path:[files|public\\/files]+\\/+[0-9]{1,}+\\/[0-9a-fA-F]{8}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{12}+\\.[a-zA-Z0-9]{1,}}', 'FileController@rotate');
    $router->patch('/file/{id:[0-9]+}/rotate/{angular:[0-9]{2,3}}/{path:[files|public\\/files]+\\/+[0-9]{1,}+\\/[0-9a-fA-F]{8}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{12}+\\.[a-zA-Z0-9]{1,}}', 'FileController@rotateSave');
    $router->get('/file/{id:[0-9]+}/{width:[0-9]{2,4}}/{height:[0-9]{2,4}}/{path:[files|public\\/files]+\\/+[0-9]{1,}+\\/[0-9a-fA-F]{8}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{12}+\\.[a-zA-Z0-9]{1,}}', 'FileController@thumbnail');
    $router->get('/file/{id:[0-9]+}/{path:[files|public\\/files]+\\/+[0-9]{1,}+\\/[0-9a-fA-F]{8}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{4}+\\-[0-9a-fA-F]{12}+\\.[a-zA-Z0-9]{1,}}', 'FileController@path');
    $router->post('/file/{model:[A-Za-z]+}/{model_id:[0-9]+}[/{type}]', ['uses' => 'FileController@post', 'middleware' => 'upload-file']);
    $router->put('/file/{id:[0-9]+}/{model:[A-Za-z]+}/{model_id:[0-9]+}[/{type}]',  ['uses' => 'FileController@put', 'middleware' => 'upload-file']);
    $router->patch('/file/{id:[0-9]+}/{model:[A-Za-z]+}/{model_id:[0-9]+}[/{type}]',  ['uses' => 'FileController@patch', 'middleware' => 'upload-file']);
});
