const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const Crawler = require('crawler');
const colog = require('colog');

const URL = 'https://www.ultrafarma.com.br';
const PATH = '/var/www/ecommerce/storage/app/ultrafarma';
if (!fs.existsSync(PATH)) {
    fs.mkdirSync(PATH)
}

const home = new Crawler({
    maxConnections : 10,
    jQuery: true,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            const $ = res.$;
            $('#navbar-main2 > ul > li > a').each(function () {
                categories.queue(URL + $(this).attr('href'));
            })
        }
        done();
    }
});

home.queue(URL);

const categories = new Crawler({
    maxConnections : 20,
    jQuery: true,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            const $ = res.$;


            $('.product-wrapper .product-wrapper-container').each(function () {
                product.queue($($(this).find('a')[0]).attr('href'))
            });

            const $currentPage = $('.pagination.pagination-vitrine li.active');
            if ($currentPage?.next()?.find('a')?.attr('href')) {
                categories.queue(URL + $currentPage.next().find('a').attr('href'));
            }
        }
        done();
    }
});

const product = new Crawler({
    maxConnections : 20,
    jQuery: true,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            const $ = res.$;

            let category = $('.breadcrumb li a').map(function () { return $(this).text().trim(); }).get();
            category = category.slice(1, category.length -1)
            //console.log(name)
            //console.log($('.brand').text().trim())
            //console.log($('.skuReference').text().trim())
            //console.log($('.product-price-new').text().trim())
            // console.log($('.product-actions .product-price-old').text().trim())
            // console.log($($('.product-details-section p').get(0)).html().trim())

            $('script[type="text/javascript"]').each(function () {
                const content = $(this).text();
                if (content.includes('LeanEcommerce.PDP_PRODUTO_IMAGENS = ')) {
                    const product = JSON.parse(content.replace('LeanEcommerce.PDP_PRODUTO = ', '{"product": ')
                        .replace("LeanEcommerce.PDP_PRODUTO_IMAGENS = JSON.parse('", ', "images": ')
                        .replace("]}]');", "]}]}")
                        .replace("Id", '"Id"')
                        .replace("Codigo", '"Codigo"')
                        .replace("Sku", '"Sku"')
                        .replace("Tipo", '"Tipo"')
                        .replace("Estoque", '"Estoque"')
                        .replace("Preco", '"Preco"')
                        .replace("UltimaParcela", '"UltimaParcela"')
                        .replace("QuantidadeMinimaVenda", '"QuantidadeMinimaVenda"')
                        .replace("QuantidadeMaximaVenda", '"QuantidadeMaximaVenda"')
                        .replace("QuantidadeMultiplaVenda", '"QuantidadeMultiplaVenda"')
                        .replace("EhFavorito", '"EhFavorito"')
                        .replace("Permalink", '"Permalink"')
                        .replace("Parcelamento", '"Parcelamento"')
                        .replace("Indisponivel", '"Indisponivel"')
                        .replace(/Number\(\'(.*)\'\)/g, '$1')
                        .replace(/JSON.parse\(\'(.*)\'\)/g, '$1')
                        .replace(/'(.*)'/g, '"$1"'))

                    const $description = $('.product-details-section h3.tab-content-title').filter(function(){ return $(this).text() === 'Descrição' }).parent();
                    $description.children('h3').remove();
                    const data = {
                        category,
                        name: $('h1.product-name').text().trim(),
                        brand: $('.brand').text().trim(),
                        id: $('.skuReference').text().trim(),
                        images: product.images.map(image => image.Grande),
                        price_old: product.product.Preco.PrecoDe,
                        price_new: product.product.Preco.PrecoPor,
                        price_new: product.product.Preco.PrecoPor,
                        description: $description.html(),
                        href: URL.concat('/', product.product.Permalink)
                    };

                    fs.writeFile(`${PATH}/${uuidv4()}.json`, JSON.stringify(data), function (err) {
                        if (err) {
                            return colog.error(`Error on save Product File [${href}]`)
                        }
                        colog.success(`Product [${data.name}] Saved`)
                    })
                }
            })
        }
        done();
    }
});
