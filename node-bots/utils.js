

const autoscroll = async (page) => {
  await page.evaluate(async () => {
    await new Promise((resolve, reject) => {
      let totalHeight = 0
      let distance = 200
      let timer = setInterval(() => {
        let scrollHeight = document.body.scrollHeight
        window.scrollBy(0, distance)
        totalHeight += distance
        if (totalHeight >= scrollHeight) {
          clearInterval(timer)
          resolve()
        }
      }, 100)
    })
  })
}

const timeForHumans = time => {
  const _time = time.time();
  const names = {
    d: 'days',
    h: 'hours',
    m: 'minutes',
    s: 'seconds',
    ms: 'milliseconds',
  };
  const string = [];
  for (let key in _time) {
    if (_time[key]) {
      string.push(`${_time[key]} ${names[key]}`)
    }
  }
  return string.join(' ');
}


module.exports = { autoscroll, timeForHumans }
