const puppeteer = require('puppeteer');
const queryString = require('querystring');
const { v4: uuidv4 } = require('uuid');
const { Timer } = require('timer-node');
const { autoscroll, timeForHumans } = require('./utils');
const moment = require('moment');
const colog = require('colog');
const fs = require('fs');

const PATH = '/var/www/ecommerce/storage/app/ultrafarma';
if (!fs.existsSync(PATH)) {
  fs.mkdirSync(PATH)
}

const URL = 'https://www.ultrafarma.com.br';

let processesCategories = 0;
let processesProducts = 0;
const maxProcesses = 4;
const timeout = 100;
const pageLoadTimeout = 120000;
const resultsperpage = 90;

let productAmount = 0;

async function run() {

  colog.info('Ultrafarma Bot is running');
  const browser = await puppeteer.launch({
    args: [
      '--ignore-certificate-errors',
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--window-size=1024,768',
      "--disable-accelerated-2d-canvas",
      "--disable-gpu"],
    ignoreHTTPSErrors: true,
    headless: true
  });

  const home = await browser.newPage();

  try {

    let timer = new Timer();
    timer.start();
    colog.info('STEP 1: Openning home page');

    await home.goto(URL, { waitUntil: 'networkidle2', timeout: pageLoadTimeout });
    await home.waitForSelector('#navbar-main2 > ul li');

    const categoriesHref = await home.$$eval('#navbar-main2 > ul > li', lis => lis.filter(li => {
      let category = li.querySelector('a').textContent.replaceAll('\n', '').trim();
      return category && !category.includes('Promoções') && !category.includes('Linha Sidney Oliveira')
    }).map(li => {

      let a = li.querySelector('a');
      return {
        name: a.textContent.replaceAll('\n', '').trim(),
        categories: [...li.querySelectorAll(':scope ul > li')].map(li => ({
          name: li.querySelector('a').textContent.replaceAll('\n', '').trim(),
          href: li.classList.contains('dropdown-submenu') ? null : li.querySelector('a').getAttribute('href'),
          categories: li.classList.contains('dropdown-submenu') ? [...li.querySelectorAll(':scope ul li a')].map(a => ({
            name: a.textContent.replaceAll('\n', '').trim(),
            href: a.getAttribute('href')
          })) : []
        }))
      }
    }));

    await home.close();
    colog.success(`STEP 1: Finished\nTime of process: ${timeForHumans(timer)}`);
    timer.stop()


    colog.info('STEP 2: Reading categories');
    timer = new Timer();
    timer.start()
    const categories = [];
    categoriesHref.forEach((category0, i0) => {
      colog.setProgressDescription('Category level 1').progress(i0, categoriesHref.length - 1);
      let category = { name: [category0.name], href: [null] };
      categories.push(category);
      category0.categories.forEach((category1, i1) => {
        colog.setProgressDescription('Category level 2').progress(i1, category0.categories.length - 1)
        category = { name: [category0.name, category1.name], href: [null, category1.href] }
        categories.push(category)
        category1.categories.forEach((category2, i2) => {
          colog.setProgressDescription('Category level 3').progress(i2, category1.categories.length - 1)
          category = { name: [category0.name, category1.name, category2.name], href: [null, category1.href, category2.href] }
          categories.push(category)
        })
      })
    })

    colog.success(`STEP 2: Finished\nTime of process: ${timeForHumans(timer)}`);
    timer.stop()

    colog.info('STEP 3: Preparing Categories to Proccess');
    timer = new Timer();
    timer.start();
    const promises = categories
      .filter(category => category.href.filter(href => href).length)
      .map(category => () => new Promise(async (resolve, reject) => runCategoriesPage(browser, category.href.pop(), category, resolve, reject)));

    colog.success(`STEP 3: Finished\nTime of process: ${timeForHumans(timer)}`);
    timer.stop()

    colog.info('STEP 4: Reading products from categories');
    runCategoriesPromises(promises);
  } catch (e) {
    console.log('run ', e)
  }
}




function runCategoriesPromises(promises) {

  const endProccess = maxProcesses - processesCategories;
  if (!endProccess) {
    return setTimeout(() => runCategoriesPromises(promises), timeout)
  }

  colog.info('Runing categories promises');

  Promise
    .all(promises.splice(0, endProccess).map(promise => promise()))
    .then(processed => {

      runCategoriesProcessed(processed)

      if (promises.length) {
        setTimeout(() => runCategoriesPromises(promises), timeout)
      }

    }).catch(errors => {
      console.log('runCategoriesPromises', errors)
    })

}




async function runCategoriesPage(browser, href, category, resolve, reject) {

  processesCategories++
  if (!category.products) {
    category.products = []
  }

  let arrHref = href.split('?');
  colog.info(`Changing Results per page from 30 to ${resultsperpage}`)
  if (arrHref.length === 1) {
    href = arrHref[0].concat(`?resultsperpage=${resultsperpage}&sortby=relevance&page=1`)
  } else {
    const queryStringDecoded = queryString.decode(arrHref[1]);
    queryStringDecoded.resultsperpage = resultsperpage
    href = arrHref[0].concat('?', queryString.stringify(queryStringDecoded))
  }

  href = URL.concat(href);

  let page = await browser.newPage();
  let timer = new Timer();
  timer.start()
  try {
    colog.info(`Opening category page [${href}]`);
    await page.goto(href, { waitUntil: 'networkidle2', timeout: pageLoadTimeout })

    // await autoscroll(page);
    await page.waitForSelector('.prd-list-item');
    category.products = category.products.concat(await page.$$eval('.prd-list-item', elements => elements.map(element => element.querySelector('.product-item-link').getAttribute('href'))))


    colog.info(`${category.products.length} Products`);

    try {
      href = await page.$eval('ul.pagination li.active + li a', anchor => {
        return anchor.getAttribute('href')
      })
      if (href) {
        await page.close();
        processesCategories--;
        colog.success(`Closing category\nTime of process: ${timeForHumans(timer)}`);
        timer.stop();
        return runCategoriesPage(browser, href, category, resolve, reject)
      }
      resolve({ category, browser })
    } catch (e) {
      colog.warning(`It is the last page: ${href}`)
      resolve({ category, browser })
    }
  } catch (e) {
    reject({ category, e })
  }

  await page.close();
  processesCategories--;
  colog.success(`Closing category\nTime of process: ${timeForHumans(timer)}`);
  timer.stop();
}


const runProductsPromises = (promises) => {

  const endProccess = maxProcesses - processesProducts;
  if (!endProccess) {
    return setTimeout(() => runProductsPromises(promises), timeout)
  }

  colog.info('Runing products promises');

  Promise
    .all(promises.splice(0, endProccess).map(promise => promise()))
    .then(() => {
      if (promises.length) {
        setTimeout(() => runProductsPromises(promises), timeout)
      }
    }).catch(errors => {
      console.log('runProductsPromises', errors)
    })
}



const runCategoriesProcessed = async processed => {
  const promises = [];

  for (let proccess of processed) {
    const { browser, category } = proccess;
    const products = category.products;
    productAmount += products.length
    for (let href of products) {
      promises.push(() => new Promise((resolve, reject) => {
        runProductsPage(browser, category, href, resolve, reject)
      }))
    }
  }

  runProductsPromises(promises)
}



const runProductsPage = async (browser, category, href, resolve, reject) => {

  processesProducts++

  colog.info(`Opening Product Page [${href}]`)

  let timer = new Timer();
  timer.start()

  const page = await browser.newPage();
  await page.goto(href, { waitUntil: 'networkidle2', timeout: pageLoadTimeout });


  await page.waitForSelector('.product-name');
  await page.waitForSelector('.product-price-new span:last-child');
  //await autoscroll(page);
  const aBody = await page.$('body');
  let body = await aBody;
  try {
    let product = await body.asElement().evaluate(el => {
      const productPriceOld = el.querySelector('.product-price-old del');
      return {
          category: [...el.querySelectorAll('.breadcrumb li a')].map(a => a.textContent.trim()),
        name: el.querySelector('.product-name').textContent.trim(),
        brand: el.querySelector('.brandName a').textContent.trim(),
        id: el.querySelector('span.skuReference').textContent.trim(),
        images: [...el.querySelectorAll('.img-wrapper a')].map(a => a.getAttribute('ng-href')),
        price_old: productPriceOld ? productPriceOld.textContent.replace(/\./g, '').replace(',', '.').replace('R$', '').trim() : null,
        price_new: el.querySelector('.product-price-new span:last-child').textContent.replace(/\./g, '').replace(',', '.').replace('R$', '').trim(),
        description: el.querySelector('.product-details-section div').innerHTML
      }
    });
    product.category = product.category.slice(1, product.category.length - 1)

    product.href = href;
    console.log(JSON.stringify(product.category))

    fs.writeFile(`${PATH}/${uuidv4()}.json`, JSON.stringify(product), function (err) {
      if (err) {
        return colog.error(`Error on save Product File [${href}]`)
      }
      colog.success(`Product File Saved`)
    })

    resolve(product)
  } catch (e) {
    colog.error(`ERROR proccessing Product [${product}]`, e)
    reject(product)
  }

  await page.close();
  processesProducts--;
  colog.success(`Closing Product Page\nTime of process: ${timeForHumans(timer)}`);
  timer.stop();
}


(async () => {

  run();

})();
